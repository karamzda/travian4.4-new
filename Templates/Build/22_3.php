<div class="clear"></div>
<?php
$fail = $success = 0;
$acares = $technology->grabAcademyRes();
for($i=22;$i<=29;$i++) {
	if($technology->meetRRequirement($i) && !$technology->getTech(($i-20)) && !$technology->isResearch(($i-20),1)) {
        echo "<div class=\"build_details researches\">
        <div>
			<div class=\"bigUnitSection\">
			 <a href=\"#\" onclick=\"return Travian.Game.iPopup(".$i.",1);\">
					<img class=\"unitSection u".$i."Section\" src=\"img/x.gif\" alt=\"".$technology->getUnitName($i)."\">
				</a>
				<a href=\"#\" class=\"zoom\" onclick=\"return Travian.Game.unitZoom(".$i.");\">
					<img class=\"zoom\" src=\"img/x.gif\" alt=\"zoom in\">
				</a>
			</div>
			<div class=\"information\">
<div class=\"title\">
<a href=\"#\" onclick=\"return Travian.Game.iPopup(".$i.",1);\">
<img class=\"unit u".$i."\" src=\"img/x.gif\" alt=\"".$technology->getUnitName($i)."\"></a>
<a href=\"#\" onclick=\"return Travian.Game.iPopup(".$i.",1);\">".$technology->getUnitName($i)."</a>
</div>
<div class=\"costs\">
<div class=\"showCosts\"> <span class=\"resources r1 little_res\"><img class=\"r1\" src=\"img/x.gif\" alt=\"".WOOD."\">".${'r'.$i}['wood']."</span>
                    <span class=\"resources r2 little_res\"><img class=\"r2\" src=\"img/x.gif\" alt=\"".CLAY."\">".${'r'.$i}['clay']."</span>
                    <span class=\"resources r3 little_res\"><img class=\"r3\" src=\"img/x.gif\" alt=\"".IRON."\">".${'r'.$i}['iron']."</span>
                    <span class=\"resources r4 little_res\"><img class=\"r4\" src=\"img/x.gif\" alt=\"".CROP."\">".${'r'.$i}['crop']."</span>
                    <div class=\"clear\"></div>
                    <span class=\"clocks\"><img class=\"clock\" src=\"img/x.gif\" alt=\"".HRS."\">";
        echo $generator->getTimeFormat(round(${'r'.$i}['time'] * ($bid22[$village->resarray['f'.$id]]['attri'] / 100)/SPEED));
        echo "</span>";
        if($session->gold >= 3 && $building->getTypeLevel(17) > 1) {
            echo "&nbsp;&nbsp;<button id='button".crc32($i)."' type=\"button\" value=\"npc\" class=\"icon\">&nbsp;<img src=\"img/x.gif\" style=\"margin-top:6px;\" class=\"npc\" alt=\"npc\"></button>";
            ?>
            <script type="text/javascript">
                window.addEvent('domready', function()
                {
                    if($('button<?=crc32($i)?>'))
                    {
                        $('button<?=crc32($i)?>').addEvent('click', function ()
                        {
                            window.fireEvent('buttonClicked', [this, {"type":"button","value":"Exchange resources","name":"","id":"button5487115a9b649","class":"gold ","title":"Click here to exchange resources.","confirm":"","onclick":"","dialog":{"cssClass":"white","draggable":false,"overlayCancel":true,"buttonOk":false,"saveOnUnload":false,"data":{"cmd":"exchangeResources","defaultValues":{"tid":"1","nr":"1","btyp":"1","r1":<?=((${'r'.$i}['wood']))?>,"r2":<?=((${'r'.$i}['clay']))?>,"r3":<?=((${'r'.$i}['iron']))?>,"r4":<?=((${'r'.$i}['crop']))?>,"supply":"1","pzeit":0,"max1":0,"max2":0,"max3":0,"max4":0,"max":0},"did":"<?=$village->wid;?>"}}}]);
                        });
                    }
                });
            </script>
        <?php }
        if(${'r'.$i}['wood'] > $village->maxstore || ${'r'.$i}['clay'] > $village->maxstore || ${'r'.$i}['iron'] > $village->maxstore) {
            echo "<br><div class=\"contractLink\"><span class=\"none\">Expand warehouse</span></div></div>
<div class=\"clear\">&nbsp;</div>
</div></div>";
        }
        else if(${'r'.$i}['crop'] > $village->maxcrop) {
            echo "<br><div class=\"contractLink\"><span class=\"none\">Expand granary</span></div></div>
<div class=\"clear\">&nbsp;</div>
</div></div>";
        }
        else if(${'r'.$i}['wood'] > $village->awood || ${'r'.$i}['clay'] > $village->aclay || ${'r'.$i}['iron'] > $village->airon || ${'r'.$i}['crop'] > $village->acrop) {
            if($village->getProd("crop")>0){
                $time = $technology->calculateAvaliable(22,${'r'.$i});
                echo "<br><div class=\"contractLink\"><span class=\"none\">Enough resources ".$time[0]." at ".$time[1]."</span></div></div>
<div class=\"clear\">&nbsp;</div>
</div></div>";
            } else {
                echo "<br><div class=\"contractLink\"><span class=\"none\">Crop production is negative so you will never reach the required resources</span></div></div>
<div class=\"clear\">&nbsp;</div>
</div></div>";
            }
            echo "<div class=\"contractLink\"><div class=\"none\">Too few<br>resources</div></div></div></div>";
        }
        else if (count($acares) > 0) {
            echo "<br><div class=\"contractLink\"><span class=\"none\">
					Research in Process</span></div></div></div></div>
                    <div class=\"clear\">&nbsp;</div>
                    </div></div>";
        }
        else {
            echo "<div class=\"contractLink\"><button type=\"button\"  class=\"green\" onclick=\"window.location.href = 'build.php?id=$id&amp;a=$i&amp;c=".$session->mchecker."'; return false;\">
 <div class=\"button-container addHoverClick\">
  <div class=\"button-background\">
   <div class=\"buttonStart\">
    <div class=\"buttonEnd\">
     <div class=\"buttonMiddle\"></div>
    </div>
   </div>
  </div>
  <div class=\"button-content\">".AK8."</div></div></button></div>
</div>
<div class=\"clear\">&nbsp;</div>
</div></div><div class=\"clear\">&nbsp;</div></div></div>";
        }
        $success += 1;
    }
    else {
        $fail += 1;
    }
}
if($success == 0) {
    echo "<div class=\"buildActionOverview trainUnits\"><td colspan=\"2\"><div class=\"none\" align=\"center\">".AK3."</div></td></div>";
}
?>

<?php
if($fail > 0) {
    echo "<p class=\"switch\"><a class=\"openedClosedSwitch switchOpened\" id=\"researchFutureLink\" href=\"#\" onclick=\"Travian.toggleSwitch($('researchFuture'), this);\">".AK6."</a></p>
    <div id=\"researchFuture\" class=\"researches hide \">";
for($uc=22;$uc<=29;$uc++){
    if(!$technology->meetRRequirement($uc) && !$technology->getTech($uc)) {
        echo"<div ><div class=\"bigUnitSection\"><a href=\"#\" onclick=\"return Travian.Game.iPopup(".$uc.",1);\"><img class=\"unitSection u".$uc."Section\" src=\"img/x.gif\" alt=\"".constant('U'.$uc)."\"></a><a href=\"#\" class=\"zoom\" onclick=\"return Travian.Game.unitZoom(".$uc.");\"><img class=\"zoom\" src=\"img/x.gif\" alt=\"zoom in\"></a></div><div class=\"information\"><div class=\"title\"><a href=\"#\" onclick=\"return Travian.Game.iPopup(".$uc.",1);\"><img class=\"unit u".$uc."\" src=\"img/x.gif\" alt=\"".constant('U'.$uc)."\"></a><a href=\"#\" onclick=\"return Travian.Game.iPopup(".$uc.",1);\">".constant('U'.$uc)."</a></div><div class=\"costs\"><div class=\"showCosts\"><span class=\"resources r1 little_res\"><img class=\"r1\" src=\"img/x.gif\" alt=\"".WOOD."\">".${'r'.$uc}['wood']."</span><span class=\"resources r2 little_res\"><img class=\"r2\" src=\"img/x.gif\" alt=\"".CLAY."\">".${'r'.$uc}['clay']."</span><span class=\"resources r3 little_res\"><img class=\"r3\" src=\"img/x.gif\" alt=\"".IRON."\">".${'r'.$uc}['iron']."</span><span class=\"resources r4 little_res\"><img class=\"r4\" src=\"img/x.gif\" alt=\"".CROP."\">".${'r'.$uc}['crop']."</span><div class=\"clear\"></div><span class=\"clocks\"><img class=\"clock\" src=\"img/x.gif\" alt=\"".HRS."\">";
        echo $generator->getTimeFormat(round(${'r'.$uc}['time'] * ($bid22[$village->resarray['f'.$id]]['attri'] / 100)/SPEED));
        switch($uc){
            case 22:echo"</span><div class=\"clear\"></div></div></div><div class=\"contractLink\"><a href=\"#\">".$building->procResType(22)."</a><span class=\"level\"> ".LVL." 3</span>, <a href=\"#\"> ".$building->procResType(12)." </a><span class=\"level\"> ".LVL." 1</span></div></div><div class=\"clear\"></div></div><hr>";break;
            case 23:echo"</span><div class=\"clear\"></div></div></div><div class=\"contractLink\"><a href=\"#\">".$building->procResType(22)."</a><span class=\"level\"> ".LVL." 5</span>, <a href=\"#\"> ".$building->procResType(20)." </a><span class=\"level\"> ".LVL." 1</span></div></div><div class=\"clear\"></div></div><hr>";break;
            case 24: echo"</span><div class=\"clear\"></div></div></div><div class=\"contractLink\"><a href=\"#\">".$building->procResType(22)."</a><span class=\"level\"> ".LVL." 5</span>, <a href=\"#\"> ".$building->procResType(20)." </a><span class=\"level\"> ".LVL." 3</span></div></div><div class=\"clear\"></div></div><hr>";break;
            case 25:echo"</span><div class=\"clear\"></div></div></div><div class=\"contractLink\"><a href=\"#\">".$building->procResType(22)."</a><span class=\"level\"> ".LVL." 5</span>, <a href=\"#\"> ".$building->procResType(20)." </a><span class=\"level\"> ".LVL." 5</span></div></div><div class=\"clear\"></div></div><hr>";break;
            case 26:echo"</span><div class=\"clear\"></div></div></div><div class=\"contractLink\"><a href=\"#\">".$building->procResType(22)."</a><span class=\"level\"> ".LVL." 15</span>, <a href=\"#\"> ".$building->procResType(20)." </a><span class=\"level\"> ".LVL." 10</span></div></div><div class=\"clear\"></div></div><hr>";break;
            case 27: echo"</span><div class=\"clear\"></div></div></div><div class=\"contractLink\"><a href=\"#\">".$building->procResType(22)."</a><span class=\"level\"> ".LVL." 10</span>, <a href=\"#\"> ".$building->procResType(21)." </a><span class=\"level\"> ".LVL." 1</span></div></div><div class=\"clear\"></div></div><hr>";break;
            case 28:echo"</span><div class=\"clear\"></div></div></div><div class=\"contractLink\"><a href=\"#\">".$building->procResType(22)."</a><span class=\"level\"> ".LVL." 15</span>, <a href=\"#\"> ".$building->procResType(21)." </a><span class=\"level\"> ".LVL." 10</span></div></div><div class=\"clear\"></div></div><hr>";break;
            case 29:echo"</span><div class=\"clear\"></div></div></div><div class=\"contractLink\"><a href=\"#\">".$building->procResType(22)."</a><span class=\"level\"> ".LVL." 20</span>, <a href=\"#\"> ".$building->procResType(16)." </a><span class=\"level\"> ".LVL." 10</span></div></div><div class=\"clear\"></div></div>";break;
        }
    }
}

    echo "<script type=\"text/javascript\">
		//<![CDATA[
			$(\"researchFuture\").toggle = (function()
			{
				this.toggleClass(\"hide\");

				$(\"researchFutureLink\").set(\"text\",
					this.hasClass(\"hide\")
					?	\"Показать еще\"
					:	\"Скрыть\"
				);

				return false;
			}).bind($(\"researchFuture\"));
		//]]>
		</script>";
    echo "<div class=\"clear\"></div></div>";
}
//$acares = $technology->grabAcademyRes();
if(count($acares) > 0) {
    echo "<table cellpadding=\"1\" cellspacing=\"1\" class=\"under_progress\"><thead><tr><td>Researching</td><td>Duration</td><td>Complete</td></tr>
	</thead><tbody>";
    if(!isset($timer)){$timer = 1;}
    foreach($acares as $aca) {
        $unit = substr($aca['tech'],1,2);
        $unit+=($session->tribe-1)*10;
        echo "<tr><td class=\"desc\"><img class=\"unit u$unit\" src=\"img/x.gif\" alt=\"".$technology->getUnitName($unit)."\" title=\"".$technology->getUnitName($unit)."\" />".$technology->getUnitName($unit)."</td>";
        echo "<td class=\"dur\"><span id=\"timer$timer\">".$generator->getTimeFormat($aca['timestamp']-time())."</span></td>";
        $date = $generator->procMtime($aca['timestamp']);
        echo "<td class=\"fin\"><span>".$date[1]."</span><span> hrs</span></td>";
        echo "</tr>";
        $timer +=1;
    }
    echo "</tbody></table>";
}
?>
