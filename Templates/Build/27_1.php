<?php

        $artefact = $database->getOwnArtefactInfo2($village->wid);
        $result = count($artefact);


?>
<div class="gid27">
<body>
    <table id="own" cellpadding="1" cellspacing="1">
        <thead>
            <tr>
                <th colspan="4"><?=sokr10?></th>
            </tr>
            <tr>
                <td></td>
                <td><?=sokr11?></td>
                <td><?=sokr1?></td>
                <td><?=sokr9?></td>
            </tr>
        </thead>

        <tbody>
            <tr>
            <?php

        if($result == 0) {
        	echo '<td colspan="4" class="none">'.sokr12.'</td>';
        } else {
        foreach($artefact as $artefac){
          $te = $artefac['type'];
                   $se = $artefac['size'];
                    if($te== 1 AND $se == 1){
                    $name = ART1;
                    $desc = ART16;
                    $bonus= "(4x) " ;
                    $image =  '<img class="artefact_icon_2" src="img/x.gif">';
                    }
                     if($te== 1 AND $se == 2){
                    $name = ART2;
                    $desc =ART16;
                    $bonus= "(3x) " ;
                    $image =  '<img class="artefact_icon_2" src="img/x.gif">';
                    }
                     if($te== 1 AND $se == 3){
                    $name = ART3;
                    $desc =  ART16;
                    $bonus= "(5x) " ;
                         $image =  '<img class="artefact_icon_2" src="img/x.gif">';
                    }
                     if($te== 2 AND $se == 1){
                    $name = ART4;
                    $desc = ART17;
                    $bonus= "(2x) " ;
                         $image =  '<img class="artefact_icon_4" src="img/x.gif">';
                    }
                     if($te== 2 AND $se == 2){
                    $name = ART5;
                    $desc =  ART17;
                    $bonus= "(1.5x) " ;
                         $image =  '<img class="artefact_icon_4" src="img/x.gif">';
                    }
                     if($te== 2 AND $se == 3){
                    $name = ART6;
                    $desc =  ART17;
                    $bonus= "(3x) " ;
                         $image =  '<img class="artefact_icon_4" src="img/x.gif">';
                    }
                     if($te== 3 AND $se == 1){
                    $name = ART7;
                     $desc = ART18;
                    $bonus= "(5x) " ;
                         $image =  '<img class="artefact_icon_5" src="img/x.gif">';
                    }
                     if($te== 3 AND $se == 2){
                    $name = ART8;
                     $desc = ART18  ;
                    $bonus= "(3x) " ;
                         $image =  '<img class="artefact_icon_5" src="img/x.gif">';
                    }
                     if($te== 3 AND $se == 3){
                    $name = ART9;
                     $desc =  ART18   ;
                    $bonus= "(10x) " ;
                         $image =  '<img class="artefact_icon_5" src="img/x.gif">';
                   }
                     if($te== 4 AND $se == 1){
                    $name = "Волшебная мельничка";
                     $desc =    "Этот артефакт сокращает потребление зерна войсками."   ;
                    $bonus= "(50%) " ;
                         $image =  '<img class="artefact_icon_6" src="img/x.gif">';
                   }
                     if($te== 4 AND $se == 2){
                    $name = "Стол Лукулла";
                     $desc =   "Этот артефакт сокращает потребление зерна войсками."      ;
                    $bonus= "(25%) " ;
                         $image =  '<img class="artefact_icon_6" src="img/x.gif">';
                   }
                     if($te== 4 AND $se == 3){
                    $name = "Чаша короля Артура";
                     $desc =      "Этот артефакт сокращает потребление зерна войсками."   ;
                    $bonus= "(50%) " ;
                         $image =  '<img class="artefact_icon_6" src="img/x.gif">';
                   }
                     if($te== 5 AND $se == 1){
                    $name = ART10;
                     $desc =       ART19   ;
                    $bonus= "(50%) " ;
                         $image =  '<img class="artefact_icon_8" src="img/x.gif">';
                   }
                     if($te== 5 AND $se == 2){
                    $name = ART11;
                     $desc =       ART19     ;
                    $bonus= "(25%) " ;
                         $image =  '<img class="artefact_icon_8" src="img/x.gif">';
                   }
                     if($te== 5 AND $se == 3){
                    $name = ART12;
                     $desc =        ART19   ;
                    $bonus= "(50%) " ;
                         $image =  '<img class="artefact_icon_8" src="img/x.gif">';
                   }
                     if($te== 6){
                    $name = ART13;
                    $desc = ART20;
                    $bonus= ART16 ;
                         $image =  '<img class="artefact_icon_9" src="img/x.gif">';
                     }
                    if($te== 7 AND $se == 1){
                    $name = "Бездонный мешок";
                     $desc = "Этот артефакт увеличивает вместимость тайника. Кроме этого, вражеские катапульты могут вести прицельный огонь только по сокровищнице и Чуду света либо по случайным целям. Уникальный артефакт предотвращает возможность вести прицельный огонь по сокровищнице.";
                    $bonus= "(200) " ;
                        $image =  '<img class="artefact_icon_" src="img/x.gif">';
                   }
                     if($te== 7 AND $se == 2){
                    $name = "Подземный храм";
                     $desc =          "Этот артефакт увеличивает вместимость тайника. Кроме этого, вражеские катапульты могут вести прицельный огонь только по сокровищнице и Чуду света либо по случайным целям. Уникальный артефакт предотвращает возможность вести прицельный огонь по сокровищнице."  ;
                    $bonus= "(100) " ;
                         $image =  '<img class="artefact_icon_" src="img/x.gif">';
                   }
                     if($te== 7 AND $se == 3){
                    $name = "Троянский конь";
                     $desc =           "Этот артефакт увеличивает вместимость тайника. Кроме этого, вражеские катапульты могут вести прицельный огонь только по сокровищнице и Чуду света либо по случайным целям. Уникальный артефакт предотвращает возможность вести прицельный огонь по сокровищнице." ;
                    $bonus= "(500) " ;
                         $image =  '<img class="artefact_icon_" src="img/x.gif">';
                   }
                     if($te== 8 AND $se == 1){
                    $name = "Книга тёмных тайн";
                     $desc =            "Эффект этого артефакта меняется как при захвате, так и каждые 24 часа. Тем самым, эффект артефакта может быть как позитивным, так и негативным, то есть бонусы каких-либо других артефактов могут иметь отрицательный показатель. Например, войска могут строиться медленнее или потреблять больше зерна.";
                    $bonus= "Случайно" ;
                         $image =  '<img class="artefact_icon_" src="img/x.gif">';
                   }
                     if($te== 8 AND $se == 3){
                    $name = "Обгоревший манускрипт";
                     $desc =           "Эффект этого артефакта меняется как при захвате, так и каждые 24 часа. Тем самым, эффект артефакта может быть как позитивным, так и негативным, то есть бонусы каких-либо других артефактов могут иметь отрицательный показатель. Например, войска могут строиться медленнее или потреблять больше зерна.";
                    $bonus= "Случайно" ;
                         $image =  '<img class="artefact_icon_" src="img/x.gif">';
                   }
                     if($te== 11 ){
                    $name = ART14;
                     $desc =    ART21;
                    $bonus= ART21  ;
                         $image =  '<img class="artefact_icon_1" src="img/x.gif">';
                   }
        	if($artefac['size'] == 1) {
        		$reqlvl = 10;
        		$effect = sokr1;
        	} elseif($artefac['size'] == 2 or 3) {
        		$reqlvl = 20;
        		$effect = pluss11;
        	}
        	echo '<td class="icon">'.$image.'</td>';

        	echo '<td class="nam">
                            <a href="build.php?id=' . $id . '&show='.$artefac['id'].'">' . $name . '</a> <span class="bon">' . $bonus . '</span>
                            <div class="info">
                                '.sokr17.' <b>' . $reqlvl . '</b>, '.sokr3.' <b>' . $effect . '</b>
                            </div>
                        </td>';
        	echo '<td class="pla"><a href="karte.php?d=' . $artefac['vref'] . '&c=' . $generator->getMapCheck($artefac['vref']) . '">' . $database->getVillageField($artefac['vref'], "name") . '</a></td>';
        	echo '<td class="dist">' . date("d/m/Y H:i", $artefac['conquered']) . '</td>';
       echo' </tr>';

        }
              }
?>

        </tbody>
    </table>

    <table id="near" cellpadding="1" cellspacing="1">
        <thead>
            <tr>
                <th colspan="4"><?=sokr13?></th>
            </tr>

            <tr>
                <td></td>

                <td><?=sokr11?></td>

                <td><?=sokr14?></td>

                <td><?=sokr15?></td>
            </tr>
        </thead>

        <tbody>
          <?php
                   $arts=$database->geallart();

                   $coarts=count($arts);
        if($coarts == 0) {
        	echo '<td colspan="4" class="none">'.sokr16.'</td>';
        } else {






        	$rows = array();
        	$coor = $village->coor;
                  $numeric=0;
                  $ids='';
        	foreach($arts as $row) {
        	$numeric++;
        		if($coarts>$numeric){
        	$ids.=$row['vref'].',';
            }else{ $ids.=$row['vref'];
        	}
            }
        		$xyid=$database->getxyart($ids);
        		foreach($xyid as $xyi){
        			$xy[$xyi['id']]=array('x'=>$xyi['x'],'y'=>$xyi['y']);
        			}

        	foreach($arts as $row) {
        		$coor2 = $xy[$row['vref']];

                 		$xdistance = ABS($coor['x'] - $coor2['x']);
		if($xdistance > WORLD_MAX) {
			$xdistance = (2 * WORLD_MAX + 1) - $xdistance;
		}
		$ydistance = ABS($coor['y'] - $coor2['y']);
		if($ydistance > WORLD_MAX) {
			$ydistance = (2 * WORLD_MAX + 1) - $ydistance;
		}
		$dist = round(SQRT(POW($xdistance,2)+POW($ydistance,2)),1);

                   array_push($row,$dist);
        		$rows[$dist] = $row;

        	}
        	ksort($rows, SORT_DESC);
        	foreach($rows as $row) {

                $wref2 = $row['vref'];

                  $te = $row['type'];
                   $se = $row['size'];
                if($te== 1){
                    $image =  '<img class="artefact_icon_2" src="img/x.gif">';
                }elseif($te== 2){
                    $image =  '<img class="artefact_icon_4" src="img/x.gif">';
                }if($te== 3){
                    $image =  '<img class="artefact_icon_5" src="img/x.gif">';
                }elseif($te== 4){
                    $image =  '<img class="artefact_icon_6" src="img/x.gif">';
                }elseif($te== 5){

                    $image =  '<img class="artefact_icon_8" src="img/x.gif">';
                }elseif($te== 6){
                    $image =  '<img class="artefact_icon_9" src="img/x.gif">';
                }elseif($te== 11){
                    $image =  '<img class="artefact_icon_1" src="img/x.gif">';
                }
                    if($te== 1 AND $se == 1){
                    $name = ART1;
                    $bonus= "(4x) " ;
                    }
                     if($te== 1 AND $se == 2){
                    $name = ART2;
                    $bonus= "(3x) " ;

                    }
                     if($te== 1 AND $se == 3){
                    $name = ART3;
                    $bonus= "(5x) " ;

                    }
                     if($te== 2 AND $se == 1){
                    $name = ART4;
                    $bonus= "(2x) " ;

                    }
                     if($te== 2 AND $se == 2){
                    $name = ART5;
                    $bonus= "(1.5x) " ;

                    }
                     if($te== 2 AND $se == 3){
                    $name = ART6;
                    $bonus= "(3x) " ;

                    }
                     if($te== 3 AND $se == 1){
                    $name = ART7;
                     $bonus= "(5x) " ;

                    }
                     if($te== 3 AND $se == 2){
                    $name = ART8;
                    $bonus= "(3x) " ;

                    }
                     if($te== 3 AND $se == 3){
                    $name = ART9;
                     $bonus= "(10x) " ;

                   }

                     if($te== 5 AND $se == 1){
                    $name = ART10;
                    $bonus= "(50%) " ;

                   }
                     if($te== 5 AND $se == 2){
                    $name = ART11;
                    $bonus= "(25%) " ;

                   }
                     if($te== 5 AND $se == 3){
                    $name = ART12;
                    $bonus= "(50%) " ;

                   }
                     if($te== 6){
                    $name = ART13;
                    $bonus= ART15 ;

                     }

                     if($te== 11 ){
                    $name = ART14;

                    $bonus= ART15 ;

                   }

        		echo '<tr>';
        		echo '<td class="icon">'.$image.'</td>';
        		echo '<td class="nam">';
        		echo '<a href="build.php?id=' . $id . '&show='.$row['id'].'">' . $name . '</a> <span class="bon">' . $bonus . '</span>';
        		echo '<div class="info">';
        		if($row['size'] == 1) {
        			$reqlvl = 10;
        			$effect = sokr1;
        		} elseif($row['size'] == 2 or $row['size'] == 3) {
        			$reqlvl = 20;
        			$effect = pluss11;
        		}
        		echo '<div class="info">'.sokr17.' <b>' . $reqlvl . '</b>, Эффект <b>' . $effect . '</b>';
        		echo '</div></td><td class="pla"><a href="karte.php?d=' . $row['vref'] . '&c=' . $generator->getMapCheck($row['vref']) . '">' . $database->getUserField($row['owner'], "username", 0) . '</a></td>';
        		echo '<td class="dist">'.$row[0].'</td>';
        		echo '</tr>';
        	}
        }

?>
        </tbody>
    </table>
</div>
