<div class="clear"></div>
<div class="build_details researches">
    <?php
    $abdata = $database->getABTech($village->wid);
    $ABups = $technology->getABUpgrades('a');

    for($i=($session->tribe*10-9);$i<=($session->tribe*10-2);$i++) {
        $j = $i % 10 ;

        if ( $technology->getTech(($i-(($session->tribe-1)*10))) || $j == 1 ) {
            if(count($ABups) && $ABups[0]['tech']=="a".($i-(($session->tribe-1)*10))){$abdata['a'.$j]+=1;}
            echo "<div class=\"research\">
		<div class=\"bigUnitSection\">
			<a class=\"unitSection\" href=\"#\" \">
				<img class=\"unitSection u".$i."Section\" src=\"img/x.gif\" alt=\"".$technology->getUnitName($i)."\">
			</a>
			<a href=\"#\" class=\"zoom\" onclick=\"return Travian.Game.unitZoom(".$i.");\">
				<img class=\"zoom\" src=\"img/x.gif\" alt=\"Zoom\">
			</a>
		</div>
		<div class=\"information\">
<div class=\"title\">
<a href=\"#\" \">
<img class=\"unit u".$i."\" src=\"img/x.gif\" alt=\"".$technology->getUnitName($i)."\"></a>
<a href=\"#\" \">".$technology->getUnitName($i)."</a>
<span class=\"level\">".LVL." ".$abdata['a'.$j]."</span>
</div>";
            if($abdata['a'.$j] != 20) {
                echo "<div class=\"costs\">
<div class=\"showCosts\">
<span class=\"resources r1 little_res\"><img class=\"r1\" src=\"img/x.gif\" alt=\"fa\">".${'ab'.$i}[$abdata['a'.$j]+1]['wood']."</span>
<span class=\"resources r2 little_res\"><img class=\"r2\" src=\"img/x.gif\" alt=\"agyag\">".${'ab'.$i}[$abdata['a'.$j]+1]['clay']."</span>
<span class=\"resources r3\"><img class=\"r3\" src=\"img/x.gif\" alt=\"vasérc\">".${'ab'.$i}[$abdata['a'.$j]+1]['iron']."</span>
<span class=\"resources r4\"><img class=\"r4\" src=\"img/x.gif\" alt=\"búza\">".${'ab'.$i}[$abdata['a'.$j]+1]['crop']."</span>
<div class=\"clear\"></div>

<span class=\"clocks\">
<img class=\"clock\" src=\"img/x.gif\" alt=\"Idő\">";
                echo $generator->getTimeFormat(round(${'ab'.$i}[$abdata['a'.$j]+1]['time']*($bid12[$building->getTypeLevel(12)]['attri'] / 100)/SPEED));
                echo "</span>";
                if($session->gold >= 3 && $building->getTypeLevel(17) >= 1) {
                    echo "&nbsp;&nbsp;<button id='button".crc32($i)."' type=\"button\" value=\"npc\" class=\"icon\">&nbsp;<img src=\"img/x.gif\" style=\"margin-top:6px;\" class=\"npc\" alt=\"npc\"></button>";
                    ?>
                    <script type="text/javascript">
                        window.addEvent('domready', function()
                        {
                            if($('button<?=crc32($i)?>'))
                            {
                                $('button<?=crc32($i)?>').addEvent('click', function ()
                                {
                                    window.fireEvent('buttonClicked', [this, {"type":"button","value":"Exchange resources","name":"","id":"button5487115a9b649","class":"gold ","title":"Click here to exchange resources.","confirm":"","onclick":"","dialog":{"cssClass":"white","draggable":false,"overlayCancel":true,"buttonOk":false,"saveOnUnload":false,"data":{"cmd":"exchangeResources","defaultValues":{"tid":"1","nr":"1","btyp":"1","r1":<?=((${'ab'.$i}[$abdata['a'.$j]+1]['wood']))?>,"r2":<?=((${'ab'.$i}[$abdata['a'.$j]+1]['clay']))?>,"r3":<?=((${'ab'.$i}[$abdata['a'.$j]+1]['iron']))?>,"r4":<?=((${'ab'.$i}[$abdata['a'.$j]+1]['crop']))?>,"supply":"1","pzeit":0,"max1":0,"max2":0,"max3":0,"max4":0,"max":0},"did":"<?=$village->wid;?>"}}}]);
                                });
                            }
                        });
                    </script>
                <?php
                }
                echo "<div class=\"clear\"></div>
</div>
</div>";
            }
            if (${'ab'.$i}[$abdata['a'.$j]+1]['wood'] > $village->maxstore || ${'ab'.$i}[$abdata['a'.$j]+1]['clay'] > $village->maxstore || ${'ab'.$i}[$abdata['a'.$j]+1]['iron'] > $village->maxstore) {
                echo "<div class=\"contractLink\"><span class=\"none\">Upgrade Warehouse</span></div>";
            }
            else if (${'ab'.$i}[$abdata['a'.$j]+1]['crop'] > $village->maxcrop) {
                echo "<div class=\"contractLink\"><span class=\"none\">Upgrade Granary</span></div>";
            }
            else if (${'ab'.$i}[$abdata['a'.$j]+1]['wood'] > $village->awood || ${'ab'.$i}[$abdata['a'.$j]+1]['clay'] > $village->aclay || ${'ab'.$i}[$abdata['a'.$j]+1]['iron'] > $village->airon || ${'ab'.$i}[$abdata['a'.$j]+1]['crop'] > $village->acrop) {
                if($village->getProd("crop")>0){
                    $time = $technology->calculateAvaliable(12,${'ab'.$i}[$abdata['a'.$j]+1]);
                    echo "<div class=\"contractLink\"><span class=\"none\">Enough Resources: ".$time[0]." ".$time[1]."</span></div>";
                } else {
                    echo "<div class=\"contractLink\"><span class=\"none\">Wheat production is negative, there will never be enough resources</span></div>";
                }
                //echo "<div class=\"contractLink\"><span class=\"none\">few resources</span></div>";
            }
            else if ($building->getTypeLevel(12) <= $abdata['a'.$j]) {
                if ($abdata['a'.$j] == 20)
                {
                    echo "<div class=\"contractLink\"><span class=\"none\">".kuzupg0." </span></div>";
                }
                else
                {
                    echo "<div class=\"contractLink\"><span class=\"none\">".kuzupg1."</span></div>";
                }
            }
            else if (count($ABups) > 1) {
                echo "<div class=\"contractLink\"><span class=\"none\">".kuzupg2."</span></div>";
            }
            else {

                echo "<div class=\"contractLink\"><span class=\"none\">
                    <button type=\"button\" value=\"Upgrade level\" class=\"green\" onclick=\"window.location.href = 'build.php?id=$id&amp;a=$j&amp;c=$session->mchecker'; return false;\">
<div class=\"button-container addHoverClick\">
  <div class=\"button-background\">
   <div class=\"buttonStart\">
    <div class=\"buttonEnd\">
     <div class=\"buttonMiddle\"></div>
    </div>
   </div>
  </div>
  <div class=\"button-content\">".KZ2."</div></div></button>
                    </span></div>";
            }
            echo "</div>
<div class=\"clear\"></div>
</div><hr>";
        }
    }
    ?>

</div>

<?php
if(count($ABups) > 0) {
    echo "<table cellpadding=\"1\" cellspacing=\"1\" class=\"under_progress\"><thead><tr><td>".KZ5."</td><td>".KZ6."</td><td>".KZ7."</td></tr>
</thead><tbody>";
    if(!isset($timer)) {
        $timer = 1;
    }
    foreach($ABups as $black) {
        $unit = ($session->tribe-1)*10 + substr($black['tech'],1,2);
        echo "<tr><td class=\"desc\"><img class=\"unit u$unit\" src=\"img/x.gif\" alt=\"".$technology->getUnitName($unit)."\" title=\"".$technology->getUnitName($unit)."\" />".$technology->getUnitName($unit)."</td>";
        echo "<td class=\"dur\"><span id=\"timer$timer\">".$generator->getTimeFormat($black['timestamp']-time())."</span></td>";
        $date = $generator->procMtime($black['timestamp']);
        echo "<td class=\"fin\"><span>".$date[1]."</span><span> </span></td>";
        echo "</tr>";
        $timer +=1;
    }
    echo "</tbody></table>";
}
?>