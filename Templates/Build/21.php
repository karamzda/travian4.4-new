<div id="build" class="gid21">
<?php

    if ($building->getTypeLevel(21) > 0) { ?>
        <div class="clear"></div>
        <form method="POST" name="snd" action="build.php">
            <input type="hidden" name="id" value="<?php echo $id; ?>" />
            <input type="hidden" name="ft" value="t1" />
            <div class="buildActionOverview trainUnits">
                <?php
                $art = $database->checkArtefactsEffects($session->uid, $village->wid, 5);
                $success = 0;
                $start = ($session->tribe == 1)? 7 : (($session->tribe == 2)? 17 : 27);
                if ($session->tribe == 1){
                    $start = 7;
                }else if ($session->tribe == 2){
                    $start = 17;
                }else if ($session->tribe == 3){
                    $start = 27;
                }
                for ($i = $start; $i <= ($start + 1); $i++) {
                    $ava = $i - (($session->tribe - 1) * 10);
                    if ($technology->getTech($ava)) {
                        echo "<div class=\"action first\">
                	<div class=\"bigUnitSection\">
						<a href=\"#\" onclick=\"return Travian.Game.iPopup($i,1);\">
							<img class=\"unitSection u".$i."Section\" src=\"img/x.gif\" alt=\"".$technology->getUnitName($i)."\">
						</a>
						<a href=\"#\" class=\"zoom\" onclick=\"return Travian.Game.unitZoom($i);\">
							<img class=\"zoom\" src=\"img/x.gif\" alt=\"zoom in\">
						</a>
					</div>
					<div class=\"details\">
						<div class=\"tit\">
							<a href=\"#\" onclick=\"return Travian.Game.iPopup($i,1);\"><img class=\"unit u$i\" src=\"img/x.gif\" alt=\"Paladin\"></a>
							<a href=\"#\" onclick=\"return Travian.Game.iPopup($i,1);\">".$technology->getUnitName($i)."</a>
							<span class=\"furtherInfo\">(Avalaible: ".$village->unitarray['u'.$ava].")</span>
						</div>
                        <div class=\"showCosts\">
                        <span class=\"resources r1\"><img class=\"r1\" src=\"img/x.gif\" alt=\"Fa\">".${'u'.$i}['wood']."</span>
                        <span class=\"resources r2\"><img class=\"r2\" src=\"img/x.gif\" alt=\"Agyag\">".${'u'.$i}['clay']."</span>
                        <span class=\"resources r3\"><img class=\"r3\" src=\"img/x.gif\" alt=\"Vasérc\">".${'u'.$i}['iron']."</span>
                        <span class=\"resources r4\"><img class=\"r4\" src=\"img/x.gif\" alt=\"Búza\">".${'u'.$i}['crop']."</span>
                        <span class=\"resources r5\"><img class=\"r5\" src=\"img/x.gif\" alt=\"Búzafogyasztás\">".${'u'.$i}['pop']."</span>
                        <div class=\"clear\"></div>
                        <span class=\"clocks\"><img class=\"clock\" src=\"img/x.gif\" alt=\"duration\">";
                        $dur = round((${'u' . $i}['time'] * ($bid21[$village->resarray['f' . $id]]['attri'] / 100) / SPEED * $art), 5);

                        $dur = $generator->getTimeFormat($dur);
                        echo $dur;
                        if($session->gold >= 3 && $building->getTypeLevel(17) >= 1) {
                            echo "&nbsp;&nbsp;<button id='button".crc32($i)."' type=\"button\" value=\"npc\" class=\"icon\">&nbsp;<img src=\"img/x.gif\" style=\"margin-top:6px;\" class=\"npc\" alt=\"npc\"></button>";
                            ?>
                            <script type="text/javascript">
                                window.addEvent('domready', function()
                                {
                                    if($('button<?=crc32($i)?>'))
                                    {
                                        $('button<?=crc32($i)?>').addEvent('click', function ()
                                        {
                                            window.fireEvent('buttonClicked', [this, {"type":"button","value":"Exchange resources","name":"","id":"button5487115a9b649","class":"gold ","title":"Click here to exchange resources.","confirm":"","onclick":"","dialog":{"cssClass":"white","draggable":false,"overlayCancel":true,"buttonOk":false,"saveOnUnload":false,"data":{"cmd":"exchangeResources","defaultValues":{"tid":"1","nr":"1","btyp":"1","r1":<?=((${'u'.$i}['wood'])*$technology->maxUnitPlus($i))?>,"r2":<?=((${'u'.$i}['clay'])*$technology->maxUnitPlus($i))?>,"r3":<?=((${'u'.$i}['iron'])*$technology->maxUnitPlus($i))?>,"r4":<?=((${'u'.$i}['crop'])*$technology->maxUnitPlus($i))?>,"supply":"1","pzeit":0,"max1":0,"max2":0,"max3":0,"max4":0,"max":0},"did":"<?=$village->wid;?>"}}}]);
                                        });
                                    }
                                });
                            </script>
                        <?php }
                        echo "</span><div class=\"clear\"></div></div><span class=\"value\">".mastr5."</span>
						<input type=\"text\" class=\"text\" name=\"t$i\" value=\"0\" maxlength=\"".MAXLENGHT."\">
                        <span class=\"value\"> / </span>
						<a href=\"#\" onClick=\"document.snd.t$i.value=".$technology->maxUnit($i)."; return false;\">".$technology->maxUnit($i)."</a>
					</div></div>
					<div class=\"clear\"></div><br />";
                        $success += 1;
                    }
                }
                if($success == 0) {
                    echo "<tr><td colspan=\"3\"><div class=\"none\"><center>".mastr0."</center></div></td></tr>";
                }
                ?>
            </div><div class="clear"></div>
            <?php if($success) {?><button type="submit"  class="green small">
                <div class="button-container addHoverClick ">
                    <div class="button-background">
                        <div class="buttonStart">
                            <div class="buttonEnd">
                                <div class="buttonMiddle"></div>
                            </div>
                        </div>
                    </div><div class="button-content"><?=mastr1?></div>
                </div>
            </button>
            <?php }?>
        </form>
    <?php
    } else {
        echo "<b>".mastr0."</b><br>\n";
    }

    $trainlist = $technology->getTrainingList(3);
    if(count($trainlist) > 0) {
        //$timer = 2*count($trainlist);
        echo "
    <table cellpadding=\"1\" cellspacing=\"1\" class=\"under_progress\">
		<thead><tr>
			<td>".mastr2."</td>
			<td>".mastr3."</td>
			<td>".mastr4."</td>
		</tr></thead>
		<tbody>";
        $TrainCount = 0;
        if(!isset($timer)){ $timer=1;}
        foreach ($trainlist as $train) {
            $TrainCount++;
            echo "<tr><td class=\"desc\">";
            echo "<img class=\"unit u" . $train['unit'] . "\" src=\"img/x.gif\" alt=\"" . $train['name'] . "\" title=\"" . $train['name'] . "\" />";
            echo $train['amt'] . " " . $train['name'] . "</td><td class=\"dur\">";
            if ($TrainCount == 1) {
                echo "<span id=timer1>" . $generator->getTimeFormat(round($train['eachtime'] * $train['amt'])) . "</span>";
            } else {
                echo $generator->getTimeFormat(round($train['eachtime'] * $train['amt']));
            }
            echo "</td><td class=\"fin\">";
            $time = $generator->procMTime($train['timestamp']);
            if ($time[0] != "today") {
                echo "on " . $time[0] . " at ";
            }
            echo $time[1];
        } ?>
        </tr>
        </tbody></table>
        <?php
		echo "<font color=darkorange>Train all types of troops in current village for 1 hour    :    </font>";
		if($session->gold >= 10) {
    echo "<button type=\"button\"  class=\"green ".$disabl."\" onclick=\"return Travian.Game.buyplus(14,".HOWRES."); return false;\"><div class=\"button-container addHoverClick \">     <div class=\"button-background\">         <div class=\"buttonStart\">             <div class=\"buttonEnd\">                 <div class=\"buttonMiddle\"></div>             </div>         </div>     </div>     <div class=\"button-content\">".PLUS26."</div></div></button>";
} else {
    //echo "<button type=\"button\" class=\"gold \"  class=\" disabled\" onclick=\"(new Event(event)).stop(); return false;\" onfocus=\"$$('button', 'input[type!=hidden]', 'select')[0].focus(); (new Event(event)).stop(); return false;\"><div class=\"button-container\"><div class=\"button-position\"><div class=\"btl\"><div class=\"btr\"><div class=\"btc\"></div></div></div><div class=\"bml\"><div class=\"bmr\"><div class=\"bmc\"></div></div></div><div class=\"bbl\"><div class=\"bbr\"><div class=\"bbc\"></div></div></div></div><div class=\"button-contents\">".PLUS14."</div></div></button>";
    echo "<button class=\"gold \" type=\"button\"  class=\" disabled\" onclick=\"(new Event(event)).stop(); return false;\" onfocus=\"$$('button', 'input[type!=hidden]', 'select')[0].focus(); (new Event(event)).stop(); return false;\"><div class=\"button-container addHoverClick \">     <div class=\"button-background\">         <div class=\"buttonStart\">             <div class=\"buttonEnd\">                 <div class=\"buttonMiddle\"></div>             </div>         </div>     </div>     <div class=\"button-content\">".pluss27."</div></div></button>";
}
?>
 <b>
                    
                    10
                </b>
                <img src="img/x.gif" class="gold">
	
	
	<?php

    }

    ?>
    </div>

<div class="clear">&nbsp;</div>
<div class="clear"></div>