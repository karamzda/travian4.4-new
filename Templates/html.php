<head>
    <title><?= SERVER_NAME ?></title>
    <link rel="shortcut icon" href="favicon.ico"/>
    <meta name="content-language" content="ru" />
    <link href="gpack/delusion_4.4/lang/en/compact.css" rel="stylesheet" type="text/css" />
    <script src="jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        var j$ = $.noConflict();
    </script>
    <script type="text/javascript" src="crypt.js"></script>


    <script type="text/javascript">
        Travian.Translation.add(
            {
                'allgemein.anleitung':	'Справка',
                'allgemein.cancel':	'Отменить',
                'allgemein.ok':	'ОК',
                'allgemein.close':	'Закрыть',
                'cropfinder.keine_ergebnisse': 'Ничего не было найдено'
            });
        Travian.applicationId = 'T4.4 Game';
        Travian.Game.version = '4.4';
        Travian.Game.worldId = 'rux18';
        Travian.Game.speed = <?=SPEED?>;

        Travian.Templates = {};
        Travian.Templates.ButtonTemplate = "<button >\n\t<div class=\"button-container addHoverClick\">\n\t\t<div class=\"button-background\">\n\t\t\t<div class=\"buttonStart\">\n\t\t\t\t<div class=\"buttonEnd\">\n\t\t\t\t\t<div class=\"buttonMiddle\"><\/div>\n\t\t\t\t<\/div>\n\t\t\t<\/div>\n\t\t<\/div>\n\t\t<div class=\"button-content\"><\/div>\n\t<\/div>\n<\/button>\n";

        Travian.Game.eventJamHtml = '&lt;a href=&quot;http://t4.answers.travian.ru/index.php?aid=252#go2answer&quot; target=&quot;blank&quot; title=&quot;Помощь при скоплении событий&quot;&gt;&lt;span class=&quot;c0 t&quot;&gt;0:00:0&lt;/span&gt;?&lt;/a&gt;'.unescapeHtml();

        window.addEvent('domready', function() {
            Travian.Form.UnloadHelper.message = 'Были проведены некоторые изменения. Вы действительно желаете покинуть эту страницу?';
        });
    </script>

</head>