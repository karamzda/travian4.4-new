﻿<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76876209-1', 'auto');
  ga('send', 'pageview');

</script>
<div id="servertime" class="stime">
    <?php echo SERVER_TIME; ?>&nbsp;
    <span id="tp1"><?php echo date('H:i:s'); ?></span>
</div>
<?php
if(isset($_GET['lang'])){

    if(count($_GET)==1){
        $_SERVER['QUERY_STRING']= preg_replace('/lang='.$_GET['lang'].'/','',$_SERVER['QUERY_STRING']);
    }else{
        $_SERVER['QUERY_STRING']= preg_replace('/&lang='.$_GET['lang'].'/','',$_SERVER['QUERY_STRING']);
    }
}
if(count($_GET) && isset($_GET['lang'])){
    if($_GET['lang']!='en'){
        $linken='?'.$_SERVER['QUERY_STRING'].'&lang=en';
    }else{$linken='?'.$_SERVER['QUERY_STRING'];}
    if($_GET['lang']!='ru'){
        $linkru='?'.$_SERVER['QUERY_STRING'].'&lang=ru';
    }else{$linkru='?'.$_SERVER['QUERY_STRING'];}
}elseif(!count($_GET)){
    $linkru='?lang=ru';
    $linken='?lang=en';
}else{
    $linkru='?'.$_SERVER['QUERY_STRING'].'&lang=ru';
    $linken='?'.$_SERVER['QUERY_STRING'].'&lang=en';

}


?>

<a href="<?=$_SERVER['PHP_SELF'].$linkru?>" title="Изменить Язык на Русский" class="ru"></a>
<a href="<?=$_SERVER['PHP_SELF'].$linken?>" title="Change language into English" class="en"></a>

