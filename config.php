<?php
###############################  E    N    D   ##################################
##              -= YOU MAY NOT REMOVE OR CHANGE THIS NOTICE =-                 ##
## --------------------------------------------------------------------------- ##
##  Developed by:  Brainiac & Wolfcruel                                        ##
##  License:       BrainianZ Project                                        ##
##  Copyright:     BrainianZ © 2011-2014. Skype brainiac.brainiac         ##
##                                                                             ##
#################################################################################
header('Content-Type: text/html; charset=UTF-8');

set_time_limit(0);
error_reporting(E_ALL ^ E_NOTICE);

date_default_timezone_set("Europe/Moscow");

$loginDB['index']=array('Server'=>"localhost",'User'=>"karamzda_index",'Password'=>"superparolforbank",'Database'=>"karamzda_index");

define("SERVER_NAME","xTravian");
define("COSTRES",10);
define("DEFAULT_GOLD",100000);  //скок голды даем по умолчанию
define("HOWRES",10000);
define("COSTCP",20);
define("HOWCP",2500);
define("AUCTIONTIME",10800);
define("GP_LOCATE", "gpack/delusion_4.4/");
define("OPENING", 1430982902); //открытие серва
define("REF_POP",500);
define("REF_GOLD",50);
define("OASISX",8000000);
define("SPEED", 8000000);
define("MAX_FILES",1000);
define("MAX_FILESH",3000);
define("IMGQUALITY",50);
define("MOMENT_TRAIN",False);
define("QUEST",True);
define("ARTEFACTS",1431155702);
define("WW_TIME",1431328502);
define("WW_PLAN",1431414902);
define("SELL_CP",True);
define("SELL_RES",True);
define("CRANNY_CAP",8000000);
define("ADV_TIME",86400/500);
define("TRAPPER_CAPACITY",100000);
define("xQUEST",0);


define("MAX_UNIT",60);
define("MAX_TRIBE",6);
// ***** Change storage capacity
define("STORAGE_MULTIPLIER","8000000");

define("STORAGE_BASE",800*STORAGE_MULTIPLIER);

// ***** World size
// Defines world size. NOTICE: DO NOT EDIT!!
define("WORLD_MAX", "100");

define("INCREASE_SPEED",500);

// ***** Beginners Protection
define("PHOUR", "3600");
define("PROTECTIOND",7200);
$timestoup = 0;
$fromstart=time()-OPENING;
if($fromstart>=42300){
$timestoup=floor($fromstart/42300);
}
define("PROTECTION",PROTECTIOND+($timestoup*PHOUR));
// ***** Trader capacity
// Values: 1 (normal), 3 (3x speed) etc...
define("TRADER_CAPACITY",8000000);

define("INCLUDE_ADMIN",True);

//////////////////////////////////
//   ****  SQL SETTINGS  ****   //
//////////////////////////////////

// ***** SQL Hostname
// example. sql106.000space.com / localhost
// If you host server on own PC than this value is: localhost
// If you use online hosting, value must be written in host cpanel
define("SQL_SERVER", "localhost");

// ***** Database Username
define("SQL_USER", "xtravian1_test");

// ***** Database Password
define("SQL_PASS", "test");

// ***** Database Name
define("SQL_DB", "xtravian1_test");


define("CP", "1");

// ***** PLUS
//Plus account lenght
define("PLUS_TIME",(3600*24*7));
//+25% production lenght
define("PLUS_PRODUCTION",(3600*24*7));
// ***** через сколько клеток начинается действие арены
define("TS_THRESHOLD",20);


//////////////////////////////////////////
//   ****  DO NOT EDIT SETTINGS  ****   //
//////////////////////////////////////////
define("ALLOW_ALL_TRIBE",false);
define("USRNM_MIN_LENGTH",3);
define("PW_MIN_LENGTH",4);
define("BANNED",0);
define("MULTIHUNTER",8);
define("ADMIN",9);
define("COOKIE_EXPIRE", 60*60*24*7);
define("COOKIE_PATH", "/");
define("HOMEPAGE", "http://test.xtravian.net/");
define("MAXLENGHT","15");
define("RADIUS",2); //делитель для максимального радиуса выпадения артов,например если карта 400 а делитель 2 то дальше 200х200 арт НЕ улетит

