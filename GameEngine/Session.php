<?php
###############################  E    N    D   ##################################
##              -= YOU MAY NOT REMOVE OR CHANGE THIS NOTICE =-                 ##
## --------------------------------------------------------------------------- ##
##  Developed by:  Brainiac & Wolfcruel                                        ##
##  License:       BrainianZ Project                                        ##
##  Copyright:     BrainianZ © 2011-2014. Skype brainiac.brainiac         ##
##                                                                             ##
#################################################################################
//ob_start("ob_gzhandler");
ob_start();
if(!class_exists("MYSQL_DB")){
    if(!empty($_GET['lang'])){
        if($_GET['lang']=='ru'){
            setcookie('lang', 'ru');
            $_COOKIE['lang']='ru';
        }elseif($_GET['lang']=='en'){
            setcookie('lang', 'en');
            $_COOKIE['lang']='en';

        }

    }

include("Database.php");
    if ((!isset($_COOKIE['lang'])  || empty($_COOKIE['lang'])) && isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        if (in_array($language, array('ru', 'ua', 'be'))) {
            setcookie('lang', 'ru');
            $language=$_COOKIE['lang']='ru';
        } else {
            setcookie('lang', 'en');
            $language=$_COOKIE['lang']='en';
        }
    }elseif(!isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])){
        setcookie('lang', 'en');
        $language=$_COOKIE['lang']='en';
    }else {
        $language = $_COOKIE['lang'];
    }
    include("Lang/" . $language . ".php");
}
include("Generator.php");
include("Form.php");



//print_r($_SERVER);


if (!empty($_GET['builder'])) {
    if ($_GET['builder'] == 'On') {
        setcookie('builder', 'On');
        $_COOKIE['builder'] = "On";
    } elseif ($_GET['builder'] == 'Off') {
        setcookie('builder', 'Off');
        $_COOKIE['builder'] = "Off";
    }
}

if (!isset($_COOKIE['builder'])) {
    setcookie('builder', 'Off');
    $_COOKIE['builder'] = "Off";
    $another = "Off";
    $todo = "отключения";
}elseif ($_COOKIE['builder'] == "On") {
    $another = "Off";
    $todo = "отключения";
} else {
    $another = "On";
    $todo = "включения";
}


class Session {

    public $logged_in = false;
    public $username, $uid,$unread,$link,$quest,$deleting, $access, $plus,$mescheck, $tribe,$silver,$face,$evasion,  $alliance, $gold, $brewery,$lastupdate,$sit,$sit1,$sit2,$protection,$protect,$cp,$password,$plust,$vvillages,$email,$refer,$heroD,$goldclub,$checker, $mchecker;
    public $bonus = 0;
    public $bonus1 = 0;
    public $bonus2 = 0;
    public $bonus3 = 0;
    public $bonus4 = 0;
    public $villages = array();
    public $right;

    function Session() {
        session_start();
        $_SESSION['numba']=0;
        $this->logged_in = $this->checkLogin();
        $this->SurfControl();
        if(isset($_GET['newdid']) && is_numeric($_GET['newdid'])) {
            $_SESSION['wid'] = $_GET['newdid'];
            //header("Location: ".$_SERVER['PHP_SELF']);
            //exit();
        }
    }

    public function Login($user) {
        global $generator;

        $_SESSION['username'] = $user;
        $this->logged_in = true;
        $_SESSION['checker'] = $generator->generateRandStr(3);
        $_SESSION['mchecker'] = $generator->generateRandStr(5);
        $this->PopulateVar();
        $_SESSION['dorf']=1;
        $_SESSION['wid'] = $this->villages[0];
        $this->sit = $_SESSION['sit'];
        $_SESSION['timestamp']=time()-30;


        header("Location: dorf1.php");
    }




    public function Logout() {
        $this->logged_in = false;
        unset($_SESSION);
        session_destroy();

    }

    public function changeChecker() {
        global $generator;
        $this->checker = $_SESSION['checker'] = $generator->generateRandStr(3);
        $this->mchecker = $_SESSION['mchecker'] = $generator->generateRandStr(5);
        $this->mescheck = $_SESSION['mescheck'] = $generator->generateRandStr(3);
    }

    private function checkLogin(){
        global $database;
        //print_r($_SESSION);
        if(isset($_SESSION['username']) && isset($_SESSION['sessid'])) {
            $mas=count($database->GetAOnline2($_SESSION['sessid']));

            if(!$mas){ $this->Logout();return false;}

            //Get and Populate Data
            $this->PopulateVar();

            //update database

            if(time()-$_SESSION['timestamp']>30){$_SESSION['timestamp']=time();
                $database->updateUserField($_SESSION['username'], "timestamp", time(),0);
            }

            return true;


        }
        return false;
    }

    private function PopulateVar() {
        global $database;

        $u = $database->getUserSes($_SESSION['username']);
        $this->username = $_SESSION['username'];
        $this->refer = $u['invited'];
        $this->password = $u['password'];
        $this->email = $u['email'];
        $this->uid = $_SESSION['id_user'] =  $u['id'];
        $this->access = $u['access'];
        $this->link=$_SESSION['dorf'];
        $this->plus = ($u['plus'] > time());
        $this->brewery = $u['brewery'];
        $this->deleting =$u['deleting'];
        $this->tribe = $u['tribe'];
        $this->lastupdate = $u['lastupdate'];
        $this->alliance = $_SESSION['alliance_user'] = $u['alliance'];
        $this->checker = $_SESSION['checker'];
        $this->mchecker = $_SESSION['mchecker'];
        $this->evasion = $u['evasion'];
        $this->sit1 = $u['sit1'];
        $this->quest = $u['quest'];
        $this->sit2 = $u['sit2'];
        $this->protection = $u['regtime']+PROTECTION;
        $this->protect = $u['protect'];
        $this->cp = floor($u['cp']);
        $this->gold  = $_SESSION['gold'] = $u['gold'];
        $_SESSION['ok'] = $u['ok'];

        $this->bonus1 = $u['b1'];
        $this->bonus2 = $u['b2'];
        $this->bonus3 = $u['b3'];
        $this->bonus4 = $u['b4'];
        $this->goldclub  = $u['goldclub'];
        $this->silver = $_SESSION['silver'] = $u['silver'];
        $this->plust = $u['plus'];
        $this->sit=$_SESSION['sit'];
        $this->heroD = $database->WowSoQueryH($this->uid);

        $this->unread=$database->NoticeMessage($this->uid);

        $vilmas=$database->getVillID2($this->uid);
        $this->villages = $vilmas[0];
        $this->vvillages = $vilmas[1];
        $this->updateHero();
       // print_r($vilmas[0]);
        if($u['advtime']+ADV_TIME<time()){
            $advs=floor((time()-$u['advtime'])/ADV_TIME);
            for($i=0;$i<$advs;$i++){
               $rvil= rand(0,count($vilmas[0])-1);
                       $tovil=$vilmas[0][$rvil];
                $database->addAdventure($tovil, $this->uid,1);
            }
            if($advs>0){

            $database->newAdvTime($this->uid,time()-(time()-$u['advtime']-($advs*ADV_TIME)));
            }
        }
if(!$this->sit){
    $this->right =array('s1'=>1,'s2'=>1,'s3'=>1,'s4'=>1,'s5'=>1,'s6'=>1);
}else{
    $this->right =array('s1'=>$_SESSION['s1'],'s2'=>$_SESSION['s2'],'s3'=>1,$_SESSION['s3'],'s4'=>$_SESSION['s4'],'s5'=>$_SESSION['s5'],'s6'=>$_SESSION['s6']);
}

    }

    private function SurfControl(){
        $page = $_SERVER['SCRIPT_NAME'];
        $pagearray = array("/index.php", "/login.php", "/activate.php", "/anmelden.php", "/first.php",'/password.php');
        if(!$this->logged_in) {
            if(!in_array($page, $pagearray) || $page == "/logout.php") {
                header("Location: login.php");
                exit();
            }
        } else {
            if(!$this->access && !in_array($page,array("/banned.php","/nachrichten.php"))){
                header("Location: banned.php");
            }
            if(in_array($page, $pagearray)) {
                header("Location: dorf1.php");
                exit();
            }

        }
    }
    function updateHero() {
        global $database,$hero_levels;
        //накидываем герою здоровье и уровни
        $timeisrunningout=time()-$this->heroD['lastupdate'];
        $herodone=$miracle1=$miracle2=$miracle3=0;
        $sql="";
        if($this->heroD['dead']==0 && $this->heroD['health']<100) {
            $speed=SPEED;
            if(SPEED>10000){$speed=10000;}
            $hill=$this->heroD['health']+($this->heroD['autoregen']*$speed/10*2.5/(24*60*60)*$timeisrunningout);

            if($hill>=1 and $this->heroD['health']<$hill){
                if($hill>100){$hill=100;}
                $miracle1=1;
                $sql.="`health`='".$hill."'";
                $this->heroD['health']=$hill;
            }
        }
        $i=0;

       if($this->heroD['level']<100){
            for($i=$this->heroD['level'];$i<=99;$i++) {
                if($this->heroD['experience'] < $hero_levels[$i+1]) {
                    break;
                }
            }
        }


        if($this->heroD['experience'] > $hero_levels[99] && $this->heroD['level']==100 ||$this->heroD['level']==99){$herodone=1;}
//проверяем героя на существование
        $hero=0;
        $vills=implode(",",$this->villages);
        $q1 = "SELECT SUM(u11) as sum1 from enforcement where `from` IN (".$vills.")";       // check if hero is send as reinforcement
        $he1 = $database->query($q1);
        $hero+=$he1[0]['sum1'];
        $q2 = "SELECT SUM(u11) as sum2 from units where `vref` IN (".$vills.")";   // check if hero is on my account (all villages)
        $he2 = $database->query($q2);
        $hero+=$he2[0]['sum2'];
        $q3 = "SELECT SUM(t11) as sum3 from prisoners where `from` IN (".$vills.")";   // check if hero is on traps (all villages)
        $he3 = $database->query($q3);
        $hero+=$he3[0]['sum3'];
        foreach($this->villages as $myvill){
            $hero+=$database->HeroNotInVil($myvill); // check if hero is not in village (come back from attack , raid , etc.)
        }
        if($this->heroD['dead']==0 && !$hero && !$this->heroD['revivetime']){ //если герой жив,но его нет
            $miracle3=1;
            $sql="`dead`='1',`health`='0'";
                $this->heroD['dead']=1;

            $this->heroD['health']=0;
        }elseif($this->heroD['dead']==1 && $hero){ //если герой мертв,но он есть
            $miracle3=1;
            $and=($miracle1)?",":" ";
            $sql.=$and;
            $sql.="`dead`='0'";
            $this->heroD['dead']=0;
        }
        if($this->heroD['level'] != $i && !$herodone) {

            //накидываем сразу в окно пользователя обновленные данные
           $this->heroD['level']=$i;

            $miracle2=1;
            $and=($miracle1 || $miracle3)?",":" ";
            $sql.=$and;
            $sql.="`level`='".$i."'";

        }
        if($miracle1 || $miracle2 || $miracle3){

            //exit;
            $and2=($miracle2 || $miracle1 || $miracle3)?",":" ";
            $sql.=$and2;
            $sql.="`lastupdate`='".time()."'";
            $database->modifyHeroS($sql,$this->heroD['heroid']);
        }


    }
}
$session = new Session;
$form = new Form;
//делаем работу по созданию объекта $session

$dorf1 = $dorf2 = '';
${'dorf'.$session->link}='active';
