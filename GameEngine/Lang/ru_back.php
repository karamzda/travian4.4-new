﻿<?php
###############################  E    N    D   ##################################
##              -= YOU MAY NOT REMOVE OR CHANGE THIS NOTICE =-                 ##
## --------------------------------------------------------------------------- ##
##  Developed by:  Brainiac & Wolfcruel & PaLLadiYs                            ##
##  License:       Brainianz Project                                           ##
##  Copyright:     Brainianz (c) 2011-2014. Skype brainiac.brainiac            ##
##                                                                             ##
#################################################################################
define("INCLUDED","");
define("INS0","Поменять права на папках (CHMOD)");
define("INS1","После установки");
define("INS2","Удалить папку install");
define("INS3","Поменять права у папок обратно на 644");
define("INS4","Команда Разработчиков не несет ответственности за последствия использования данного контента.");
define("INS5","Разработчики не несут ответственности за какой-либо вред приченный данными файлами.");
define("INS6","Код протестирован.Работоспособность и коррекность подтверждена.Вредные программы/файлы отсутствуют.");
define("INS7","Пользователь,приобревший данный скрипт является его собственником и несет полную ответственность за него.");
define("INS8","У вас нет прав удалять авторские права команды создавший данный скрипт.");
define("INS9","Не испугались?Тогда приступим:)");
define("INS10","Error creating constant.php check cmod.");
define("INS11","Месяц");
define("INS12","День");
define("INS13","Год");
define("INS14","Часы");
define("INS15","Мин");
define("INS16","Сек");
define("INS17","Название Сервера:");
define("INS18","Скорость Сервера:");
define("INS19","Скорость Войск:");
define("INS20","Размер торговцев (1 = 1x...):");
define("INS21","Размер карты:");
define("INS21M","25x25");
define("INS22","50x50");
define("INS23","100x100");
define("INS24","150x150");
define("INS25","200x200");
define("INS26","250x250");
define("INS27","300x300");
define("INS28","350x350");
define("INS29","400x400");
define("INS30","Главная Страница:");
define("INS31","Защита новичков");
define("INS31M","2 hours");
define("INS32","3 hours");
define("INS33","5 hours");
define("INS34","8 hours");
define("INS35","10 hours");
define("INS36","12 hours");
define("INS37","24 hours (1 day)");
define("INS38","48 hours (2 days)");
define("INS39","72 hours (3 days)");
define("INS40","120 hours (5 days)");
define("INS41","Размер складов:");
define("INS42","Начало действия арены(клетки):");
define("INS43","Админ");
define("INS44","Показ Админа в Статистике:");
define("INS45","True");
define("INS46","False");
define("INS47","SQL RELATED");
define("INS48","Hostname:");
define("INS49","Username:");
define("INS50","Password:");
define("INS51","DB name:");
define("INS52","Настройки Травиан Плюса");
define("INS53","Длительность Плюса:");
define("INS54","12 часов");
define("INS55","1 день");
define("INS56","2 дня");
define("INS57","3 дня");
define("INS58","4 дня");
define("INS59","5 дней");
define("INS60","6 дней");
define("INS61","7 дней");
define("INS62","+25% Длительность:");
define("INS63","12 часов");
define("INS64","1 день");
define("INS65","2 дня");
define("INS66","3 дня");
define("INS67","4 дня");
define("INS68","5 дней");
define("INS69","6 дней");
define("INS70","7 дней");
define("INS71","Продажа Ресурсов");
define("INS72","Да");
define("INS73","Нет");
define("INS74","Продажа Очков Культуры");
define("INS75","Да");
define("INS76","Нет");
define("INS77","Число ресов на продажу");
define("INS78","Цена ресов на продажу");
define("INS79","Цена Очков Культуры");
define("INS80","Сколько Очков Культуры");
define("INS81","Золото по умолчанию(у игроков)");
define("INS82","Реф.население");
define("INS83","Золото за реферала");
define("INS84","Настройки сервера");
define("INS85","Открытие Сервера:");
define("INS86","(Дату указывать в timestamp генерируется в самом верху;<br /> Генерация происходит по UTC+0!)");
define("INS87","Артефакты:");
define("INS88","(Дату указывать в timestamp)");
define("INS89","Чудо Деревни:");
define("INS90","(Дату указывать в timestamp) ");
define("INS91","Свитки:");
define("INS92","(Дату указывать в timestamp) ");
define("INS92M","Размер необходимых Очков Культуры:");
define("INS93","Много");
define("INS94","Мало");
define("INS95","Квесты");
define("INS96","Да");
define("INS97","Нет");
define("INS98","Максимальное Число Кэшируемых картинок для карты");
define("INS99","Максимальное Число Кэшируемых картинок для героя");
define("INS100","Качество изображений на карте ");
define("INS101","Моментальная постройка войск");
define("INS102","Да");
define("INS103","Нет");
define("INS104","Время аукциона");
define("INS105","Рейты Оазов");
define("INS106","На сколько нубка будет удлинятся каждые 12 часов(указать в секундах)");
define("INS107","В случае если у вас мало свободного места,учитывайте");
define("INS108","1000 картинок=~80МБ при привышении данного числа папки очищаются");
define("INS109","Для слабых серверов с малым кол-вом памяти можно экономить пространство занимемое кэшем карты на качестве карты");
define("INS110","1000 картинок=~2.60ГБ при привышении данного числа папки очищаются");
define("INS111","Отличное качество - 100,Выше Среднего - 75(занимаое место снижается в 3 раза)");
define("INS112","Войск в оазисе");
define("INS113","Внимание");
define("INS114",": This can take some time. Do not click, just wait till the next page has been loaded!");
define("INS115","Создание аккаунта мультихантера");
define("INS116","Ник:");
define("INS117","Пароль:");
define("INS118","Note: Rember this password! You need it for the ACP");


define("INS119","Error creating wdata. Check configuration or file.");
define("INS120","Create World Data");
define("INS121","Внимание");
define("INS122",": This can take some time. Do not click, just wait till the next page has been loaded!");
define("INS123","Thanks for installing BrainianZ.");
define("INS124","Please remove/rename the installation folder.");
define("INS125","All the files are placed. The database is created, so you can now start playing on your own Travian.");
define("INS126","Error importing database. Check configuration.");
define("INS127","Create SQL Structure");
define("INS128","Warning: This can take some time. Do not click, just wait till the next page has been loaded");
define("INS129","Приключений в день");





define("OK","Ok");
define("DIRECTION","ltr");
define("DIRECTION_2","left");
define("LOCATION_NAME","Global - EN");
define("CROPFINDER","Поиск зерновых клеток");
define("MAP","Карта");
define("MINIMAP","Мини карта ");
define("GO","Вперед");
define("GO_TO","Перейти");
define("PLEASE_WAIT","Подождите");
define("MAINMENU","Главное меню");
define("CATEGORY","'Категория");
define("EDITPROFILE","Изменит ьпрофиль");
define("COORDIANTES","Координаты");
define("POPULATION","Население");
define("WOOD","Дерево");
define("ABONDENDVALLY","Покинутая долина");
define("UNOCCUPIEDOASES","Свободный оазис");
define("UNOCCUPIEDOASIS","Свободный оазис");
define("OCCUPIEDOASES","Занятый оазис");
define("OCCUPIEDOASIS","Занятый оазис");
define("ABANDONEDVALLEY","Заброшенная долина");
define("BUILDRALLYPOINTTORAID","(постройте пункт сбора)");
define("PLAYER","Игрок");
define("TRIBE","Раса");
define("VILLAGE","Деревня");
define("ALLIANCE","Альянс");
define("SIDEINFO_ADVENTURES","Приключение");
define("SIDEINFO_AUCTIONS","Аукцион");
define("SIDEINFO_PROFILE","Профиль");
define("SIDEINFO_ALLIANCE","Альянс");
define("SIDEINFO_ALLY_FORUM","Форум альянса");
define("SIDEINFO_CHANGE_TITLE","Кликните дважды для переименования деревни");
define("SIDEINFO_CHANGEVIL_TITLE","Изменить название деревни");
define("SIDEINFO_CHANGEVIL_LABEL","Новое имя деревни");
define("SIDEINFO_CHANGEVIL_BTN","Ок");
define("HEADER_MESSAGE_NEW","Новое");

define("PRISONERS","Заключенные");
define("PRISONERSIN","Заключенные в");
define("PRISONERSFROM","Заключенные");
define("CLANG","Выберите Язык:");
//MAIN MENU
define("TRIBE1","Римляне");
define("TRIBE2","Германцы");
define("TRIBE3","Галлы");
define("TRIBE4","Природа");
define("TRIBE5","Натары");
define("TRIBE6","Римская Империя");
define("NO_PERMISSION","Недостаточно прав");
define("KARG","Золотой рудник");
define("HOME","Главная");
define("INSTRUCT","Справка");
define("ADMIN_PANEL","Админка");
define("RULES","Правила");
define("MULTIHUNTER_PAN","Мультих. панель");
define("CREATE_NAT","Создать Натар");
define("MASS_MESSAGE","Сообщение всем");
define("LOGOUT","Выйти");
define("XTRAVIANX","Травиан");
define("PROFILE","Профиль");
define("SUPPORT","Поддержка");
define("P","П");
define("L","л");
define("U","ю");
define("S","с");
define("UPDATE_T_10","Обновить  ТОП 10");
define("SYSTEM_MESSAGE","Сист. сообщение");
define("TRAVIAN_PLUS","Травиан <b><font color=\"#71D000\">П</font><font color=\"#FF6F0F\">л</font><font color=\"#71D000\">ю</font><font color=\"#FF6F0F\">с</font></b>");
define("CONTACT","Связь с нами!");
define("GAME_RULES","Правила игры");
define("HEADER_MESSAGES_NEW","New");
//MENU
define("REG","Регистрация");
define("FORUM","Форум");
define("CHAT","Чат");
define("IMPRINT","Распечатать");
define("MORE_LINKS","Больше");
define("LOGIN_FOR_GAME","Вход");



//PLUS
define("PLUS0","Плюс функции");
define("PLUS1","Описание");
define("PLUS2","Длительность");
define("PLUS3","Золото");
define("PLUS4","Действие");
define("PLUS5","У Вас ");
define("PLUS6","Золотых Монет");
define("PLUS7","Осталось:");
define("PLUS8","Дней");
define("PLUS9","Часов");
define("PLUS10","Минут");
define("PLUS11","Золото");
define("PLUS12","Активировать");
define("PLUS13","Продлить");
define("PLUS14","Недостаточно Золота");
define("PLUS15","Производство: Дерево");
define("PLUS16","Производство: Глина");
define("PLUS17","Производство: Железо");
define("PLUS18","Производство: Зерно");
define("PLUS19","1:1 Торговля с NPC торговцем");
define("PLUS20","Сейчас");
define("PLUS21","На Рынок!");
define("PLUS22","Размен Серебра и Золота");
define("PLUS23","Обменять");
define("PLUS24","Завершить все строительства и изучения.");
define("PLUS25","Завершить");
define("PLUS26","Купить");
define("PLUS27","Единиц Культуры");
define("PLUS28","Купить");
define("PLUS29","Каждого Ресурса");
define("PLUS30","День");
define("PLUS31","Золотой Клуб");
define("PLUS32","Функции Золотого Клуба:");
define("PLUS33","1.Фарм-Лист");
define("PLUS34","2.Торговые Пути");
define("PLUS35","3.Автоувод войск для столицы");
define("PLUS36","4.Поисковик 9-ок и 15-ек");
define("PLUS37","5.Архитектор");
define("PLUS38","6.3-ая Транспортировка");
define("PLUS39","Все функции Бесплатны!");
define("PLUS40","Весь Раунд");
define("PLUS41","Активирован");

//ERRORS
define("USRNM_EMPTY","(пустое имя пользователя)");
define("USRNM_TAKEN","(Имя уже используется)");
define("USRNM_SHORT","(мин. 3 символов)");
define("USRNM_CHAR","Допустимые символы:А-Я,A-Z,0-9");
define("PW_EMPTY","(Пустой пароль)");
define("PW_ER","Пароль неверен");
define("PW_SHORT","(мин. 4 символов)");
define("PW_INSECURE","(Простой пароль. Придумайте более сложный)");
define("EMAIL_EMPTY","(Пустой email)");
define("EMAIL_INVALID","(Неправильный email)");
define("EMAIL_TAKEN","(Email уже используется)");
define("TRIBE_EMPTY","<li>Выберите племя.</li>");
define("AGREE_ERROR","<li>Вы должны принять правила игры и общие условия и условия для того, чтобы зарегистрироваться.</li>");
define("LOGIN_USR_EMPTY","Введите имя.");
define("LOGIN_PASS_EMPTY","Введите паролб.");
define("EMAIL_ERROR","Электронный адрес не соответствует существующему");
define("PASS_MISMATCH","Пароли не совпадают");
define("ALLI_OWNER","Пожалуйста, назначьте владельца перед удалением");
define("SIT_ERROR","Уже установленно");
define("USR_NT_FOUND","Имя пользователя не существует.");
define("LOGIN_PW_ERROR","Пароль неправильный.");
define("WEL_TOPIC","Useful tips & information ");
define("ATAG_EMPTY","Тэг пуст");
define("ANAME_EMPTY","Название пустое");
define("ATAG_EXIST","Тэг занят");
define("ANAME_EXIST","Название занято");
define("NOT_OPENED_YET","Сервер еще не запущен.");
define("NAME_EMPTY","Please insert name");
define("NAME_NO_EXIST","There is no user with the name ");
define("SAME_NAME","You can't invite yourself");
define("ALREADY_INVITED"," already invited");
define("ALREADY_IN_ALLY"," already in this alliance");
//active
define("ACTIV1","Привет");
define("ACTIV2","Регистрация завершена. Через несколько минут на вашу почту будет выслано письмо с данными для входа в игру.<br /><br />

Письмо будет отправлено на указанный вами адрес:");

define("ACTIV3","Для активации вашего аккаунта введите код или пройдите по ссылке приложенной в письме.");
define("ACTIV4","Код Активации");
define("ACTIV5","Не пришло письмо?");
define("ACTIV6","Иногда письма расцениваются как Спам. Для отмены регистрации нажмите ");
define("ACTIV7","здесь");
define("ACTIV8","Вы можете удалить вашу регистрацию и попробывать зарегистрироваться снова.");
define("ACTIV9","Ваш акаунт был успешно активирован.</p><p class=\"f9\"> <a href=\"login.php\">Войти в игру</a>");
define("ACTIV10","Код активации неверен<br>или регистрация была удалена.");



define("CUR_PROD","Текущее производство");
define("NEXT_PROD","Производство на уровне ");

//BUILDINGS
define("B1","Лесопилка");
define("B1_DESC","На лесопилке идет производство древесины. С увеличением уровня развития здания увеличивается его производительность.");
define("B2","Глиняный карьер");
define("B2_DESC","На глиняном карьере добывают сырье глину. С развитием глиняного карьера увеличивается его производительность.");
define("B3","Железный карьер");
define("B3_DESC","На железных рудниках шахтеры добывают ценное сырье – железо. С развитием рудника увеличивается его производительность.");
define("B4","Ферма");
define("B4_DESC","На фермах выращивают зерно для обеспечения продовольствием населения. С развитием фермы увеличивается ее производительность.");

//DORF1
define("LUMBER","Древесина");
define("CLAY","Глина");
define("IRON","Железо");
define("CROP","Зерно");
define("LEVEL","Уровень");
define("CROP_COM","Потребление зерна");
define("DURATION","Время строительства");
define("PER_HR","в час");
define("PROD_HEADER","Производство");
define("MULTI_V_HEADER","Деревни");
define("ANNOUNCEMENT","Объявление");
define("GO2MY_VILLAGE","Войти в мою деревню");
define("VILLAGE_CENTER","Центр деревни");
define("FINISH_GOLD","Завершить все строительство и исследования в деревне за 2 золота?");
define("WAITING_LOOP","(очередь)");
define("HRS","ч.");
define("DONE_AT","будет завершено в");
define("CANCEL","отмена");
define("LOYALTY","Лояльность");
define("CALCULATED_IN","Создана за");
define("SEVER_TIME","Время сервера:");
define("MILISECS","мс");


//======================================================//
//================ UNITS - DO NOT EDIT! ================//
//======================================================//
define("U0","Герой");

//ROMAN UNITS
define("U1","Легионер");
define("U2","Преторианец");
define("U3","Империанец");
define("U4","Конный разведчик");
define("U5","Конница императора");
define("U6","Конница Цезаря");
define("U7","Таран");
define("U8","Огненная катапульта");
define("U9","Сенатор");
define("U10","Поселенец");

//TEUTON UNITS
define("U11","Дубинщик");
define("U12","Копейщик");
define("U13","Топорщик");
define("U14","Скаут");
define("U15","Паладин");
define("U16","Тевтонская конница");
define("U17","Стенобитное орудие");
define("U18","Катапульта");
define("U19","Вождь");
define("U20","Поселенец");

//GAUL UNITS
define("U21","Фаланга");
define("U22","Мечник");
define("U23","Следопыт");
define("U24","Тевтатский гром");
define("U25","Друид-всадник");
define("U26","Эдуйская конница");
define("U27","Стенобитное орудие");
define("U28","Требушет");
define("U29","Предводитель");
define("U30","Поселенец");

//NATURE UNITS
define("U31","Крыса");
define("U32","Паук");
define("U33","Змея");
define("U34","Летучая мышь");
define("U35","Кабан");
define("U36","Волк");
define("U37","Медведь");
define("U38","Крокодил");
define("U39","Тигр");
define("U40","Слон");

//NATARS UNITS
define("U41","Пикейщик");
define("U42","Дубинщик с шипами");
define("U43","Гвардеец");
define("U44","Ворон");
define("U45","Всадник с топором");
define("U46","Рыцарь Натаров");
define("U47","Боевой слон");
define("U48","Балиста");
define("U49","Принц Натаров");
define("U50","Поселенец");

//MONSTER UNITS
define("U51","Жестокий Инари");
define("U52","Адская Гончая");
define("U53","Жрец Иллюзий");
define("U54","Феникс");
define("U55","Катафрактарий");
define("U56","Красный Шен");
define("U57","Гидра");
define("U58","Разрушитель");
define("U59","Террор");
define("U60","Ромулы");
define("U99","Капканы");
// RESOURCES
define("R1","Lumber");
define("R2","Clay");
define("R3","Iron");
define("R4","Crop");


//ANMELDEN.php
define("NICKNAME","Логин");
define("INVITED","Пригласил(если есть)");
define("EMAIL","Email");
define("PASSWORD","Пароль");
define("ROMANS","Римляне");
define("TEUTONS","Германцы");
define("GAULS","Галлы");
define("NW","Северо-запад");
define("NE","Северо-восток");
define("SW","Юго-запад");
define("SE","Юго-восток");
define("RANDOM","случайно");
define("ACCEPT_RULES","Я согласен с условиями и <a href='rules.php' target='_blank'>правилами</a> игры.");
define("ONE_PER_SERVER","Каждый игром имеет право иметь только один аккаунт на сервере.");
define("BEFORE_REGISTER","Прежде чем зарегистрироваться,ознакомьтесь с <a href='../pravila.php' target='_blank'>правилами</a>");
define("MULTIBAN","Один компьютер-1 аккаунт.Мультиаккаунты подлежат бану!");
define("BUILDING_UPGRADING","Строительство:");
define("HOURS","ч.");
define("SIGN1","Регистрация Аккаунта");
define("SIGN2","Выберите Народ");
define("SIGN3","Выберите Регион");
define("SIGN4","Главная");
define("SIGN5","Регистрация");
define("SIGN6","Вход");
define("SIGN7","Активация Аккаунта");
define("SIGN8","Банк");

define("QST0","Задание");
define("QST1","Задача");
define("QST2","Лесопилка");
define("QST3","Построить лесопилку <b>5</b>го Уровня.");
define("QST4","Ферма");
define("QST5","Построить ферму <b>3</b>го Уровня.");
define("QST6","Железо и Глина");
define("QST7","Построить Глиняный Карьер и Железный Рудник до <b>4</b>го уровня.");
define("QST8","Главное Здание");
define("QST9","Построить Главное Здание <b>8</b>го уровня.");
define("QST10","Экономика");
define("QST11","Построить Склад и Амбар <b>4</b>го уровня,Рынок <b>1</b>го уровня.");
define("QST12","Милитаризация");
define("QST13","Построить Пункт сбора <b>1</b>го Уровня, Казарму <b>3</b>го уровня.");
define("QST14","Надежная защита");
define("QST15","Построить 100 единиц войска и Стену <b>8</b>го уровня.");
define("QST16","Первая Кровь");
define("QST17","Набрать <b>1000</b> Очков Атаки.");
define("QST18","Да Здравствует Социум!");
define("QST19","Посетить одну из наших групп");
define("QST20","Задания Кончились!");
define("QST21","На данный момент больше нет доступных квестов.Со временем их станет больше.Ну а пока,приятной игры!:)");
define("QST22","Отлично!Вот ваша награда:");
define("QST23","К Следующему Квесту!");
//ATTACKS ETC.
define("TROOP_MOVEMENTS","Перемещения войск:");
define("ARRIVING_REINF_TROOPS","Прибытие войск");
define("ARRIVING_REINF_TROOPS_SHORT","Приб.");
define("OWN_ATTACKING_TROOPS","Собственные атакующие войска");
define("ATTACK","Атака");
define("OWN_REINFORCING_TROOPS","Собственные прибивающие войска");
define("TROOPS_DORF","Войска:");


//LOGIN.php
define("COOKIES","Чтобы войти в игру необходимо разрешение Cookies в вашем браузере. Если вы не единственный пользователь компьютера, то при выходе из сеанса необходимо удалять Cookies.");
define("NAME","Имя");
define("PW_FORGOTTEN","Забыли пароль?");
define("PW_REQUEST","Вы можете запросить другой пароль на свой почтовый адрес.");
define("PW_GENERATE","Создать новый пароль.");
define("EMAIL_NOT_VERIFIED","Email nне принят!");
define("EMAIL_FOLLOW","Проследуйте по следующей ссылке чтобы активировать аккаунт.");
define("LOGIN_SERVER_START","Сервер откроется через:");

//404.php
define("NOTHING_HERE","Ничего тут!");
define("WE_LOOKED","Мы посмотрели 404 раза и ничего не нашли");

//TIME RELATED
define("CALCULATED","Создана за");
define("SERVER_TIME","Время сервера:");
//NOTICES
define("REPORT_SUBJECT","Тема:");
define("REPORT_ATTACKER","Нападение");
define("REPORT_DEFENDER","Оборона");
define("REPORT_RESOURCES","Ресурсы");
define("REPORT_FROM_VIL","из деревни");
define("REPORT_FROM_ALLY","from ally");
define("REPORT_SENT","Дата:");
define("REPORT_SENDER","Отпарвитель");
define("REPORT_RECEIVER","Receiver");
define("REPORT_AT","в");
define("REPORT_TO","To");
define("REPORT_SEND_RES","отправить ресурсы");
define("REPORT_DEL_BTN","delete report");
define("REPORT_DEL_QST","Are you sure that you want to delete the report?");
define("REPORT_WARSIM","combat simulator");
define("REPORT_ATK_AGAIN","return on the attack");
define("REPORT_TROOPS","Troops");
define("REPORT_REINF","Reinforecment");
define("REPORT_CASUALTIES","Casualties");
define("REPORT_INFORMATION","information");
define("REPORT_BOUNTY","Добыча");
define("REPORT_CLOCK","Time");
define("REPORT_UPKEEP","Upkeep");
define("REPORT_PER_HOURS","per hour");
define("REPORT_SEND_REINF_TO","send reinforcement to village");
define("REPORT_NO","There are no reports available.");
define("REPORT1"," scouts ");
define("REPORT2"," attacks ");
//MASSMESSAGE.php
define("MASS","Сообщение");
define("MASS_SUBJECT","Тема:");
define("MASS_COLOR","Цвет сообщения:");
define("MASS_REQUIRED","Все поля рекомендуемые");
define("MASS_UNITS","Картинки (единицы):");
define("MASS_SHOWHIDE","Показать/Скрыть");
define("MASS_READ","Read this: after adding smilie, you have to add left or right after number otherwise image will won't work");
define("MASS_CONFIRM","Confirmation");
define("MASS_REALLY","Do you really want to send MassIGM?");
define("MASS_ABORT","Aborting right now");
define("MASS_SENT","Mass IGM was sent");

 define("HEADER_MESSAGES","Сообщения");
define("HEADER_PLUS","Плюс");
define("HEADER_ADMIN","Админка");
define("HEADER_PLUSMENU","Плюс Меню");
define("HEADER_NOTICES","Отчеты");
define("HEADER_STATS","Статистика");
define("HEADER_MAP","Карта");
define("HEADER_DORF2","Центр Деревни");
define("HEADER_DORF1","Обзор деревни");
define("HEADER_GOLD","Золото");
define("HEADER_SILVER","Серебро");
define("HEADER_NIGHT","Ночь");
define("HEADER_DAY","День");
define("HEADER_NOTICES_NEW","New Report");
define("MULTI_RULES","Запрещается:<br/>Регистрировать более 1го аккаунта на один IP адрес.<br/>Нас не интересует,играете ли вы с братом/сестрой/с соседями/всем офисом: 1 IP - 1 Аккаунт.<br/>Запрещается регистрация аккаунтов с помощью друзей/левых IP,<br/>такие аккаунты будут отслеживаться по определенным критериям и штрафы будут очень суровыми:<br/> Например 50% от войска.<br />Будьте честны и имейте совесть играть честно.");

 //profile
define("PROFHEAD","Профиль Игрока");
define("ACC1","Сменить пароль");
define("ACC2","Старый Пароль");
define("ACC3","Новый Пароль");
define("ACC4","Смена E-mail");
define("ACC5","Введите вашу старую и новую почту.Вы получите на них 2 части кода которые должны будет ввести здесь");
define("ACC6","Старый email");
define("ACC7","Новый email");
define("ACC8","Заместители для этой учетной записи");
define("ACC9","Заместитель может зайти на ваш Аккаунт под своим паролем.");
define("ACC10","Имя Заместителя");
define("ACC11","Нет записи");
define("ACC12","<td class=\"note\" colspan=\"2\">Удалить заместителя вы можете нажав <img class=\"del\" src=\"img/x.gif\" title=\"del\" alt=\"del\"></td>");
define("ACC13","Удалить аккаунт");
define("ACC14","Тут вы можете удалить ваш аккаунт.Введите ваш пароль в качестве подтверждения вашего решения,удаление длится 24 часа.Отмена активна 12 часов.Администрация удаление не отменяет.");
define("ACC15","Удалить Аккаунт?");
define("ACC16","Введите ваш пароль:");
define("ACC17","Да");
define("ACC18","Нет");
define("ACC19","До удаления осталось:");
define("ACC20","Заместитель для других учетных записей");
define("ACC21","Заместитель");
define("SAVE","Сохранить");
define("OVERVIEW39","Мужской");
define("OVERVIEW40","Женский");
//menu prof
define("PROFM1","Обзор");
define("PROFM2","Профиль");
define("PROFM3","Ссылки");
define("PROFM4","Учетная запись");
define("PROFM5","Сессии");
define("PROFM6","Статус");
define("PROFM7","Кто");
define("PROFM8","Последняя активность");
define("PROFM9","Владелец");
define("PROFM10","Заместитель");
define("PROFM11","Дуал");
define("PROFM12","Вы");
//OVERVIEW
define("OVERVIEW1","Игрок");
define("OVERVIEW2","Детали");
define("OVERVIEW3","Описание");
define("OVERVIEW4","Ранг");
define("OVERVIEW5","Народ");
define("OVERVIEW6","Альянс");
define("OVERVIEW7","Деревни");
define("OVERVIEW8","Население");
define("OVERVIEW9","Возраст");
define("OVERVIEW10","Мужской");
define("OVERVIEW11","Женский");
define("OVERVIEW12","Пол");
define("OVERVIEW13","Страна");
define("OVERVIEW14","Редактировать профиль");
define("OVERVIEW15","Написать сообщение");
define("OVERVIEW16","Деревня");
define("OVERVIEW17","Название");
define("OVERVIEW18","Население");
define("OVERVIEW19","Координаты");
define("OVERVIEW20","Столица");
define("OVERVIEW21","Заблокирован");
define("OVERVIEW22","День Рождения");
define("OVERVIEW23","Январь");
define("OVERVIEW24","Февраль");
define("OVERVIEW25","Март");
define("OVERVIEW26","Апрель");
define("OVERVIEW27","Май");
define("OVERVIEW28","Июнь");
define("OVERVIEW29","Июль");
define("OVERVIEW30","Август");
define("OVERVIEW31","Сентябрь");
define("OVERVIEW32","Октрябрь");
define("OVERVIEW33","Ноябрь");
define("OVERVIEW34","Декабрь");
define("OVERVIEW35","Медали");
define("OVERVIEW36","Категория");
define("OVERVIEW37","Неделя");
define("OVERVIEW38","Код");
//define("OVERVIEW39","День");
//medals
define("MEDAL1","Атакующий ");
define("MEDAL2","Защищающийся ");
define("MEDAL3","Взлет ");
define("MEDAL4","Грабитель ");
define("MEDAL5","Попадание в топ 10 Атакующих и Защищающихся");
define("MEDAL6","Попадание в топ 3 Атакующих ");
define("MEDAL7","Попадание в топ 3 Защиющихся ");
define("MEDAL8","Попадание в топ 3 Взлета ");
define("MEDAL9","Попадание в топ 3 Грабителей ");
define("MEDAL10","Взлет ");
define("MEDAL11","Попадание в топ 3 Взлета ");
define("MEDAL12","Попадание в топ Атакующих ");
define("DNYA","Дня");
define("TIMES","раза");
define("MEDAL15","");
define("MEDAL16","");
define("MEDAL17","Герой ");
define("MEDAL18","Торговец ");
define("MEDAL19","подряд");
define("MEDAL20","День");
define("BONUS","Бонус");
define("STATISTIC1","Статистика Игроков");
define("STATISTIC2","Игроков не найдено");
define("STATISTIC3","Не найден");
define("STATISTIC4","Статистика Альянсов");
define("STATISTIC5","Альянс не найден");
define("STATISTIC6","Очки");
define("STATISTIC7","Статистика Альянсов(Атака)");
define("STATISTIC8","Статистика Альянсов(Защита)");
define("STATISTIC9","Топ");
define("STATISTIC10","Статистика героев");
define("STATISTIC11","Опыт");
define("STATISTIC12","Герой не найден");
define("STATISTIC13","Статистика Игроков(Атака)");
define("STATISTIC14","Статистика Игроков(Защита)");
define("STATISTIC15","Статистика Игроков(Римляне)");
define("STATISTIC16","Статистика Игроков(Германцы)");
define("STATISTIC17","Статистика Игроков(Галлы)");
define("STATISTIC18","Ресурсы");
define("STATISTIC19","Игроков");
define("STATISTIC20","Всего Зарегистрировано");
define("STATISTIC21","Активных игроков");
define("STATISTIC22","Игроков Онлайн");
define("STATISTIC23","Народы");
define("STATISTIC24","Народ");
define("STATISTIC25","Зарегистрировано");
define("STATISTIC26","Процент");
define("STATISTIC27","Чудо Света");
define("STATISTIC28","Игроки");
define("STATISTIC29","Альянсы");
define("STATISTIC30","Герои");
define("STATISTIC31","Общее");
define("STATISTIC32","Статистика");
define("STATISTIC33","или");
define("STATISTIC34","Назад");
define("STATISTIC35","Вперёд");
define("STATISTIC36","Статистика Игроков(Монстры)");
define("STATISTIC37","Защита");

//aliances
define("ALLIANCE1","Опции");
define("ALLIANCE2","Назначить на должность");
define("ALLIANCE3","Сменить название");
define("ALLIANCE4","Выгнать игрока");
define("ALLIANCE5","Изменить описание");
define("ALLIANCE6","Дипломатия");
define("ALLIANCE7","Пригласить игрока");
define("ALLIANCE8","Выйти из альянса");
define("ALLIANCE9","Новости");
define("ALLIANCE10","Событие");
define("ALLIANCE11","Дата");
define("ALLIANCE12","Подтвердите ваше решение паролем.");
define("ALLIANCE13","Дипломатия");
define("ALLIANCE14"," заключить союз");
define("ALLIANCE15"," заключить пакт");
define("ALLIANCE16"," Объявить войну");
define("ALLIANCE17","Подсказка");
define("ALLIANCE18","Для отобрежения дипломатических отношений вставьте в профиль альянса следующие теги: <span class=\"e\">[diplomatie]</span>, <span class=\"e\">[ally]</span>, <span class=\"e\">[nap]</span> и <span class=\"e\">[war]</span>");
define("ALLIANCE19","Ожидающие связи");
define("ALLIANCE20","
матриваемые предложения");
define("ALLIANCE21","Заключенные связи");
define("ALLIANCE22","Союз");
define("ALLIANCE23","Пакт о Ненападении");
define("ALLIANCE24","Война");
define("ALLIANCE25","Должность");
define("ALLIANCE26","Наделить правами");
define("ALLIANCE27","Массовый рассыл");
define("ALLIANCE28","Здесь вы можете наделить игроков вашего альянса правами.");
define("ALLIANCE29","Нападения");
define("ALLIANCE30","Здесь вы можете исключать игроков из вашего альянса.");
define("ALLIANCE31","Приглашенные");
define("ALLIANCE32","Союзы");
define("ALLIANCE33"," пригласил ");
define("ALLIANCE34"," отклонил приглашение ");
define("ALLIANCE35"," отозвал приглашения для ");
define("ALLIANCE36"," вступил в альянс");
define("ALLIANCE37","В альянсе нет мест.");
define("ALLIANCE38","Основатель");
define("ALLIANCE39","Альянс был основан игроком");
define("ALLIANCE40","сменил название альянса");
define("ALLIANCE41"," вступил в альянс");
define("ALLIANCE42"," изменил описание");
define("ALLIANCE43"," изменил права");
define("ALLIANCE44"," вышел из альянса");
define("ALLIANCE45"," предложил союз");
define("ALLIANCE46"," предложил ПоН");
define("ALLIANCE47"," объявил войну");
define("ALLIANCE48","Приглашение отправлено");
define("ALLIANCE49","Предлжение от вас уже рассматривается");
define("ALLIANCE50","Хакер,Да?");
define("ALLIANCE51","Альянс не существует");
// crop finder
define("FINDER1","Здесь вы сможете найти деревни с 9 или 15 зерновыми полями,используя различные фильтры.");
define("FINDER2","Поиск");
define("FINDER3","Центр");
define("FINDER4","Тип");
define("FINDER5","Оазис");
define("FINDER6","Занятость");
define("FINDER7","Только свободные");
define("FINDER8","Дистанция");
define("FINDER9","Позиция");
define("FINDER10","Владелец");
define("FINDER11","Поиск 9-ок и 15-ек");
define("FINDER12","Оазисы");
//otpravka figni
define("OTPRAV1","Отправить Войска");
define("OTPRAV2","Подкрепление");
define("OTPRAV3","Атака");
define("OTPRAV4","Набег");
define("OTPRAV5","Разведка");
define("OTPRAV6","Подкрепление для");
define("OTPRAV7","Атака на");
define("OTPRAV8","Набег на");
define("OTPRAV9","Разведка");
define("OTPRAV10","Разведать ресурсы и войска<br>");
define("OTPRAV11","Разведать защиту и войска");
define("OTPRAV12","Цель");
define("OTPRAV13","Случайно");
define("OTPRAV14","Происходит праздник в пивной.Только случайные цели.");
define("OTPRAV15","(Будет атаковано катапультами)");
define("OTPRAV16","Артефакт");
define("OTPRAV17","Прибытие");
define("OTPRAV18","Игрок находится под защитой новичков");
define("OTPRAV19","Нападение на деревни другого игрока приведет к потере защиты новичка.");
define("OTPRAV20","");
//karta vrode
define("BAN","Заблокирован");
define("KAR1","Покинутая долина");
define("KAR2","Свободный оазис");
define("KAR3","Захваченный оазис");
define("KAR4","Нет информации.");
define("KAR5","Основать поселение");
define("KAR6","Единиц Культуры");
define("KAR7","Поселенцев");
define("KAR8","требуется пункт сбора");
define("KAR9","Отправить войска");
define("KAR10","защита новичков");
define("KAR11","Отправить Торговцев");
define("KAR12","Добавить в Фармлист");
define("KAR13","Добавить в Фармлист.(Добавлено)");
define("KAR14","Добавить в Фармлист.(Лимит исчерпан)");
define("KAR15","Ресурсные точки");
define("KAR16","Центровать карту");
define("KAR17","требуется рынок");
define("KAR18","Карта");
define("KAR19","Поиск");
define("KAR20","Список избранных целей");
define("KAR21","Позиция");
define("KAR22","Последняя Атака");
define("KAR23","Потери");
define("KAR24","Добыча");
define("KAR25","");
define("KAR26","");
define("KAR27","");
define("KAR28","");
define("KAR29","");
define("KAR30","");
//Artefacti
define("ART1","Алмазное долото");
define("ART2","Дорийский молот");
define("ART3","Записки Архимеда");
define("ART4","Подкова Пегаса");
define("ART5","Аппиева дорога");
define("ART6","Колесница Гелиоса");
define("ART7","Письмо генерала");
define("ART8","Щит первого марафонца");
define("ART9","Плащ Бренна");
define("ART10","Доспехи Леонида");
define("ART11","Призыв Сципиона");
define("ART12","Записки Александра Великого");
define("ART13","Свиток для большого склада и большого амбара");
define("ART14","Строительный свиток Чудо Света");
define("ART15","(Доступ к Постройке)");
define("ART16","Этот артефакт защищает вашу деревню от катапульт и таранов. Благодаря ему все здания и стена становятся прочнее.");
define("ART17","Этот артефакт ускоряет передвижение ваших войск.");
define("ART18","Этот артефакт делает ваших разведчиков сильнее. Все разведчики, как и находящиеся в деревне, так и отправленные из неё на разведку в другую деревню, получают этот бонус. Кроме того, при нападении на вас, вы можете увидеть в пункте сбора тип вражеских войск.");
define("ART19","Артефакт уменьшает время обучения войск в казарме, конюшне и мастерской.");
define("ART20","Этот артефакт дает возможность построить большой склад и большой амбар. Артефакт необходим также и для улучшения обоих зданий.");
define("ART21","Артефакт необходимый для постройки Чуда Света");
//ss6Ilo4ki EBAN6IE:D
define("LINK1","Ссылки");
define("LINK2","Адрес");
//pravila anti-gad6I
define("RULES1","Правила игры");
define("RULES2","Данные правила игры являются правилами разработанные администрацией xTravian.ru . В случае блокировки учетной записи, а также в целях лучшего понимания запрещенных действий, следует обратиться к Мультихантеру, к §3.<br>
Обход правил игры является нарушением. Правила являются едиными и обязательны к соблюдению всеми игроками, в том числе и теми, которые собираются удалить или уже удаляют свои учетные записи.");
define("RULES3","§ 1 Учетная запись");
define("RULES4","§1.1. Каждый игрок имеет право владеть только одной учетной записью на одном и том же игровом сервере.");
define("RULES5","§1.2. Владельцем игровой учетной записи является тот игрок, чей адрес электронной почты прописан в настройках учетной записи. Изменить электронный адрес возможно в профиле учетной записи (Профиль » Учетная запись).");
define("RULES6","§1.3. Передача пароля от учетной записи лицам, играющим на этом же сервере, запрещена. Запрещены также и входы на учетные записи других игроков по паролю владельца. Любые подобные действия классифицируются как владение двумя и более учетными записями на одном игровом сервере. ");
define("RULES7","Игра нескольких игроков на одной учетной записи допускается при условии, что ни один из игроков не владеет и/или не играет другими учетными записями на том же игровом сервере. ");
define("RULES8","Запрещается также использовать одинаковые пароли на учетных записях при использовании общего компьютера и/ или замещении.");
define("RULES9","§1.4. Владелец учетной записи несет полную ответственность за всё происходящее с его учетной записью.");
define("RULES10","§ 2 Игровая этика");
define("RULES11","§2.1. Продажа и покупка учетной записи, единиц войск, ресурсов или действий игроков запрещены. Это относится также и к инвестированному в учетную запись времени.");
define("RULES12","§2.2. Оскорбления, унижения или прочие сомнительные отзывы о других игроках в любой форме и в любом месте в игре запрещены. Использование ненормативной лексики (в том числе и завуалированной) и любые угрозы, касающиеся реальной жизни, также запрещены.");
define("RULES13","§2.3. Запрещается подражание официальным структурам xTravian.ru, как и использование имен, названий, носящих оскорбительный и некорректный характер с точки зрения нравственных и политических норм.");
define("RULES14","§2.4. Составление игровых профилей и профилей альянсов допустимо только на русском и английском языках.");
define("RULES15","§2.5. Реклама любого рода, спам или цепочные письма запрещены.");
define("RULES16","§2.6. Публикация игровых сообщений, электронных писем игроков, мультихантеров или коммьюнити-менджеров запрещена.");
define("RULES17","§2.7. Запрещено призывать игроков к нарушению правил игры, удалению, передаче пароля, совместной игре на одной учетной записи, требовать передать учетную запись или указать заместителем.");
define("RULES18","§2.8. Использование ошибок в игре для извлечения какой-либо выгоды запрещено. Использование каких-либо программ, автоматизирующих и/или имитирующих деятельность игрока, а также изменяющих каким-либо образом внешний вид игры, запрещено. Исключением является использование графических пакетов.");
define("RULES19","§ 3 Административные постановления");
define("RULES20","§3.1. Способ наказания при нарушении правил определяют мультихантеры и/или администрация. Наказание в любом случае будет превосходить выгоду, полученную от нарушения. Без каких-либо исключений будут оштрафованы все учетные записи, которые имели отношение к нарушению правил. Потерянные во время блокировки сырье, постройки, деревни или войска не возмещаются. Потерянное вследствие блокировки золото и время доступа к xTravian.ru  Плюс не возмещается. Для игроков, покупающих золото, нет никаких привилегий при обработке писем и определении наказания.");
define("RULES21","§3.2. Мультихантер является единственным контактным лицом относительно нарушений правил. Игроки могут приводить аргументы в качестве доказательства своей правоты посредством отправки сообщений мультихантеру. В случае несогласия с решением мультихантера, игрок может обратиться к администрации.");
define("RULES22","Все вопросы о нарушениях и наказаниях команда xTravian.ru   решает только с владельцем учетной записи.");
define("RULES23","§3.3. Команда xTravian.ru   оставляет за собой право изменения правил игры в любое время.");

/*
|--------------------------------------------------------------------------
|   top_menu
|--------------------------------------------------------------------------
*/
		$lang['header'] = array (
							0 => 'Обзор деревни',
							1 => 'Центр деревни',
							2 => 'Карта',
							3 => 'Статистика',
							4 => 'Отчеты',
							5 => 'Сообщения',
							6 => 'Plus меню');

		$lang['buildings'] = array (
							1 => "Лесопилка",
							2 => "Глиняный карьер",
							3 => "Железный рудник",
							4 => "Ферма",
							5 => "Лесопильный завод",
							6 => "Кирпичный завод",
							7 => "Сталелитейный завод",
							8 => "Мукомольная мельница",
							9 => "Пекарня",
							10 => "Склад",
							11 => "Амбар",
							12 => "Кузница",

							14 => "Арена",
							15 => "Главное здание",
							16 => "Пункт сбора",
							17 => "Рынок",
							18 => "Посольство",
							19 => "Казарма",
							20 => "Конюшня",
							21 => "Мастерская",
							22 => "Академия",
							23 => "Тайник",
							24 => "Ратуша",
							25 => "Резиденция",
							26 => "Дворец",
							27 => "Сокровищница",
							28 => "Торговая палата",
							29 => "Большая казарма",
							30 => "Большая конюшня",
							31 => "Городская стена",
							32 => "Земляной вал",
							33 => "Изгородь",
							34 => "Каменотес",
							35 => "Пивоваренный завод",
							36 => "Капканщик",
							37 => "Таверна",
							38 => "Большой склад",
							39 => "Большой амбар",
							40 => "Чудо Света",
							41 => "Водопой",
							42 => "Башня",
							43 => "Ошибка",
 							44 => "&nbsp;(уровень&nbsp;");
$lang['desc'][1]=array(0=>'На лесопилке идет производство древесины. С увеличением уровня развития здания увеличивается его производительность.');
$lang['desc'][2]=array(0=>'На глиняном карьере добывают сырье глину. С развитием глиняного карьера увеличивается его производительность.');
$lang['desc'][3]=array(0=>'На железных рудниках шахтеры добывают ценное сырье – железо. С развитием рудника увеличивается его производительность.');
$lang['desc'][4]=array(0=>'На фермах выращивают зерно для обеспечения продовольствием населения. С развитием фермы увеличивается ее производительность.');
$lang['desc'][5]=array(0=>'На лесопильном заводе происходит дальнейшая обработка древесины. Производство древесины связано с уровнем развития завода и может быть увеличено на 25 процентов.');
$lang['desc'][6]=array(0=>'На кирпичном заводе производят кирпичи из глины. Производство глины связано с уровнем развития кирпичного завода и может быть увеличено на 25 процентов.');
$lang['desc'][7]=array(0=>'На чугунолитейном заводе происходит производство чугуна. Производство железа связано с уровнем развития чугунолитейного завода и может быть увеличено на 25 процентов.');
$lang['desc'][8]=array(0=>'На мукомольной мельнице зерно мелят в муку. Производство зерна связано с уровнем развития мукомольной мельницы и может быть увеличено на 25 процентов.');
$lang['desc'][9]=array(0=>'В пекарне из муки пекут хлеб. Производство зерна связано с уровнем развития пекарни и может быть увеличено, совместно с мельницей, на 50 процентов.');
$lang['desc'][10]=array(0=>'На складе хранится сырье: древесина, глина и железо. С развитием этого здания увеличивается его вместимость.');
$lang['desc'][11]=array(0=>'В амбаре хранятся излишки зерна. С развитием зернохранилища увеличивается его вместимость.');
$lang['desc'][12]=array(0=>'В плавильной печи кузницы можно улучшить оружие воинов. С развитием этого здания повышается максимальный уровень улучшения оружия.');
$lang['desc'][13]=array(0=>'В плавильных печах кузницы можно улучшить характеристики доспехов воинов. С развитием этого здания повышается максимальный уровень совершенствования доспехов.');
$lang['desc'][14]=array(0=>'На арене Ваши воины тренируют выносливость. С развитием арены увеличивается скорость передвижения Ваших войск на расстояния больше 20 полей.');
$lang['desc'][15]=array(0=>'В главном здании живут строители деревни. Чем выше уровень развития главного здания, тем быстрее ведется строительство.');
$lang['desc'][16]=array(0=>'В пункте сбора собираются солдаты Вашей деревни. Отсюда Вы можете послать подкрепление, организовать набег или захват деревни.');
$lang['desc'][17]=array(0=>'На рынке можно обменяться с другими игроками сырьем. С развитием рынка увеличивается количество доступных торговцев.');
$lang['desc'][18]=array(0=>'Посольство — это место встречи дипломатов. После 3-го уровня развития вы можете основать свой собственный альянс.');
$lang['desc'][19]=array(0=>'В казарме могут быть обучены пехотные войска. С развитием казармы уменьшается время обучения солдат.');
$lang['desc'][20]=array(0=>'В конюшне могут быть обучены все войска кавалерии. С развитием конюшни уменьшается время обучения солдат.');
$lang['desc'][21]=array(0=>'В мастерской можно создать осадные орудия, такие как катапульты и стенобитные орудия. С развитием мастерской уменьшается время производства.');
$lang['desc'][22]=array(0=>'В академии могут быть исследованы новые типы войск. С развитием академии увеличивается количество доступных для исследования типов войск.');
$lang['desc'][23]=array(0=>'При набеге на Вашу деревню ее жители прячут в тайник часть сырья из хранилищ. Это сырье не может быть украдено нападающим.');
$lang['desc'][24]=array(0=>'В ратуше Вы можете организовать для своих жителей великолепные праздники. Таким образом увеличивается количество Ваших единиц культуры.');
$lang['desc'][25]=array(0=>'Резиденция — это небольшой дворец, в котором останавливается король или королева Империи, когда он или она находится в деревне. Резиденция также предотвращает захват деревни.');
$lang['desc'][26]=array(0=>'Во дворце живет король или королева Империи. Дворец служит правителю для назначения столицы в его владениях и поэтому может быть построен только один дворец.');
$lang['desc'][27]=array(0=>'В сокровищнице хранятся сокровища Вашей Империи. В каждой сокровищнице есть место для одного артефакта. Кроме того, воздействие артефакта будет активировано только через 24 часа (для обычного сервера) или 12 часов (для скоростного сервера) после его захвата.');
$lang['desc'][28]=array(0=>'В торговой палате улучшают характеристики телег и разводят сильных лошадей. С развитием торговой палаты увеличивается максимальное количество сырья, которое способен перевезти один торговец.');
$lang['desc'][29]=array(0=>'Большая казарма позволяет обучать солдат. Однако стоимость обучения солдата здесь в три раза больше. Используя казарму и большую казарму можно обучать в два раза больше солдат за одно и то же время. Большую казарму нельзя построить в столице.');
$lang['desc'][30]=array(0=>'Большая конюшня позволяет обучать кавалерию. Однако, стоимость обучения здесь в три раза больше. Большую конюшню нельзя построить в столице.');
$lang['desc'][31]=array(0=>'Городская стена защищает поселение от нападений. Чем выше уровень стены, тем эффективнее её защита во время обороны вашей деревни.');
$lang['desc'][32]=array(0=>'Стена защищает Ваше поселение от нападений. Чем выше стена, тем легче защищать деревню от грабительских набегов и нападений войск Ваших противников.');
$lang['desc'][33]=array(0=>'Стена защищает Ваше поселение от нападений. Чем выше стена, тем легче защищать деревню от грабительских набегов и нападений войск Ваших противников.');
$lang['desc'][34]=array(0=>'Каменотес является экспертом в работах связанных с обработкой камней. С развитием этого здания увеличивается устойчивость всех построек в этой деревне.');
$lang['desc'][35]=array(0=>'В пивоварне варят для народа вкусные напитки. Они делают ваших воинов смелее и сильнее в сражениях. Однако есть и другая сторона медали, не столь радужная. Ваши вожди не столь красноречивы во вражеских деревнях, а катапульты попадают лишь по случайным целям. Пивоварня может быть построена только в столице.');
$lang['desc'][36]=array(0=>'С хорошо спрятанными ловушками около вашей деревни, неосторожные нападающие могут быть пойманы и, тем самым, не представлять для вас опасности. С каждым уровнем увеличивается количество доступных для создания ловушек..');

$lang['desc'][37]=array(0=>'В таверне вы можете нанять героя.Начиная с 10 уровня таверны вы можете захватывать оазисы. ');//тайник на хуй
$lang['desc'][38]=array(0=>'Большой склад вмещает в себя в 3 раза больше ресурсов, чем обычный склад.');
$lang['desc'][39]=array(0=>'Большой амбар вмещает в себя в 3 раза больше ресурсов, чем обычный амбар.');
$lang['desc'][40]=array(0=>'Чудо Света является венцом развития цивилизации. Только самые сильные и богатые империи могут построить это произведение искусства и успешно защищать его от нападений врагов.');
$lang['desc'][41]=array(0=>'Благодаря водопою улучшается самочувствие Ваших лошадей и, тем самым, ускоряется обучение конных войск и снижается количество потребляемого ими зерна.');

$lang['descs'][5]=array(0=>array(1,10),array(15,5));
$lang['descs'][6]=array(0=>array(2,10),array(15,5));
$lang['descs'][7]=array(0=>array(3,10),array(15,5));
$lang['descs'][8]=array(0=>array(4,5));
$lang['descs'][9]=array(0=>array(4,10),array(15,5));
$lang['descs'][12]=array(0=>array(22,1),array(15,3));
$lang['descs'][13]=array(0=>array(22,1),array(15,3));
$lang['descs'][14]=array(0=>array(16,15));
$lang['descs'][17]=array(0=>array(15,3),array(10,1),array(11,1));
$lang['descs'][19]=array(0=>array(16,1),array(15,3));
$lang['descs'][20]=array(0=>array(22,5),array(12,3));
$lang['descs'][21]=array(0=>array(22,10),array(15,5));
$lang['descs'][22]=array(0=>array(19,3),array(15,3));
$lang['descs'][24]=array(0=>array(15,10),array(22,10));
$lang['descs'][25]=array(0=>array(15,5));
$lang['descs'][26]=array(0=>array(18,1),array(15,5));
$lang['descs'][27]=array(0=>array(15,10));
$lang['descs'][28]=array(0=>array(17,20),array(20,10));
$lang['descs'][29]=array(0=>array(19,20));
$lang['descs'][30]=array(0=>array(20,20));
$lang['descs'][34]=array(0=>array(26,3),array(15,5));
$lang['descs'][35]=array(0=>array(11,20),array(16,10));
$lang['descs'][36]=array(array(16,1));
$lang['descs'][37]=array(0=>array(16,1),array(15,3));
$lang['descs'][38]=array(0=>array(15,10));
$lang['descs'][39]=array(0=>array(15,10));
$lang['descs'][41]=array(0=>array(20,20),array(16,10));

		$lang['fields'] = array (
							0 => '&nbsp;уровень',
							1 => 'Лесопилка уровень',
							2 => 'Глиняный карьер уровень ',
							3 => 'Железный рудник уровень',
							4 => 'Ферма уровень',
							5 => 'Наружняя строительная площадка',
							6 => 'Строительная площадка',
							7 => 'Строительная площадка пункта сбора');

		$lang['npc'] = array (
							0 => 'NPC торговец');

		$lang['upgrade'] = array (
							0 => 'Здание уже на максимальном уровне',
							1 => 'Максимальный уровень здания строится',
							2 => 'Здание будет снесено',
							3 => '<b>Стоимость</b> строительства до уровня&nbsp;',
							4 => 'Рабочие заняты.',
							5 => 'Не хватает еды. Развивайте фермы.',
							6 => 'Постройте склад.',
							7 => 'Постройте амбар.',
							8 => 'Достаточно ресурсов будет&nbsp;',
							9 => '&nbsp;в&nbsp;&nbsp;',
							10 => 'Улучшить до уровня&nbsp;',
							11 => 'сегодня',
							12 => 'завтра');

		$lang['movement'] = array (
							0 => 'в&nbsp;');

		$lang['troops'] = array (
							0 => 'нет',
							1 => 'Герой');


														//BUILD TIPA
define ("RESS","Для основания новой деревни вам необходима резиденция 10 или 20 уровня и 3 поселенца. Для захвата деревни вам необходима резиденция 10 или 20 уровня, Сенатор, Вождь или Предводитель.");
define("PALL","Во дворце живет правитель империи. Дворец служит ему для назначения столицы в его владениях и поэтому может быть построен только в одной деревне.");
define ("PALS","Для основания новой деревни вам необходим дворец 10 или 20 уровня и 3 поселенца. Для захвата деревни вам необходим дворец 10 или 20 уровня, Сенатор, Вождь или Предводитель.");
define ("STON","Каменотес является экспертом в работах связанных с обработкой камней. С развитием этого здания увеличивается устойчивость всех построек в этой деревне.");
define ("SOKR","В сокровищнице хранятся сокровища вашей империи. В каждой сокровищнице есть место для одного артефакта. Кроме того, воздействие артефакта будет активировано только через 24 часа (для обычного сервера) или 12 часов (для скоростного сервера) после его захвата.");
define("PIVO","В пивоварне варят для народа вкусные напитки. Они делают ваших воинов смелее и сильнее в сражениях. Однако есть и другая сторона медали, не столь радужная. Ваши вожди не столь красноречивы во вражеских деревнях, а катапульты попадают лишь по случайным целям. Пивоварня может быть построена только в столице.");

define("RATU","В ратуше вы можете организовать для своих жителей великолепные праздники. Таким образом увеличивается количество ваших единиц культуры.");
define("PERC","Процентов");
//GZ


define ("NGZ2", "Время строительства");
define ("NGZ3", "Время строительства на уровне");




//CTENA
define ("C1", " Стена Уровень");
define ("C2", " Стена защищает Ваше поселение от нападений. Чем выше стена, тем легче защищать деревню от грабительских набегов и нападений войск Ваших противников.");
define ("C3", " Текущий бонус к защите ");
define ("C4", " Бонус защиты на уровне ");
define ("C5", " Расходы на строительство до уровня ");
define ("C6", " Улучшить до уровня ");
define ("C7", " Земляной вал уровень");
define ("C8", "Изгородь уровень");
define ("C9", "Городской вал уровень");


//CKLAD
define ("CK0", " Склад Уровень");
define ("CK", " На складе хранится сырье: древесина, глина и железо. С развитием этого здания увеличивается его вместимость. ");
define ("CK2 ", " единиц сырья ");
define ("CK3 ", " Вместимость на уровне ");
define ("CK4 ", " единиц сырья ");
define ("CK5 ", " Расходы на строительство до уровня ");
define ("CK6 ", " Улучшить до уровня ");


//AMBAR
define ("AM", " Амбар Уровень ");
define ("AM1", " В амбаре хранятся излишки зерна. С развитием зернохранилища увеличивается его вместимость. ");
define ("AM3", " единиц сырья ");
define ("AM5", " единиц сырья ");
define ("AM6", " Расходы на строительство до уровня ");
define ("AM7", " Улучшить до уровня ");
define("CAPACITY","Вместимость");
define("CAPACITYA","Вместимость на уровне ");
//TAINIK
define ("TA", " Тайник Уровень ");
define ("TA1", " При набеге на Вашу деревню ее жители прячут в тайник часть сырья из хранилищ. Это сырье не может быть украдено нападающим. ");
define ("TA2", " Тайник ");
define ("TA3", " единиц сырья ");
define ("TA4", " Тайник на уровне ");
define ("TA5", " единиц сырья ");
define ("TA6", " Расходы на строительство до уровня ");
define ("TA7","Улучшить до уровня ");

//upgrade.php
define("UPG0","Здание на максимальном уровне.");
define("UPG1","Строится максимальный уровень.");
define("UPG2","Здание находится на сносе.");
define("UPG3","Расходы");
define("UPG4","на строительство до уровня");
define("UPG5","Все Рабочие заняты.");
define("UPG6","Все Рабочие заняты. (Очередь)");
define("UPG7","Нехватает еды. Развивайте фермы.");
define("UPG8","Развивайте Склад.");
define("UPG9","Развивайте Амбар.");
define("UPG10","Достаточно ресурсов Никогда");
define("UPG11","Улучшить до уровня");


//отправка войск
define("nap0","Подкрепление");
define("nap1","Атака: обычная");
define("nap2","Атака: набег");


define ("PY1", "Передвижения в деревню ");
define ("PY2", "Обзор ");
define ("PY3", "Отправка войск ");
define ("PY5", "Войска в деревне ");
define ("PY6", "Деревня ");
define ("PY7", "Собственные войска");
define ("PY8", "Войска ");
define ("PY9", "Содержание ");
define ("PY10", "в час ");
define ("PY11", "Отправить назад ");
define ("PY12", "Войска в других деревнях ");
define ("PY13", "Подкрепление Для ");
define ("PY14", "Отозвать ");
define ("PY15", "Передвижения ваших войск ");
define ("PY16", "Убежище");
define ("PY17", "Фарм Лист");
define ("PY18", "Оазис");
define ("PY19", "Войска в оазисах ");
define ("GOLDC","Золотой Клуб");
//KAZARMA
define ("KA", "Казарма Уровень");
define ("KA1", " В казарме могут быть обучены пехотные войска. С развитием казармы уменьшается время обучения солдат. ");
define ("KA2", "Казарма");
define ("KA3", "Обучение можно будет начать как построится казарма!");


//REZA
define ("RE", " Резиденция Уровень ");
define ("RE1", " Резиденция — это небольшой дворец, в котором останавливается король или королева Империи, когда он или она находится в деревне. Резиденция также предотвращает захват деревни. ");
define ("RE2", "Данная деревня является столицей");
define ("RE3", "Резиденция");
define ("RE4", "Обучение");
define ("RE5", "Очки культуры");
define ("RE6", "Одобрение");
define ("RE7", "Экспансия");
define("RE8","В целях расширения своей нации вам нужны единицы культуры. Они накапливаются с течением времени от ваших зданий, и быстрее на более высоких уровнях.");
define("RE9","Выбранная деревня:");
define("RE10","культуры в день");
define("RE11","Выработка всех деревень:");
define("RE12","Ваши деревни выработали");
define("RE13","единиц культуры. Чтобы основать или завоевать новую деревню вам нужно");
define("RE14","очков");
define("RE15","Нападая с сенататорам, вождями или предводителями лояльность деревни может быть ухудшена. Если  дойдет до нуля, деревня присоединяется к царству злоумышленника. Лояльность этой деревне в настоящее время ");
define("RE16","Деревни, основанные или завоеванные этой деревней");
define("RE17","Деревня");
define("RE18","Игрок");
define("RE19","Население");
define("RE20","Координаты");
define("RE21","Дата");
define("RE22","Ни одна другая деревня была основана или завоеван этой деревне еще​​.");
define("RE23","Обучение");
define("RE24","Период");
define("RE25","Готовые");
define("RE26","Обучить");
define("RE27","Для того, чтобы основать новую деревню вам нужен уровень 10 или 20 резеденции и 3 поселенцев. Для того, чтобы завоевать новую деревню вам нужен уровень 10 или 20 резеденции и сенатор, предводитель или вождь.");

//AKADEM
define ("AK", " Академия Уровень");
define ("AK1", " В академии могут быть исследованы новые типы войск. С развитием академии увеличивается количество доступных для исследования типов войск. ");
define ("AK2", "Академия");
define ("AK3", "Нет доступных исследований");
define ("AK4", "Действия");
define ("AK5", "Требования");
define ("AK6", "Показать еще");
define ("AK7", "Скрыть");
define ("AK8", "Исследовать");


//KYZNIZA
define ("KZ", " Кузница оружия Уровень ");
define ("KZ1", " В плавильной печи кузницы можно улучшить оружие воинов. С развитием этого здания повышается максимальный уровень улучшения оружия. ");
define ("KZ2", "Улучшить");
define ("KZ3", "Кузница доспехов");
define ("KZ4", "Действия");
define ("KZ5", "Улучшение");
define ("KZ6", "Продолжительность");
define ("KZ7", "Завершение");
define ("KZ8", "Увеличьте уровень<br>Кузницы доспехов");
define ("KZ9", "Проводится<br>улучшение");


//KON
define ("KO", " Конюшня Уровень ");
define ("KO1", "В конюшне могут быть обучены все войска кавалерии. С развитием конюшни уменьшается время обучения солдат.");
define("KO2","Нет изученных юнитов. Изучите в академии.");
define("KO3", "Обучение может начаться, когда большая конюшня будет завершена.");
define("KZ333", "Обучение может начаться, когда большая казарма будет завершена.");



define ("CURB", "Текущий бонус");
define ("CURBL", "Бонус на уровне");
define("NOTDONEU","Здание находится на стадии строительства.");
define("SPEEDB","Текущий бонус скорости");
define("SPEEDBL","Бонус скорости на уровне");

//GLAVNOE ZDANIE
define("gz0","Разрушение зданий:");
define("gz1","Если вам больше не нужно здание, вы можете заказать снос здания.");
define("gz2","Снос");
define("gz3","Завершить все строительные и научно-исследовательские заказы в этой деревне сразу за 2 золота?");
define("gz4","Разрушить здание");

//ратуша
define("ratusha0","Праздники могут начаться, когда ратуша будет завершена.");
define("ratusha1","Праздники");
define("ratusha2","Действие");
define("ratusha3","Праздник");
define("ratusha4","Выполняется");
define("ratusha5","Отрицательная добыча зерна, так что вы никогда не достигнете требуемых ресурсов");
define("ratusha6","Слишком мало");
define("ratusha7","ресурсов");
define("ratusha8","Выполнить");
define("ratusha9","Большой праздник");
define("ratusha10","Не хватает ресурсов");
define("ratusha11","очков культуры");
define("ratusha12","Длительность");
define("ratusha13","Конец");
define("ratusha14","Праздник");

//DVOREC
define ("DV", " Дворец уровень ");
define ("DV1", "Во дворце живет король или королева империи. Деревня, в которой построен дворец, может быть провозглашена столицей. Чем выше уровень дворца, тем тяжелее будет захватить деревню.");
define("dvrc0","Неверный пароль");
define("dvrc1","В деревне, в которой построен дворец, не может быть построена резиденция. Во дворце можно обучить трёх поселенцев или одного сенатора/вождя/предводителя на 10-ом, 15-ом и 20-ом уровне здания. Поселенцы необходимы для основания нового поселения, а ораторы — для завоевания деревни.");
define("dvrc2","Эта деревня является столицей");
define("dvrc3","Вы действительно хотите изменить столицу?");
define("dvrc4","Вы не можете отменить это!");
define("dvrc5","В целях защиты,введите пароль:");
define("dvrc6","Изменить");
define("dvrc7","Дворец строится");
define("dvrc8","Пароль:");
define("dvrc9","Название");
define("dvrc10","Количество");
define("dvrc11","Максимально");

//POSOLbSTVO
define("posl0","Альянс");
define("posl1","Тэг");
define("posl2","Имя");
define("posl3","в альянс");
define("posl4","Приглашения");
define("posl5","Принять");
define("posl6","Нет приглашений");
define("posl7","Создание альянса");
define("posl8","Создать");

//masterskaya
define("mastr0","Необходимо изучить юнитов");
define("mastr1","Обучить");
define("mastr2","Обучение");
define("mastr3","Продолжительность");
define("mastr4","Готовы");
define("mastr5","Количество");


//oasis
define ("oasis", "Оазисы");
define ("Namet", "Имя");
define ("Quantityе", "Колличество");
define ("Maxе", "Макс");
define ("Avaliablet", "Имеется");
define ("train1", "Обучение");
define ("TRA2", "продолжительность");
define ("TRA3", "Готово в ");
define ("Stable", "Конюшня");
define ("Workshop", "Мастерская");
define ("RallyPoint", "Пункт Сбора");
define ("Blacksmith", "Кузница оружия");
define ("Armoury", "Кузница доспехов");
define ("MainBuilding", "Главное здание");
define ("SendResouces", "Отправить ресурсы");
define ("Buyma", "Купить");
define ("Offerma", "Продать");
define ("ONPCtrading", "НПС торговец");
define ("ilior", "или");
define ("markVillages", "Деревня");
define ("markgo", "отправить");
define ("Constructnewbuilding", "Построить новое здание");
define ("SOCR", "В сокровищнице хранятся сокровища вашей империи.<br><br> В каждой сокровищнице есть место для одного артефакта. Для хранения маленького артефакта необходимо иметь сокровищницу 10-го уровня или 20-го уровня для большого артефакта.");
//Самая жопа avaliable
define ("avaAcademy", "Академия");
define ("avaAcademy1", "В академии могут быть исследованы новые типы войск. С развитием академии увеличивается количество доступных для исследования типов войск.");
define ("avaArmoury", "Кузница доспехов");
define ("avaArmoury1", "In the armoury melting furnaces your warriors armour is enhanced. By increasing its level you can order the fabrication of even better armour.");
define ("avaCityWall", "Городская стена");
define ("avaCityWall1", "Городская стена защищает поселение от нападений. Чем выше уровень стены, тем эффективнее её защита во время обороны вашей деревни.");
//messages
define ("mesotkogo", "От кого:");
define ("mestena", "Тема:");
define ("meskomy", "Кому:");

define("ITEM0","Шлем знаний");
define("IEFF0","+15% больше опыта для героя");
define("ITEM1","Шлем просвещения");
define("IEFF1","+20% больше опыта для героя");
define("ITEM2","Шлем мудрости");
define("IEFF2","+25% больше опыта для героя");
define("ITEM3","Шлем восстановления");
define("IEFF3","+10 здоровья в день");
define("ITEM4","Шлем здоровья");
define("IEFF4","+15 здоровья в день");
define("ITEM5","Шлем исцеления");
define("IEFF5","+20 здоровья в день");
define("ITEM6","Шлем гладиатора");
define("IEFF6","+100 единиц культуры в день");
define("ITEM7","Шлем трибуна");
define("IEFF7","+400 единиц культуры в день");
define("ITEM8","Шлем консула");
define("IEFF8","+800 единиц культуры в день");
define("ITEM9","Шлем наездника");
define("IEFF9","Время обучения войск в конюшне уменьшается на 10%.");
define("ITEM10","Шлем кавалерии");
define("IEFF10","Время обучения войск в конюшне уменьшается на 15%.");
define("ITEM11","Шлем тяжёлой кавалерии");
define("IEFF11","Время обучения войск в конюшне уменьшается на 20%.");
define("ITEM12","Шлем наёмника");
define("IEFF12","Время обучения войск в казарме уменьшается на 10%.");
define("ITEM13","Шлем воина");
define("IEFF13","Время обучения войск в казарме уменьшается на 15%.");
define("ITEM14","Шлем архонта");
define("IEFF14","Время обучения войск в казарме уменьшается на 20%.");
define("ITEM15","Легкие доспехи восстановления");
define("IEFF15","+20 здоровья в день");
define("ITEM16","Доспехи восстановления");
define("IEFF16","+30 здоровья в день");
define("ITEM17","Тяжелые доспехи восстановления");
define("IEFF17","+40 здоровья в день");
define("ITEM18","Лёгкие чешуйчатые доспехи");
define("IEFF18","Уменьшает урон на 4 единицы; +10 здоровья в день");
define("ITEM19","Чешуйчатые доспехи");
define("IEFF19","Уменьшает урон на 6 единиц; +15 здоровья в день");
define("ITEM20","Тяжелые чешуйчатые доспехи");
define("IEFF20","Уменьшает урон на 8 единиц; +20 здоровья в день");
define("ITEM21","Лёгкие латы");
define("IEFF21","+500 к силе героя");
define("ITEM22","Латы");
define("IEFF22","+1000 к силе героя");
define("ITEM23","Тяжелые латы");
define("IEFF23","+1500 к силе героя");
define("ITEM24","Лёгкая лорика сегментата");
define("IEFF24","+250 к силе героя; уменьшает урон на 3 единицы");
define("ITEM25","Лорика сегментата");
define("IEFF25","+500 к силе героя; уменьшает урон на 4 единицы");
define("ITEM26","Тяжелая лорика сегментата");
define("IEFF26","+750 к силе героя;уменьшает урон на 5 единиц");
define("ITEM27","Малая карта");
define("IEFF27","30% быстрее возвращение");
define("ITEM28","Карта");
define("IEFF28","40% быстрее возвращение");
define("ITEM29","Большая карта");
define("IEFF29","50% быстрее возвращение");
define("ITEM30","Малое знамя");
define("IEFF30","Войска между собственными деревнями на 30% быстрее.");
define("ITEM31","Знамя");
define("IEFF31","Войска между собственными деревнями на 40% быстрее.");
define("ITEM32","Большое знамя");
define("IEFF32","Войска между собственными деревнями на 50% быстрее.");
define("ITEM33","Малый штандарт");
define("IEFF33","Войска между участниками альянса на 15% быстрее.");
define("ITEM34","Штандарт");
define("IEFF34","Войска между участниками альянса на 20% быстрее.");
define("ITEM35","Большой штандарт");
define("IEFF35","Войска между участниками альянса на 25% быстрее.");
define("ITEM36","Маленький мешок вора");
define("IEFF36","10% бонуса грабежа");
define("ITEM37","Мешок вора");
define("IEFF37","15% бонуса грабежа");
define("ITEM38","Большой мешок вора");
define("IEFF38","20% бонуса грабежа");
define("ITEM39","Малый щит");
define("IEFF39","+500 к силе героя");
define("ITEM40","Щит");
define("IEFF40","+1000 к силе героя");
define("ITEM41","Большой щит");
define("IEFF41","+1500 к силе героя");
define("ITEM42","Малый рог Натар");
define("IEFF42","+20% силы в сражениях с Натарами. (Действует на героя и войска вместе с ним. Только в нападении)");
define("ITEM43","Рог Натар");
define("IEFF43","+25% силы в сражениях с Натарами. (Действует на героя и войска вместе с ним. Только в нападении)");
define("ITEM44","Большой рог Натар");
define("IEFF44","+30% силы в сражениях с Натарами. (Действует на героя и войска вместе с ним. Только в нападении)");
define("ITEM45","Короткий меч легионера");
define("IEFF45","+500 к силе героя; Каждый легионер получает +3 к атаке и защите.");
define("ITEM46","Меч легионера");
define("IEFF46","+1000 к силе героя; Каждый легионер получает +4 к атаке и защите.");
define("ITEM47","Длинный меч легионера");
define("IEFF47","+1500 к силе героя; Каждый легионер получает +5 к атаке и защите.");
define("ITEM48","Короткий меч преторианца");
define("IEFF48","+500 к силе героя; Каждый преторианец получает +3 к атаке и защите.");
define("ITEM49","Меч преторианца");
define("IEFF49","+1000 к силе героя; Каждый преторианец получает +4 к атаке и защите.");
define("ITEM50","Длинный меч преторианца");
define("IEFF50","+1500 к силе героя; Каждый преторианец получает +5 к атаке и защите.");
define("ITEM51","Короткий меч империанца");
define("IEFF51","+500 к силе героя; Каждый империанец получает +3 к атаке и защите.");
define("ITEM52","Меч империанца");
define("IEFF52","+1000 к силе героя; Каждый империанец получает +4 к атаке и защите.");
define("ITEM53","Длинный меч империанца");
define("IEFF53","+1500 к силе героя; Каждый империанец получает +5 к атаке и защите.");
define("ITEM54","Короткий меч императора");
define("IEFF54","+500 к силе героя; Каждая конница императора получает +9 к атаке и защите.");
define("ITEM55","Меч императора");
define("IEFF55","+1000 к силе героя; Каждая конница императора получает +12 к атаке и защите.");
define("ITEM56","Длинный меч императора");
define("IEFF56","+1500 к силе героя; Каждая конница императора получает +15 к атаке и защите.");
define("ITEM57","Лёгкое копьё Цезаря");
define("IEFF57","+500 к силе героя; Каждая конница Цезаря получает +12 к атаке и защите.");
define("ITEM58","Копьё Цезаря");
define("IEFF58","+1000 к силе героя; Каждая конница Цезаря получает +16 к атаке и защите.");
define("ITEM59","Тяжёлое копьё Цезаря");
define("IEFF59","+1500 к силе героя; Каждая конница Цезаря получает +20 к атаке и защите.");
define("ITEM60","Протазан фаланги");
define("IEFF60","+500 к силе героя; Каждая фаланга получает +3 к атаке и защите.");
define("ITEM61","Пика фаланги");
define("IEFF61","+1000 к силе героя; Каждая фаланга получает +4 к атаке и защите.");
define("ITEM62","Копьё фаланги");
define("IEFF62","+1500 к силе героя; Каждая фаланга получает +5 к атаке и защите.");
define("ITEM63","Короткий меч мечника");
define("IEFF63","+500 к силе героя; Каждый мечник получает +3 к атаке и защите.");
define("ITEM64","Меч мечника");
define("IEFF64","+1000 к силе героя; Каждый мечник получает +4 к атаке и защите.");
define("ITEM65","Длинный меч мечника");
define("IEFF65","+1500 к силе героя; Каждый мечник получает +5 к атаке и защите.");
define("ITEM66","Короткий лук тевтатцев");
define("IEFF66","+500 к силе героя; Каждый тевтатский гром получает +6 к атаке и защите.");
define("ITEM67","Лук тевтатцев");
define("IEFF67","+1000 к силе героя; Каждый тевтатский гром получает +8 к атаке и защите");
define("ITEM68","Длинный лук тевтатцев");
define("IEFF68","+1500 к силе героя; Каждый тевтатский гром получает +10 к атаке и защите.");
define("ITEM69","Трость друида-всадника");
define("IEFF69","+500 к силе героя; Каждый друид-всадник получает +6 к атаке и защите.");
define("ITEM70","Посох друида-всадника");
define("IEFF70","+1000 к силе героя; Каждый друид-всадник получает +8 к атаке и защите.");
define("ITEM71","Боевой посох друида-всадника");
define("IEFF71","+1500 к силе героя; Каждый друид-всадник получает +10 к атаке и защите.");
define("ITEM72","Лёгкое копьё эдуйского всадника");
define("IEFF72","+500 к силе героя; Каждая эдуйская конница получает +9 к атаке и защите.");
define("ITEM73","Копьё эдуйского всадника");
define("IEFF73","+1000 к силе героя; Каждая эдуйская конница получает +12 к атаке и защите.");
define("ITEM74","Тяжёлое копьё эдуйского всадника");
define("IEFF74","+1500 к силе героя; Каждая эдуйская конница получает +15 к атаке и защите.");
define("ITEM75","Палица дубинщика");
define("IEFF75","+500 к силе героя; Каждый дубинщик получает +3 к атаке и защите.");
define("ITEM76","Булава дубинщика");
define("IEFF76","+1000 к силе героя; Каждый дубинщик получает +4 к атаке и защите.");
define("ITEM77","Моргенштерн дубинщика");
define("IEFF77","+1500 к силе героя; Каждый дубинщик получает +5 к атаке и защите.");
define("ITEM78","Протазан копейщика");
define("IEFF78","+500 к силе героя; Каждый копьеносец получает +3 к атаке и защите.");
define("ITEM79","Пика копейщика");
define("IEFF79","+1000 к силе героя; Каждый копьеносец получает +4 к атаке и защите.");
define("ITEM80","Копьё копейщика");
define("IEFF80","+1500 к силе героя; Каждый копьеносец получает +5 к атаке и защите.");
define("ITEM81","Франциска топорщика");
define("IEFF81","+500 к силе героя; Каждый топорщик получает +3 к атаке и защите.");
define("ITEM82","Секира топорщика");
define("IEFF82","+1000 к силе героя; Каждый топорщик получает +4 к атаке и защите.");
define("ITEM83","Боевая секира топорщика");
define("IEFF83","+1500 к силе героя; Каждый топорщик получает +5 к атаке и защите.");
define("ITEM84","Легкий молот паладина");
define("IEFF84","+500 к силе героя; Каждый паладин получает +6 к атаке и защите.");
define("ITEM85","Молот паладина");
define("IEFF85","+1000 к силе героя; Каждый паладин получает +8 к атаке и защите.");
define("ITEM86","Тяжелый молот паладина");
define("IEFF86","+1500 к силе героя; Каждый паладин получает +10 к атаке и защите.");
define("ITEM87","Короткий меч тевтонского всадника");
define("IEFF87","+500 к силе героя; Каждая тевтонская конница получает +9 к атаке и защите.");
define("ITEM88","Меч тевтонского всадника");
define("IEFF88","+1000 к силе героя; Каждая тевтонская конница получает +12 к атаке и защите.");
define("ITEM89","Длинный меч тевтонского всадника");
define("IEFF89","+1500 к силе героя; Каждая тевтонская конница получает +15 к атаке и защите.");
define("ITEM90","Сапоги восстановления");
define("IEFF90","+10 здоровья в день");
define("ITEM91","Сапоги здоровья");
define("IEFF91","+15 здоровья в день");
define("ITEM92","Сапоги исцеления");
define("IEFF92","+20 здоровья в день");
define("ITEM93","Сапоги наёмника");
define("IEFF93","Увеличивает базовую скорость войска на 25% при расстоянии больше 20 полей.");
define("ITEM94","Сапоги воина");
define("IEFF94","Увеличивает базовую скорость войска на 50% при расстоянии больше 20 полей.");
define("ITEM95","Сапоги архонта");
define("IEFF95","Увеличивает базовую скорость войска на 75% при расстоянии больше 20 полей.");
define("ITEM96","Малые шпоры");
define("IEFF96","+3 поля в час");
define("ITEM97","Шпоры");
define("IEFF97","+4 поля в час");
define("ITEM98","Большие шпоры");
define("IEFF98","+5 поля в час");
define("ITEM99","Верховая лошадь");
define("IEFF99","Скорость передвижения героя увеличивается до 14 полей в час.");
define("ITEM100","Чистокровный жеребец");
define("IEFF100","Скорость передвижения героя увеличивается до 17 полей в час.");
define("ITEM101","Боевой конь");
define("IEFF101","Скорость передвижения героя увеличивается до 20 полей в час.");
define("ITEM102","Малая повязка 25%");
define("IEFF102"," единиц маскимально к воскрешению");
define("ITEM103","Большая повязка 33%");
define("IEFF103"," единиц маскимально к воскрешению");
define("ITEM104","Клеток");
define("IEFF104"," единиц животных максимально к поимке");
define("ITEM105","");
define("IEFF105","");
define("ITEM106","Мазь");
define("IEFF106","+1% к здоровью за единицу");
define("ITEM107","Ведро");
define("IEFF107","Моментальное воскрешение героя");
define("ITEM108","Книга мудрости");
define("IEFF108","Сбрасывает все параметры героя и позволяет распределить их по-новому.");
define("ITEM109","Скрижали права");
define("IEFF109","Увеличивается одобрение в деревне героя на 1% за одни скрижали права, но максимум до 125%. ");
define("ITEM110","Произведение искусства");
define("IEFF110","");

define("HEROI0","Атрибуты:");
define("HEROI1","Очки");
define("HEROI2","атрибуты в 1 тип");
define("HEROI3","Fighting Strength Combines with your heros defence and offense. The higher the Fighting strength the better in battle.");
define("HEROI4","Сила в сражениях:");
define("HEROI5","героя");
define("HEROI6","attribute power tooltip");
define("HEROI7","Сила включает в себя как атакующие, так и защитные параметры героя. Чем выше это значение, тем меньше повреждений получит герой на приключениях.");
define("HEROI8","Сила:");
define("HEROI9","from hero +");
define("HEROI10","bonus from items");
define("HEROI11","Fighting Strength");
define("HEROI12","Max");
define("HEROI13","Увеличивается параметр атаки всех единиц войск, участвующих с героем в атаке.");
define("HEROI14","Бонус атаки");
define("HEROI15","Увеличивается параметр защиты всех единиц войск, участвующих с героем в обороне.");
define("HEROI16","Бонус защиты");
define("HEROI17","Увеличивается производство ресурсов той деревни, в которой находится герой.");
define("HEROI18","Бонус ресурсов :");
define("HEROI19","Ресурсы:");
define("HEROI20","Distribute");
define("HEROI21","Распределение производства ресурсов героя:");
define("HEROI22","Все ресурсы");
define("HEROI23","Регенерация вашего героя:");
define("HEROI24","в день");
define("HEROI25","Здоровье:");
define("HEROI26","Ваш герой возродится в деревне");
define("HEROI27","Не хватает ресурсов для возрождения");
define("HEROI28","Возродить");
define("HEROI29","Герой будет готов восстановлен через ");
define("HEROI30","Время");
define("HEROI31","Вашему герою нужно");
define("HEROI32","опыта, чтобы достичь уровня");
define("HEROI33","Опыт:");
define("HEROI34","Ваш герой максимального уровня.");
define("HEROI35","Уровень героя");
define("HEROI36","Your Heros speed determines how many fields he travels an hour");
define("HEROI37","Скорость:");
define("HEROI38","Полей в час");
define("HEROI39","Герой будет всегда оставаться с войсками.");
define("HEROI40","Герой скроется в случае нападения на эту деревню.");
define("HEROI41","Скрыть героя:");
define("HEROI42","Герой находится в деревне:");
define("HEROI43","Производство героя:");
define("HEROI44","Базовая регенерация вашего героя: 20% в день.");
define("HEROI45","Сорость");
define("HEROI46","Текущая скорость вашего героя");
define("HEROI47","Текущее производство вашего героя:");
define("HEROI48","Параметры героя:");
define("HEROI49","Пожалуйста, сохраните изменения");
define("HEROI50","Доступно очков:");
define("HEROI51","Сохранить изменения");
define("HEROI52","Герой погиб. Его родной деревней была деревня ");
define("HEROI53","Ресурсы, необходимые для восстановления героя:");
define("HEROA0","Место");
define("HEROA1","Время");
define("HEROA2","Сложность");
define("HEROA3","Осталось");
define("HEROA4","Действие");
define("HEROA5","Нет приключений.");
define("HEROA6","Отправить");
define("HEROA7","Лес");
define("HEROA8","Поле");
define("HEROA9","Горы");
define("HEROA10","Море");
define("HEROF0","Голова");
define("HEROF1","Цвет волос");
define("HEROF2","Стрижка");
define("HEROF3","Уши");
define("HEROF4","Брови");
define("HEROF5","Глаза");
define("HEROF6","Нос");
define("HEROF7","Рот");
define("HEROF8","Борода");
define("HEROAC0","Фильтр для шлемов");
define("HEROAC1","Фильтр для предметов для тела");
define("HEROAC2","Фильтр для предметов левой руки");
define("HEROAC3","Фильтр для предметов правой руки");
define("HEROAC4","Фильтр для обуви");
define("HEROAC5","Фильтр для лошадей");
define("HEROAC6","Фильтр для маленьких повязок");
define("HEROAC7","Фильтр для повязок");
define("HEROAC8","Фильтр для клеток");
define("HEROAC9","Фильтр для свитков");
define("HEROAC10","Фильтр для мазей");
define("HEROAC11","Фильтр для ведёр");
define("HEROAC12","Фильтр для книг мудрости");
define("HEROAC13","Фильтр для скрижалей права");
define("HEROAC14","Фильтр для произведений искусства");
define("HEROAC15","Не найдено вещей.");
define("HEROAC16","Открыто");
define("HEROAC17","Закрыто");
define("HEROAC18","Мало серебра");
define("HEROAC19","Текущая цена:");
define("HEROAC20","Большая ставка:");
define("HEROAC21","Новая ставка:");
define("HEROAC22","Ставка");
define("HEROAC23","Аукцион");
define("HEROAC24","Ставок не найдено.");
define("HEROAC25","Время");
define("HEROAC26","Won");
define("HEROAC27","Конец аукциона");
define("HEROAC28","Отметить все");
define("HEROAC29","Нет информации.");
define("HEROAC30","Купить");
define("HEROAC31","Продажа");
define("HEROAC32","Предложения");
define("HEROAC33","Нет вещей");
define("HEROAC34","Для каждой единицы");
define("HEROAC35","У вас");
define("HEROAC36","предметы для продажи на аукционе (максимально допустимый в то же время является 5)");
define("HEROAC37","Конец продаж.");
define("HEROAC38","Не найдено.");
define("HEROAC39","Вы точно хотите продать?");
define("HEROAC40","Продать");
define("HEROAC41","Единиц");
define("HEROAC42","Изменить");
define("HEROAC43","Поставить");
define("HEROAC44","За единицу");
//восстановление пароля
define("REQ1","Восстановление пароля");
define("REQ2","Введите почту");
define("REQ3","Отправить код");
define("REQ4","Нажми сюда!");
define("REQ5","Забыл пароль?");
define("REQ6","Письмо отправлено.");



define("JR_SAVE","Сохранить");

define("JR_FOREST","лес");
define("JR_FIELD","поле");
define("JR_MOUNTAIN","горы");
define("JR_SEA","море");
define("JR_OK","Ок");

define("JR_CANCEL","Отменить");
define("JR_YES","Да");
define("JR_NO","Нет");
define("NUM","Нет.");
define("JR_NOTFINISHED","Не закончено");
define("JR_CONSUMPTION","Потребление");

define("JR_MOW","Медали недели");
define("JR_MEDALSCONFIRM","Выдать медали");
define("JR_MEDALSCONFIRMNOTE","Придется подождать");
define("JR_CONFIRM","Подтвердить");
define("GENDER","Пол");
define("GENDER0","Не указано");
define("GENDER1","Мужчина");
define("GENDER2","Женщина");
define("VILLAGES","Деревни");

define("search4", "Координаты");
define("plus_goldcount","Золото");
define("buygold_DESC_1","Локация");
define("buygold_DESC_2","Выберите вашу локацию:");
define("buygold_DESC_3","Изменить");
define("buygold_DESC_4","Пакеты золота:");
define("buygold_DESC_5","Изменить пакет");
define("buygold_DESC_7","Payment gateways");
define("buygold_DESC_8","Should be choosen");
define("buygold_DESC_9","Prices are marked final");
define("buygold_DESC_10","Payments will be done immediatelly");
define("buygold_DESC_11","Buy gold");
define("buygold_DESC2_1","Choose another package");
define("buygold_DESC_6","Step 2 - Choose a package");
define("first_desc1" , "Выберите расу");
define("first_desc2" , "Спасибо за активацию аккаунта");
define("first_desc3" , "Выберите расу, чтобы играть на этом сервере");
define("first_desc4" , "Мы рекомендуем галлов, если это ваша первая игра");
define("first_Gauls_desc1" , "Галлы");
define("first_Gauls_desc2" , "Спецификации:");
define("first_Gauls_desc3" , "Требуется мало времени.");
define("first_Gauls_desc4" , "С самого начала игры лучшая защита от разграбления.");
define("first_Gauls_desc5" , "Являются отличными и быстрыми войсками в игре.");
define("first_Gauls_desc6" , "The new players are the best choice.");
define("first_Roman_desc1" , "Рим");
define("first_Roman_desc2" , "Спецификации:");
define("first_Roman_desc3" , "Требуется много времени.");
define("first_Roman_desc4" , "Они могут быстрее развиваться,чем остальные расы.");
define("first_Roman_desc5" , "Мощная армия, но они драгоценны.");
define("first_Roman_desc6" , "Очень тяжело в начале игры и эта племя не рекомендуется для новых игроков.");
define("first_Teutons_desc1" , "Германцы");
define("first_Teutons_desc2" , "Спецификации:");
define("first_Teutons_desc3" , "Требуется много времени для агресивной игры.");
define("first_Teutons_desc4" , "Дешёвые войска и быстро строятся.");
define("first_Teutons_desc5" , "Подходит для агресивных и опытнах игроков.");
define("first_page_second_step_desc1" , "Выберите локацию для первой вашей деревни.");
define("first_page_tribe1","Римляне");
define("first_page_tribe2","Германцы");
define("first_page_tribe3","Галлы");
define("first_Gauls_chosen_desc1" , "Вы выбрали  и теперь я буду Вашим гидом.");
define("first_Romans_chosen_desc1" , "Вы выбрали  и теперь я буду Вашим гидом.");
define("first_Teutons_chosen_desc1" , "Вы выбрали  и теперь я буду Вашим гидом.");
define("first_page_second_step_desc2" , "Выберите расу");
define("first_page_second_step_desc3" , "Первая деревня появится тут или место может изменить свой выбор, нажав на карте.");
define("first_page_second_step_desc4" , "Вы начнете на северо-западе");
define("first_page_second_step_desc5" , "Вы начнете в северо-востоке");
define("first_page_second_step_desc6" , "Вы начнете на юго-западе");
define("first_page_second_step_desc7" , "Вы начнете на юго-востоке");
define("first_page_second_step_desc8" , "Создать деревню");
define("BUILDINGS","Здания");
define("CHANGING_YOUR_VILLAGE_NAME","Измените название деревни");
define("NEW_NAME","Новое имя");
define("IS_ON_ADVENTURE","На приключение");
define("MOVING_FROM","Переход от");
define("LN_TO","в");
define("SOME_CHANGES_DONE","Ваши изменения будут отменены. Согласны, чтобы оставить?");
define("MORE_INFO_25_BUTTON","Более подробную информацию о 25% дополнительного произдвоства ресурсов");
define("HEROFULLLVL","Ваш герой достиг максимального уровня");
define("LN_QUEST","Квест");
define("SHOW_HINTS_PANEL","Показать подсказки");
define("JR_CONSTRUCTION_PLANS_TITLE","План строительства Чудо Света");
define("JR_CONSTRUCTION_PLANS_VNAME","Строительный План деревни");
define("JR_CONSTRUCTION_PLANS_DESC","С этого древнего строительного плана вы будете в состоянии построить чудо света до уровня 50, чтобы построить дальше, ваш союз должен иметь не менее двух планов.");
define("JR_CONSTRUCTION_PLANS_RELEASE_TITLE","Строительный план");
define("JR_CONSTRUCTION_PLANS_RELEASE_DESC","
Много десятилетий назад племена Травиан были удивлены неожиданным возвращением осел. Это племя древней мудрости, несравненной величия и славы собирался нарушить свободные люди снова. Таким образом были сделаны ?? все усилия по подготовке окончательного войну против Натар и прогнать навсегда. Многие думали о так называемых  \"Чудо Света \", легендарный строительства, как одного решения. Было сказано, что конец будет сделать строители лидеров и победителей Травиан ");
define("JR_CONSTRUCTION_PLANS_RELEASE_TREASURY_DESC","However, you said that you will need a plan to build such a building. For this reason architects have designed clever ways to store them safely. After a while they could glimpse buildings like temples in many towns and cities - treasuries.");
define("JR_CONSTRUCTION_PLANS_RELEASE_MYTHS_DESC","Unfortunately, no one - not even the wise and experienced - have no idea where these plans could be found. The more people trying to find, the more seemed to be only myths.");
define("JR_CONSTRUCTION_PLANS_RELEASE_DISCOVERED_DESC","Today, however, this last secret will be discovered. Losses and past aspirations were vain as today scouts of several tribes have found the locations of these construction plans. Well guarded by the Natars, lie hidden in many oases scattered throughout the land travian. Only the bravest heroes will be able to capture such a plan and bring it home safely so that construction can begin.");
define("JR_CONSTRUCTION_PLANS_RELEASE_LINK_DESC","All information on the operation of the construction plans can be found on the servers.");
define("JR_HERE","Здесь");
define("JR_travian_TEAM","xTravian.ru Team");
define("JR_CONTINUE","продолжить");
define("JR_ATTACK_COMBAT_MODEL","Симулятор ");
define("JR_ATTACK_NORMAL","Атака");
define("JR_ATTACK_RAID","Набег");
define("JR_WARSIM_ATTACKER","Атакующий");
define("JR_WARSIM_DEFENDERS","Защищающийся");
define("JR_WARSIM_ATTACKCONFIG","Attack configuration");
define("JR_WARSIM_SIMULATE","Симуляция");
define("JR_POWERED_BY","Powered by PaLLadiYs");
define("JR_RIGHTS","Все права защищены");
define("JR_ZRAVIANX","PaLLadiYs");
define("JR_COPYYEAR","&copy; 2011 - 2014");
define("JR_NOT_ADMIN","Отказано в доступе!Вы не Администратор!!!");
define("JR_ART_BPTTL","Древний План строительства");
define("JR_ART_BPVN","Строительный план Чудо Света");
define("JR_ART_BPDES",'With this ancient construction plan you will able to build World Wonder to level 50. to build further, your alliance must hold at least two plans.');
define('WILLACTIN','Активируется в');
define("JR_CATEGORY","Категория");
define("JR_SELECT","Выбрать");
define("JR_GENERALQUESTIONS","Общие вопросы");
define("JR_ICANNOTLOGIN","Я не могу войти");
define("JR_ICANNOTREGISTER","I can not register");
define("JR_SEND","Отправить");
define("JR_REGISTERFIRST","Сперва зарегестрируйте аккаунт!");
define("JR_HEROATTRIBUTES","Качества героя");
define("JR_HEROAPPEARANCE","Внешний вид");
define("JR_HEROADVENTURE","Приключения");
define("JR_HEROAUCTION","Аукцион");
define("JR_HEROHEAD","Голова");
define("JR_HEROHAIRCOLOR","Цвет волос");
define("JR_HEROHAIRSTYLE","Прическа");
define("JR_HEROEARS","Уши");
define("JR_HEROEYEBROWS","Брови");
define("JR_HEROEYES","Глаза");
define("JR_HERONOSE","Нос");
define("JR_HEROMOUTH","Рот");
define("JR_HEROBEARD","Борода");
define("JR_HEROLOCATION","Расположение");
define("JR_HEROTIME","Время");
define("JR_HERODIFFICULTY","Сложность");
define("JR_HEROTIMELEFT","Осталось времени");
define("JR_HEROLINK","Ссылка");
define("JR_HEROBIDERROR1","You dont have enough silver coin to buy this item. You should at least have");
define("JR_HEROBIDERROR2","coin(s)");// todo изменить слово когда-нибудь
define("JR_HEROBIDERROR3","Не хватает серебра для повышения ставки.");
define("JR_HEROBIDERROR4","Имеется более высокая ставка.");
define("JR_HEROBIDERROR5","Аукцион завершен.");
define("JR_HEROBIDERROR6","Лот не существует.");
define("JR_HEROEVASION","When checked hero will hide when village attacked.");
define("JR_HERODEADORNOTHERE","Your hero is dead, or (s)he is not in this village, so you can not use this item.");
define("HEROISDEAD","Герой мертв");
define("JR_HEROBUYITEMS","Купить веши");
define("JR_HEROSELLITEMS","Продать вещи");
define("JR_HEROEXP","опыт");
define("JR_HEROEXPGROW","получено опыта");
define("JR_HEROEXPWILLBE","после использования, опыт будет");
define("JR_HEROCURRENTCP","Текущие очки культуры");
define("JR_HEROCPVALUE","Очки культуры");
define("JR_HEROCPAFTERUSE","Очков культуры будет после ъиспользования");
define("JR_HEROWANNAWEAR","Вы действительно хотите продать?");
define("JR_HEROTIU","Предметов использовано");

define("PRODUCTION_OVERVIEW","Production overview");
define("PRODUCTION_BONUS","Production bonus");
define("PRODUCTION_TOTAL_BONUS","Total bonus");
define("PRODUCTION_FIELD","Source");
define("PRODUCTION","Production");
define("PRODUCTION_BONUS2","Bonus");
define("PRODUCTION_HP","Hero production");
define("PRODUCTION_BALANCE","Interim balance");
define("PRODUCTION_P25","+25% Production");
define("PRODUCTION_INACTIVE","Inactive");
define("PRODUCTION_TOTAL","Total");
define("PRODUCTION_PROD_TOTAL","Total production per hour:");
define("PRODUCTION_PROD_S1","Clay Pit Level");
define("PRODUCTION_PROD_S2","Woodcutter Level");
define("PRODUCTION_PROD_S3","Iron Mine Level");
define("PRODUCTION_PROD_S4","Cropland Level");
define("PRODUCTION_PROD_S5","Hourly production including production bonus: ");
define("PRODUCTION_PROD_S6","The production bonus increases the production of the resource in <span class=\"underlined\">all</span> your villages.");
define("PRODUCTION_PROD_T1","Sawmill Level");
define("PRODUCTION_PROD_T2","Oasis");
define("PRODUCTION_PROD_T3","Brickyard Level");
define("PRODUCTION_PROD_T4","Iron Foundry Level");
define("PRODUCTION_PROD_T5","Grain Mill Level");
define("PRODUCTION_PROD_T6","Bakery Level");

define("OUT_OF_HOME","Отсутствует в родной деревне");
define("HERO_DIED","Герой погиб");
define("HERO_HEALTHY","Герой полностью цел");
define("ESCAPE_GORIZ","Escape");
define("CAPTCHA_1","Нажмите на изображение для нового");
define("PLUS_TIME_ENABLE","<p>Плюс аккаунт активен <b><span id=\"timer100\">%s</span></b> дней.</p>");
define("RENEW","Обновить");
define("USERS_ACTIVE","Активныйх аккаунтов");
define("USERS_ONLINE","Онлайн игроков");
define("USERS_TOTAL","Всего игроков");
define("dorf1_links","Список ссылок");
define("dorf1_villageNameBox_1","Нет рынка");
define("dorf1_villageNameBox_2","Постройте рынок");
define("dorf1_villageNameBox_3","Нет казармы");
define("dorf1_villageNameBox_4","Постройте казарму");
define("dorf1_villageNameBox_5","Нет конющни");
define("dorf1_villageNameBox_6","Постройте конюшню");
define("dorf1_villageNameBox_7","В этой деревне нет мастерской");
define("dorf1_villageNameBox_8","Нет мастерской");
define("dorf1_villageNameBox_9","На рынок");
define("dorf1_villageNameBox_10","В казарму");
define("dorf1_villageNameBox_11","Обучается");
define("dorf1_villageNameBox_12","В конюшню");
define("dorf1_villageNameBox_13","Обучается");
define("dorf1_villageNameBox_14","В мастерскую");
define("dorf1_villageNameBox_15","Обучается");
define("dorf1_villageNameBox_16","Изменить название деревни");
define("dorf1_villageNameBox2_1","Статистика деревни");
define("dorf1_villageNameBox2_2","Показать координаты");
define("dorf1_villageNameBox2_4","Спрятаны координаты");
define("dorf1_villageNameBox2_3","Деревни");
define("dorf1_villageNameBox2_5","Количество единиц культуры для основания/захвата следующей деревни:");
define("Link_desc_text_1" , "Подключите Травиан ПЛЮС, чтобы пользоваться ссылками.");
define("infobox_desc_text_1" , "Информация");
define("Questbox_desc_text_1" , "Здравствуйте");
define("Questbox_desc_text_2" , "Начать квест");
define("LVL",'уровень');
define("SIDE_I_1","Артефакты");
define("SIDE_I_2","");
define("SIDE_I_3","Aretefacts are released");
define("SIDE_I_4","<font style='font-size:11px;'>Building plans will be released in</font>");
define("SIDE_I_5","Building plans are released");
define("SIDE_I_6","Medals will be given in");
define("SIDE_I_7","");
define("UPGRADECOST","Стоимость улучшения на %s уровень");
define("SERVER_INFO","Информация");
define("MORE_ADVS_1","Вы должны отпарвить героя на приключение");
define("MORE_ADVS_2","несколько раз, чтобы иметь возможность покупать вещи");
define("WORLDWONDER","Чудо Света");
define("WAREHOUSE","Склад");
define("NO_FARM","Там нет фермы до сих пор!");
define("FARMLIST_FOOTER","<small>
Farm resources are calculated every ~30 seconds.<br>
Warehouse storage and Crop are equal in farms.<br>
The list is sorted by the time of creation of every item.<br>
</small>");
define("PROTECTION_TIME","До окончания защиты новичка <b><span id=\"timer1\">%s</span></b>");
define("ACCOUNT_DELETION","Ваш аккаунт будет удален через <b><span id=\"timer100\">%s</span></b>");
define("Elanat_dorf1","в родной деревне");
define("Ally_dorf1","Не в альянсе");
define("Elanat_dorf1_2","Информация");
define("DIRECT_LINK","Ссылки");
define("NO_PLUS_ESI","Для активации данной опции,подключите Травиан ПЛЮС.");
define("newmsg","Новые сообщения:");
define("newrpt","Новых отчетов:");

//logint4.4
define("logint40","Большая карта|| Активируйте Травиан ПЛЮС!");
define("logint41","Занято");
define("logint42","Свободный");
define("logint43","Дикая местность");
define("logint410","Центрировать карту");
define("logint411","Основать новую деревню");
define("logint412","Отчеты");
define("logint413","Игрок");
define("logint414","Раса");
define("logint415","Альянс");
define("logint416","Владелец");
define("logint417","Население");
define("logint418","Распределение");
define("logint419","Отправить войска");

//pluspalladiys
define("pluss0","Покупка золото другими способами (QIWI, WebMoney, PayPal) напишите");
define("pluss1","администратору");
define("pluss2","После покупки золото,пройдите в");
define("pluss3","Банк");
define("pluss4","Вы можете передавать золото на разные аккаунты всего проекта.");
define("pluss5","Купить золото");
define("pluss6","Функции ПЛЮСА");
define("pluss7","Описание");
define("pluss8","Длительность");
define("pluss9","Стоимость");
define("pluss10","Действие");
define("pluss11","Аккаунт");
define("pluss12","Осталось");
define("pluss13","дней");
define("pluss14","Офис обмена");
define("pluss15","Здесь вы можете перевести золото в серебро и наобарот.");
define("pluss16","Курс обмена");
define("pluss17","1 золото : 100 серебра");
define("pluss18","2000 серебра : 1 золота");
define("pluss19","обменять");
define("pluss20","ОШИИИИИИИИИИБКА");
define("pluss21","Купить золото");
define("pluss22","Функции");
define("pluss23","Заработать золото");
define("pluss24","Перевод золота и серебра");
define("pluss25","Активировать");
define("pluss26","Продлить");
define("pluss27","Мало золота");
define("pluss28","Осталось");
define("pluss29","дней");
define("pluss30","часов");
define("pluss31","минут");
define("pluss32","У вас");
define("pluss33","золота");
define("pluss34","Производство: Дерево");
define("pluss35","Производство: Глина");
define("pluss36","Производство: Железо");
define("pluss37","Производство: Зерно");
define("pluss38","1:1 Торговля с NPC торговцем");

//herohome
define("herohero0","Характеристики");
define("herohero1","Редактор Героя");
define("herohero2","Приключения");
define("herohero3","Аукцион");
define("herohero4","Купить вещи");
define("herohero5","Продать вещи");
define("herohero6","Случайно");

//ban_msg.tpl
define("yubnd","Вы заблокированы.Напишите письмо администратору");

//пункт сбора
define("punktxuev0","Ресурсы");
define("punktxuev1","Инфраструктура");
define("punktxuev2","Военные");
define("punktxuev3","Подтвердить");
define("punktxuev4","Начать приключение");
define("punktxuev5","Войск");
define("punktxuev6","Прибытие");
define("punktxuev7","Ваш герой отсутствует в этой деревне​​.");
define("punktxuev8","Ваш герой мёртв.");
define("punktxuev9","Необходимо построить пункт сбора.");
define("punktxuev10","Отправить Домой");
define("punktxuev11","Вернуть назад войска");
define("punktxuev12","Юниты");
define("punktxuev13","Время");
define("punktxuev14","Нехватает войск!");

//активация
define("activate0","Для игры  необходимо активировать учетную запись.");
define("activate1","Код активации:	");
define("activate2","Активировать и начать игру");
define("activate3","Ошиблись при вводе E-mail или Логина?");
define("activate4","Здесь вы можете удалить регистрацию и зарегистрироваться повторно.");
define("activate5","Ваш E-Mail:");
define("activate6","Ваш Логин:");
define("activate7","Ваш Пароль:");
define("activate8","Проверьте правильность введеных данных.");
define("activate9","Убедитесь что аккаунт не был активирован.");
define("activate10","Отправить");
define("activate11","Или");

//альянс
define("ally0","Приглашения");
define("ally1","Нет приглашения");
define("ally2","Приглашения для");
define("ally3","Выполнить");
define("ally4","Детали");
define("ally5","Должность");
define("ally6","Пользователи");
define("ally7","Покинуть");
define("ally8","Новости");
define("ally9","Нападения");
define("ally10","Опции");
define("ally11","Назначить");
define("ally12","Нет");
//фармлист
define("farmlist0","Деревня");
define("farmlist1","Подробнее");
define("farmlist2","");
define("farmlist3","Дистанция");
define("farmlist4","Войска");
define("farmlist5","Последний набег");
define("farmlist6","Добавить цель");
define("farmlist7","Отправить набеги");
define("farmlist8","Вы уверены, что хотите удалить этот список?");
define("farmlist9","Создать новый список");
define("farmlist10","Нет целей");
define("farmlist11","Имя");
define("farmlist12","Создать");
define("farmlist13","Это не ваш лист,сударь!");
define("farmlist14","Вы не можете добавлять в список деревню в которой находитесь.");
define("farmlist15","Не выбрано войск.");
define("farmlist16","Введите правильные координаты.");
define("farmlist17","Добавить слот");
define("farmlist18","Имя корма");
define("farmlist19","Выбрать цель");
define("farmlist20","Удалить");
define("farmlist21","Дикая местность");
define("CS","Место для строительства");


//dorf3
define("dorf0","Обзор");
define("dorf1","Ресурсы");
define("dorf2","Склад");
define("dorf3","Единицы культуры");
define("dorf4","Войска");
define("dorf5","Деревня");
define("dorf6","Нападения");
define("dorf7","Строительство");
define("dorf8","Обучение");
define("dorf9","Торговцев");
define("dorf10","Всего");
define("dorf11","ЕК в сутки");
define("dorf12","Праздники");
define("dorf13","Слоты");
define("dorf14","Собственные войска");

//makegold в плюсе
define("mkg0","Пригласи друзей и получи золото!");
define("mkg1","Как это сделать?");
define("mkg2","Отправьте вашему другу");
define("mkg3","Вашу персональную Реф-ссылку");
define("mkg4","Благоприятные условия для получения награды:");
define("mkg5","1.Как только население империи приглашенного вами игрока будет выше");
define("mkg6",",вы сможете забрать");
define("mkg7","золота кликнув по иконке");
define("mkg8","2.Вы не можете быть заместителем приглашенного игрока.");
define("mkg9","Приглашенные Игроки:");
define("mkg10","Игрок");
define("mkg11","Дата Регистрации");
define("mkg12","Население");
define("mkg13","Деревни");
define("mkg14","Забрать");
define("mkg15","Вы не пригласили еще новых игроков.");

//сообщения
define("MSG0","Тема");
define("MSG1","Игрок");
define("MSG2","Отправлено");
define("MSG3","Здесь пока нет сообщений.");
define("MSG4","Отметить все");
define("MSG5","Удалить");
define("MSG6","Входящие");
define("MSG7","Написать");
define("MSG8","Отправленные");
define("MSG9","Время отправления");
define("MSG10","Ответить");
define("MSG11","Вернуться");
define("MSG12","Получатель:");
define("MSG13","Отправить");

define("INSTANTCMLT","Завершить");

define("LOGOUT_TITLE","Выход выполнен!");
define("LOGOUT_DESC","Если ваш аккаунт не единственный на этом компьютере,из соображений безопастности рекомендуем:");
define("VERIFY_EMAIL","Email подтвержден.");
define("LOGOUT_H4","Спасибо за ваш визит!");
define("LOGOUT_LINK","Удалить cookies");
define("PREREG1","Скорость Войск");
define("PREREG2","Администатор");
define("PREREG3","Медали в ");
define("PREREG4","Сервер будет запущен: ");
define("PREREG5","До Старта регистрации осталось: ");

define("LOGIN_WELCOME","Добро пожаловать на сервер");
define("LOGIN_USERNAME","Логин");
define("LOGIN_PASSWORD","Пароль");
define("LOGIN_LOWRES_DESC","Низкое разрешение");
define("LOGIN_LOWRES_OPTION","");
define("LOGIN_LOWRES_NOTICE","");
define("LOGIN_PW_FORGOTTEN","Забыли пароль?");

define("LOGIN_PW_REQUEST","Восстановление пароля");
define("LOGIN_PW_EMAIL","Почта");
define("LOGIN_PW_BTN","Войти");

define("REGISTER_USERINFO","Регистрация");
define("REGISTER_USERNAME","Логин");
define("REGISTER_EMAIL","Почта");
define("REGISTER_PASSWORD","Пароль");
define("REGISTER_STARTER","");
define("REGISTER_SELECT_TRIBE","Выберите народ");
define("REGISTER_LOCATION","Местонахождение");
define("REGISTER_NE","Северо-Восточный");
define("REGISTER_NW","Северо-Западный");
define("REGISTER_SE","Юго-Восток");
define("REGISTER_SW","Юго-Запад");
define("REGISTER_RANDOM","Случайно");
define("REGISTER_MOREINFO","Правила");
define("REGISTER_CLOSED","Регистрация завершена.Вы не можете зарегестрироваться на сервере!");

//SOKR
define("sokr0","Владелец");
define("sokr1","Деревня");
define("sokr2","Альянс");
define("sokr3","Эффект");
define("sokr4","Бонус");
define("sokr5","Активность");
define("sokr6","Хранится в:");
define("sokr7","Сокровищнице");
define("sokr8","уровня");
define("sokr9","Захвачен");
define("sokr10","Ваши Артефакты");
define("sokr11","Название");
define("sokr12","У вас нет артефакта.");
define("sokr13","Ближайшие артефакты");
define("sokr14","Игрок");
define("sokr15","Расстояние");
define("sokr16","Ближайшие артефакты отсутствуют.");
define("sokr17","Сокровищница");
define("sokr18","Маленькие артефакты");
define("sokr19","Здесь нет артефактов.");
define("sokr20","Большие артефакты");
define("sokr21","Не Активен");
define("sokr22","Активен");

//taverna
define("TVRN0","Захваченные оазисы деревней");
define("TVRN1","");
define("TVRN2","Тип");
define("TVRN3","Лоялность");
define("TVRN4","Conquered");
define("TVRN5","Координаты");
define("TVRN6","Ресурсы");
define("TVRN7","Следующий оазис можно захватить на 10 уровне таверны.");
define("TVRN8","Следующий оазис можно захватить на 15 уровне таверны.");
define("TVRN9","Следующий оазис можно захватить на 20 уровне таверны.");
define("TVRN10","Другие оазисы ");
define("TVRN11","Хозяин");
define("TVRN12","Дерево");
define("TVRN13","Глина");
define("TVRN14","Железо");
define("TVRN15","Зерно");

//отчеты
define("rpts0","Войска");
define("rpts1","Потери");
define("rpts2","Пленные");
define("rpts3","Информация");
define("rpts4","из деревни");
define("rpts5","Подкрепление");
define("rpts6","Тема");
define("rpts7","Получены ресуры от");
define("rpts8","Отправлено");
define("rpts9","Оборона");
define("rpts10","в деревне");
define("rpts11","Нападение");
define("rpts12","разведывает");
define("rpts13","(новый)");
define("rpts14","Здесь нет отчетов.");

define("rpts15","Отправитель");
define("ot4m0","Все");
define("ot4m1","Войска");
define("ot4m2","Подкрепления");
define("ot4m3","Разное");
define("ot4m4","Торговля");
define("XUYXUYXUY","Отчеты");
define("REPORT_TODAY","Сегодня");
define("REPROT_YESTERDAY","Вчера");

//dorf3.php?id=3
define("len0","Склад");
define("len1","Деревня");

//рынок
define("MERCHANTS","Торговцы");
define("IMSEARCHING","Я ищу");
define("IMOFFERING","Я предлагаю");
define("OFFEREDONTHEMARKET","Предлагаемых на рынке");
define("rinok0","Торговые пути");
define("rinok1","Предложения на рынке");
define("rinok2","Предлагает");
define("rinok3","");
define("rinok4","Ищет");
define("rinok5","");
define("rinok6","Игрок");
define("rinok7","Время");
define("rinok8","Действие");
define("rinok9","Не хватает ресурсов");
define("rinok10","Не хватает торговцев");
define("rinok11","Принять");
define("rinok12","Нет свободных предложений на рынке");
define("rinok13","Отправить ресурсы");
define("rinok14","Каждый торговец может нести");
define("rinok15","единиц ресурсов");
define("rinok16","Не указаны координаты");
define("rinok17","Вы не можете отправить ресурсы,в деревню в которой вынаходитесь");
define("rinok18","Игрок заблокирован.Невозможно отправить ресурсы.");
define("rinok19","Ресурсы не указаны.");
define("rinok20","Введите название деревни или координаты.");
define("rinok21","Слишком мало торговцев.");
define("rinok22","Торговцы поступающие");
define("rinok23","Прибытие в");
define("rinok24","Ресурсы");
define("rinok25","В пути");
define("rinok26","Торговцы возвращаются");
define("rinok27","Предложение");
define("rinok28","Поиск");
define("rinok29","Максимальное время транспортировки:");
define("rinok30","часов");
define("rinok31","Только своим  ");
define("rinok32","Продажа");
define("rinok33","Собственные предложения");
define("rinok34","Предложения");
define("rinok35","Поиск");
define("rinok36","Альянс");
define("rinok37","часов.");
define("rinok38","Все");
define("rinok39","НПЦ выполнено.");
define("rinok40","Стоимость");
define("rinok41","Вернуться назад");
define("rinok42","Распределить");
define("rinok43","Обменять");
define("rinok44","ВЫ не можете обменять ресурсы в деревне с Чудом Света.");
define("rinok45","Начать");
define("rinok46","Нет активных торговых путей.");
define("rinok47","Пути в");
define("rinok48","изменить");
define("rinok49","Создать новые пути");
define("rinok50","Ресурсы");
define("rinok51","Время посылок");
define("rinok52","Поставки");
define("rinok53","Изменить пути");
define("rinok54","Пути идут в");
define("build129", "Цель");

define("anlm0","Введите пожалуйста логин");
define("anlm1","Введите пожалуйста e-mail");
define("anlm2","Введите пожалуйста пароль");

define("upgra0","Стоимость:");
define("upgra1","Все рабочие заняты.");
define("upgra2","Мало производства зерны,улучшите фермы!");
define("upgra3","Улучшите склад.");
define("upgra4","Улучшите амбар.");
define("upgra5","Ресурсов хватит в ");
define("upgra6","Построить");
define("upgra7","(Очередь)");
define("upgra8","Архитектор");
define("upgra9","(смтоимость:");

define("build437", "Строительство новых зданий");
define("build438", "В скором времени доступные здания");
define("build439", "Необходимо");
define("build440", "уровень");
define("build20", "Увод войск из-под нападения");
define("build21", "Активировать увод для вашей столицы.");

//world wonder
define("ww0","Чудо Света является столь замечательно, как это звучит. Это здание построено, чтобы выиграть сервер. Каждый уровень Чуда Света стоит сотни тысяч (даже миллионы) ресурсов, чтобы построить. ");
define("ww1","Вы должны иметь 1 уровень Чуда Света, чтобы быть в состоянии изменить название.");
define("ww2","Название Чуда Света:");
define("ww3","Вы не сможете изменить название Чуда Света после 10 уровня.");
define("ww4","Название изменено.");
define("ww5","Строится максимальный уровень.");
define("ww6","Расходы");
define("ww7","на строительство до уровня");
define("ww8","Для строительства Чудо Света выше 50-го уровня необходим 2-ой Свиток находящийся у СОАЛОВЦА");
define("ww9","Для строительства Чудо Света необходим  Свиток находящийся у вас или/и(для постройки выше 50го уровня) СОАЛОВЦА");
//kuznicaupgrade
define("kuzupg0","Полностью изучено ");
define("kuzupg1","Кузнице требуется развитие");
define("kuzupg2","Проводится изучение");

define("destroyvil","Деревня полностью уничтожена.");

//exchange gold and silver
define("exchange0","Пункт обмена");
define("exchange1","Обменять");
define("exchange2","Недостаточно золота");
define("exchange3","Значение было скорректировано");
define("exchange4","Не указано золото для обмена");
define("exchange5","Сейчас обменять");
define("exchange6","Купить золото сейчас");
define("exchange7","Необходимо как минимум 2000 серебра");
define("exchange8","Обменять сейчас серебро на золото");
define("exchange9","Пройдите");
define("exchange10","приключений для открытия аукциона!");

//заместители
define("accsit0","Отправлять набеги");
define("accsit1","Отправлять подкрепление другим игрокам");
define("accsit2","Отправлять ресурсы другим игрокам");
define("accsit3","Тратить золото");
define("accsit4","Читать и писать сообщения");
define("accsit5","Удалять сообщения и отчеты");
define("sendmsg","Отправить сообщение ");

//EVERYDAY QUEST
define("questday0","Все задания на сегоднешний день,выполнены ");
define("questday1","Квесты по-прежнему доступны");
define("questday2","Ежедневные задания");
define("questday3","Очки");
define("questday4","За сбор 25 очков вы можете получить одну из этих наград:");
define("questday5","5 приключений");
define("questday6","+5000 единиц культуры ");
if(!defined("HOWRES")){define("HOWRES",100000);} //делаем опасный трюк.
define("questday7",HOWRES." ресурсов одного случайного типа");
define("questday8","За сбор 50 очков вы можете получить одну из этих наград:");
define("questday9","+1 день  Travian PLUS ");
define("questday10","+1 день +25% производства древесины");
define("questday11","+1 день +25% производства глины");
define("questday12","+1 день +25% производства железа");
define("questday13","+1 день +25% производства зерна");
define("questday14","За сбор 75 очков вы можете получить одну из этих наград:");
define("questday15","+20 приключений");
define("questday16","+2 ведёр");
define("questday17","+1000 серебра");
define("questday18","За сбор 100 очков вы можете получить одну из этих наград:");
define("questday19","+50 золота");
define("questday20","+4000 серебра");
define("questday21","+50 приключений");
define("questday22","Получайте вознаграждения каждый день!");
define("questday23","(Сброс очков в 12:00 ч. Вознаграждение необходимо забрать заранее)");
define("questday24","Выполните приключения");
define("questday25","Получите медаль");
define("questday26","Пригласите игрока");
define("questday27","Постройте или захватите деревню");
define("questday28","Потратьте золото");
define("questday29","Захватите 1 оазис");
define("questday30","Посетите группу Вконтакте (кликабельно)");
define("questday31","Улучшите любого юнита до максимального уровня в кузнице");
define("questday32","Поздравляем! Вы набрали необходимое количество очков для получения вознаграждения!");
define("questday33","Набрано");
define("questday34","очков");
define("questday35"," Вы можете забрать вознаграждение за сбор");
define("questday36","очков сегодня.");
define("questday37","Награда выбирается случайным образом из этого списка:");
define("questday38","Вы собрали сегодня");
define("questday39","очков и за это вы получаете следующее вознаграждение:");
define("questday40","Ваша награда сегодня:");
define("questday41","");
define("questday42","");
define("questday43","");
define("questday44","");
define("questday45","");
define("REP_С1"," Освобождены <b>все</b> войска");
define("REP_С2","Резиденция/Дворец");
define("REP_С3"," Вместость тайника");
define("REP_С4","Стена");
define("REP_С5","Ваш герой не смог никого убить и не получил опыт");
define("REP_С6","Ваш герой получил");
define("REP_С7","Ваш герой снизил одобрение оазиса с");
define("REP_С8","до");
define("REP_С9","и получил");
define("REP_С10","Ваш Герой захватил этот оазис и получил");
define("REP_С11","");
define("REP_С12","Деревня не может быть захвачена");
define("REP_С13","Нехватает Единиц Культуры");
define("REP_С14","Жители деревни");
define("REP_С15","присоединились к вашей империи");
define("REP_С16","");
define("REP_С17","Резиденция или Дворец не Разрушены");
define("REP_С18","Деревня полностью разрушена");
define("REP_С19","Разрушено с уровня");
define("REP_С20","");
define("REP_С21","уровня");
define("REP_С22","");
define("REP_С23","Стена не была повреждена");
define("REP_С24","Стена рузрушена");
define("REP_С25","Здесь нет стены");
define("REP_С26","Подкрепление Атаковано в");
define("REP_С27","атакует");
define("REP_С28","Получено Подкрепление от");
define("REP_С29","Доставлено Подкрепление из");
define("REP_С30","Захваченный Оазис");
define("REP_С31","Ничего полезного не найдено");
define("REP_С32","");
define("REP_С33","Ваш герой захватил артефакт и получил");
define("REP_С34","Ваш герой захватил НЕактивный артефакт и получил");
define("REP_С35","Ваш герой не смог взять артефакт и получил");
define("REP_С36","У вас максимальное кол-во артефактов. Герой получил");
define("REP_С37","разведывает");
define("REP_С38","Получены ресурсы от");
define("REP_С39","Доставлены ресурсы из");
define("REP_С40","разрушено");
define("REP_С41","не было повреждено");
define("REP_С42","Одобрение было снижено с");
define("REP_С43","Пленение Беззащитных Зверей");
define("REP_С44","");
define("REP_С45","Оазис");
define("REP_С46","Ваш герой не выжил в приключении");
define("REP_С47","");
define("REP_С48","исследует");
define("REP_С49","");
define("REP_С50","");
define("REP_С51","");
define("REP_С52","");
define("REP_С53","");
define("REP_С54","");
define("REP_С55","");
define("REP_С56","");
define("REP_С57","");
define("REP_С58","");
define("REP_С59","");
define("REP_С60","");

//punktsbora
define("punktsb1","До прибытия:");
define("punktsb2","Прописать героя");
define("DAYS","д.");
define("changepos7", "Должность");
define("savebankgold","Остаток золота с прошлого раунда вы можете перевести через");
define("allgold", "Все золото");
define("adminhelp"," В случае возникновения вопросов обращайтесь к");
define("endround", "Золото зачисляется на аккаунт сразу после оплаты.");
define("endround1", "По окончанию раунда/удалении аккаунта золото зачисляется в Банк по формуле:");


define("quest1","Следующее приключение ");
define("quest2","Во время прохождения обучения вы уже ознакомились с приключениями. Как только ваш герой вернется в деревню, отправьте его в следующее приключение. За счёт опыта и добычи ваше развитие будет проходить значительно быстрее.");
define("quest3","Отправьте героя во второе приключение ");
define("quest4","30 единиц опыта");
define("quest5","Замечательно! Ваш герой уже в пути.
Совет: Чем сильнее ваш герой, тем меньше урона ему будет нанесено при прохождении приключений. Не забывайте добавлять часть свободных очков в параметр «Сила» вашего героя. Слабый герой может быстро погибнуть.");
define("quest6","Тайник");
define("quest7","Многие игроки обеспечивают своё существование за счёт других игроков. Они организовано проводят нападения на более слабых игроков, лишая их ресурсов. В начале игры вы находитесь под защитой, поэтому напасть на вас нельзя. Однако со временем защита пройдет, и вам придется следить за своими владениями. Постройте тайник, чтобы сохранять хотя бы часть ресурсов, делая нападения врагов менее эффективными.");
define("quest8","Постройте тайник в центре деревни ");
define("quest9","Теперь нападающим будет сложнее обворовать вас. Обратите внимание на окно «Информация», чтобы выяснить, сколько ещё времени вы будете находиться под защитой.");
define("quest10","Казарма");
define("quest11","Казарма — первая военная постройка. В ней вы можете обучать войска, необходимые для защиты вас и ваших союзников, а также для проведения военных операций.");
define("quest12","Постройте казарму ");
define("quest13","Ваша казарма построена.");
define("quest14","Уровень героя ");
define("quest15","Ваш герой становится сильнее с каждым новым уровнем. Перейдите на страницу характеристики героя и распределите доступные для вашего героя очки.");
define("quest16","Распределите доступные очки после достижения героем нового уровня. ");
define("quest17","Изменить распределение очков вы сможете в любое время. Для этого вам понадобится книга мудрости, которую можно найти в приключениях.");
define("quest18","Обучение войск ");
define("quest19","Настало время обучить ваши первые войска. В казарме первый вид пехоты уже доступен для обучения.");
define("quest20","Обучите в казарме ".round(250*xQUEST)." пехотинцев ");
define("quest21","The cornerstone for a glorious army has been laid! Always remember that you can still be attacked, even when you are not online.");
define("quest22","Изгородь ");
define("quest23","Для защиты вашей деревни вам следует также построить защитные сооружения. Возведение укреплений повышает основную защиту деревни, а также улучшает боевую мощь защитных войск.");
define("quest24","Постройте стену вокруг вашей деревни. ");
define("quest25","Теперь ваши войска в деревне защищены от внешних врагов значительно лучше.");
define("quest26","Нападение на оазис");
define("quest27","Найдите в окрестностях вашей деревни свободный оазис и нападите на него. Если в оазисе находятся дикие животные, отправьте в нападение героя с клетками. Вы сможете использовать пойманных животных в качестве защиты вашей деревни.");
define("quest28","Откройте на карте свободный оазис и нападите на него ");
define("quest29","".round(250*xQUEST)." пехотинца");
define("quest30","Только что вы начали свое первое нападение. Если вы хотите отменить его, вы можете сделать это в пункте сбора в течение первых 90 секунд.");
define("quest31","Десять приключений ");
define("quest32","Отправьте героя на новые приключения. После десяти пройденных приключений вы сможете принимать участие в аукционах, на которых можно торговать предметами с другими игроками.");
define("quest33","Пройдите 10 приключений ");
define("quest34","500 серебра");
define("quest35","Поздравляем, теперь вы можете использовать аукцион.");
define("quest36","Аукцион ");
define("quest37","Перейдите на аукцион и присмотритесь к интересующим вас предметам. Если вас не заинтересовало ни одно из предложений, вы можете сами выставить на торги ненужную вам вещь.");
define("quest38","Сделайте ставку или выставите на торги какой-нибудь свой предмет ");
define("quest39","Отлично, теперь вы знаете, как пользоваться аукционом.");
define("quest40"," Улучшение казармы");
define("quest41","Для строительства некоторых новых зданий необходимым условием является строительство или улучшение казармы. Кроме того, с развитием казармы вы уменьшаете время строительства ваших войск.");
define("quest42","Перейдите на страницу казармы и проведите её улучшение до 3-го уровня.");
define("quest43","Отлично. Ваши войска в настоящее время обучаются быстрее, и вы можете построить академию.");
define("quest44"," Академия");
define("quest45","Новые и сильные войска для вашей деревни можно исследовать в академии. Имейте в виду, что стоимость и условия некоторых войск очень высоки.");
define("quest46","Для строительства академии вам необходимо нажать на свободную строительную площадку, выбрать вкладку «Военные» и нажать «Построить» для начала строительства.");
define("quest47","Хорошо. Скоро вы узнаете больше о солдатах вашего племени.");
define("quest48","Исследование войск");
define("quest49","Ознакомьтесь с возможными исследованиями вашей академии. В игре имеются пехотинцы, кавалерия и осадные орудия. Некоторые войска лучше подходят для нападений, в то время как другие специализируются на защите.");
define("quest50","Чтобы исследовать новый тип войск, вам следует перейти в академию и нажать на «Исследовать» под интересующим вас типом войск.");
define("quest51","Один исследования, конечно, не достаточно.");
define("quest52","Кузница");
define("quest53","В плавильных печах кузницы можно улучшить характеристики как доспехов, так и оружия воинов. Также и кузница является одним из условий для строительства новых зданий.");
define("quest54","Для строительства кузницы вам необходимо нажать на свободную строительную площадку, открыть вкладку «Военные» и нажать на «Построить» для начала строительства здания.");
define("quest55","Идеально. Теперь вы можете лучше подготовить своих солдат.");
define("quest56","Улучшение войск");
define("quest57","Проводимые улучшения в кузнице повышают максимальный уровень эффективности защитных и атакующих параметров ваших воинов. Такие улучшения достаточно затратны. Именно поэтому проводить улучшения следует для того типа войск, которого у вас больше всего.");
define("quest58","Чтобы провести улучшение какого-либо войска, вам необходио перейти на страницу кузницы и нажать на «Улучшить» под тем типом войска, который вас интересует.");
define("quest59","Отлично, теперь ваше войско более подготовленно.");
define("quest60","Железный рудник ");
define("quest61","Заказать строительство железной рудника! Ваша главная цель по-прежнему высокая производительность ресурсов, так что бы вы могли быстро расти.");
define("quest62","Начните строительство железного рудника");
define("quest63","Один день на «+25%» производства каждого вида сырья");
define("quest64","Больше производства железа для вашей деревни. Вам поможет бонус на производство ресурсов, который увеличит производство соответствующего вида сырья.");
define("quest65","Больше ресурсов ");
define("quest66","Постройте еще одну лесопилку, глиняный карьер, железный рудник и ферму до 1-го уровня. Пока у вас активирован TRAVIAN Plus, у вас будет возможность поставить еще одно строительство в очередь.");
define("quest67","Постройте еще одну лесопилку, глиняный карьер, железный рудник и ферму до 1-го уровня. ");
define("quest68","Поздравляем! Ваша деревня растет и процветает...");
define("quest69","Амбар ");
define("quest70","Для хранения большего количества зерна вам понадобится амбар. Индикатор запасов зерна в амбаре вы найдете вверху страницы.");
define("quest71","Постройте амбар ");
define("quest72","Превосходно! С амбаром вы можете хранить значительно больше зерна.");
define("quest73","Всё на один ");
define("quest74","В начале игры лучше всего сконцентрировать всё свое внимание на производстве ресурсов. Улучшите все ресурсные поля до 1-го уровня.");
define("quest75","Постройте все лесопилки, глиняные карьеры, железные рудники и фермы до 1-го уровня ");
define("quest76","Вы существенно увеличили производство ресурсов в вашей деревне. Отличная работа. В скором времени мы сможем начать строительство новых зданий в центре деревни.");
define("quest77","На два ");
define("quest78","Снова увеличьте производство ресурсов, построив одну лесопилку, один глиняный карьер, один железный рудник и одну ферму до 2-го уровня");
define("quest79","Улучшите одно поле каждого ресурса до 2-го уровня ");
define("quest80","Хорошая работа!");
define("quest81","Рынок ");
define("quest82","На рынке вы можете обмениваться ресурсами с другими игроками. Если появится необходимость в каком-то виде сырья, вы сможете найти его на рынке.");
define("quest83","Постройте рынок ");
define("quest84","Ваш рынок готов, и теперь вы можете начать торговать с другими игроками.");
define("quest85","Торговля");
define("quest86"," Для прохождения этого задания есть два пути:

    Вы можете принять уже имеющееся предложение на рынке. Для этого нужно перейти на страницу рынка, выбрать вкладку «Покупка» и принять интересующее вас предложение;
    Вы можете сделать своё собственно предложение на рынке. Для этого нужно перейти на страницу рынка, выбрать вкладку «Продажа» и составить своё собственное предложение.
");
define("quest87","Создать свое предложение  или примите чужое");
define("quest88","Поздравляем с заключением вашей первой торговой сделки!");
define("quest89","Всё на два ");
define("quest90","Перед тем, как мы перейдем к строительству дорогих зданий в центре деревни, нам следует в очередной раз увеличить производство ресурсов.");
define("quest91","Постройте все лесопилки, глиняные карьеры, железные рудники и фермы до 2-го уровня ");
define("quest92","Поздравляем! Ваше производство развивается хорошо.");
define("quest93","Склад 3 ");
define("quest94","С увеличением производства ресурсов следует учитывать вместимость ваших складов. Также найденные героем ресурсы могут быстро переполнить склады и амбары вашей деревни. Следите за индикатором запасов сырья на ваших складах.");
define("quest95","Постройте склад 3-го уровня ");
define("quest96","Вместимость склада увеличена, и теперь в нём может храниться значительно больше ресурсов.");
define("quest97","Амбар 3 ");
define("quest98","Чем выше производство ресурсов, тем быстрее наполняются склады. Вам также следует увеличить и вместимость амбара.");
define("quest99","Постройте амбар 3-го уровня ");
define("quest100","В вашем амбаре теперь больше места для хранения зерна, что поможет избежать потерь ресурсов при переполнении.");
define("quest101","Мукомольная мельница ");
define("quest102","Мукомольная мельница увеличивает производство всех ферм в деревне, где она построена. Чтобы она себя окупила в полной мере, у вас должно быть в достаточной мере развито базовое производство.");
define("quest103","Постройте мукомольную мельницу 1-го уровня ");
define("quest104","Мукомольная мельницу 2-го уровня ");
define("quest105","Теперь у вас есть много свободного урожая для дальнейших построений. Есть также здания, которые увеличивают продукцию других ресурсов.");
define("quest106","Всё на пять");
define("quest107","Вам понадобится гораздо больше производство, чтобы избавить вас от долгого времени ожидания, пока вы не в состоянии позволить себе здания и поселенцев, необходимые для второй деревни. Улучшите все поля ресурсов до уровня 5.");
define("quest108","Улучшите все поля ресурсов до уровня 5.");
define("quest109","Бонус «+25%» на 24 часа на производство всех ресурсов.");
define("quest110","Молодцы, теперь у вас есть приличное производство.");
define("quest111","Обзор статистики");
define("quest112","В мире TRAVIAN вы соревнуетесь с тысячами других игроков. Ознакомьтесь со статистикой, чтобы выяснить, на каких позициях вы находитесь.");
define("quest113","Откройте статистику и сравните своё положение с положением остальных игроков ");
define("quest114","Кроме позиции игроков, в разделе статистики имеется много другой полезной информации. На вкладке «Топ-10» вы можете ознакомиться с лучшими игроками недели в разных категориях.");
define("quest115","Название деревни ");
define("quest116","Самостоятельно выбранное название деревни является знаком активно развивающегося игрока.");
define("quest117","Измените название деревни, нажав на «Изменить название» под табличкой с названием вашей деревни.");
define("quest118","33 единиц культуры");
define("quest119","Отлично! Этим самым вы сделали свой первый шаг, оставив след в истории TRAVIAN.");
define("quest120","Главное здание 3 ");
define("quest121","С улучшением главного здания стали доступны другие здания, которые вы можете построить в центре вашей деревни.");
define("quest122","Улучшить главное здание на 3 уровень.");
define("quest123","Отлично, Главное здание теперь позволяет построить несколько дополнительных зданий, которые вы только что разблокировали.");
define("quest124","Посольство");
define("quest125","Мир TRAVIAN очень опасен, поэтому альянсы играют очень важную роль в игре. Сильный альянс, состоящий из активных игроков, поможет вам давать отпор противникам. Чтобы вступить в альянс, необходимо посольство.");
define("quest126","Постройте посольство ");
define("quest127","Теперь вы можете принимать приглашения для вступления в альянс.");
define("quest128","Карта");
define("quest129","На карте игры вы можете найти приключения, оазисы или же владения других игроков. Внимательно ознакомьтесь с окрестностями вашей деревни, обращая внимание на ваших потенциальных врагов и союзников.");
define("quest130","Откройте карту в главном меню ");
define("quest131","Находятся ли рядом с вами сильные игроки или большие альянсы? Открыв карту, вы можете быстро ознакомиться с текущей ситуацией в вашем регионе.");
define("quest132","Чтение сообщений ");
define("quest133","Вы должны были получить игровое сообщение с некоторыми рекомендациями по игре. О непрочитанных сообщениях вы можете узнать по цифре над главным меню. Ознакомьтесь с полученным сообщением.");
define("quest134","Нажмите на «Сообщения» в самом верхнем меню и прочтите новое сообщение ");
define("quest135","Используйте сообщения для коммуникации с другими игроками. Не забывайте даже в пылу сражений о вежливости и соблюдении спокойствия.");
define("quest136","Золото");
define("quest137","Во время обучения вы уже ознакомились с тем, как можно ускорить процесс исследований или строительства зданий. В магазине с золотом вы узнаете для чего еще можно использовать золото.");
define("quest138","Ознакомьтесь с преимуществами, которые можно активировать за золото ");
define("quest139","Чтобы вы сразу могли воспользоваться некоторыми преимуществами, вам выдано немного золота. Вы можете распоряжаться этим золотом на свое усмотрение.");
define("quest140","Альянс ");
define("quest141","Сотрудничество играет в TRAVIAN очень важную роль. Игроки,
которые помогают друг другу, объединяются в альянсы. Для того, чтобы вступить в уже существующий альянс, вам нужно получить приглашение, обратившись за ним к руководству интересующего вас альянса.");
define("quest142","Вступите в альянс или создайте свой собственный ");
define("quest143","Хорошее начало. Существование без альянса едва ли возможно. Поэтому очень важно находиться в альянсе, в котором взаимная помощь является одним из приоритетов.");
define("quest144","Главное здание 5 ");
define("quest145","Для открытия возможности строительства новых зданий следует увеличить уровень главного здания.");
define("quest146","Постройте главное здание 5-го уровня ");
define("quest147","Отлично! Помимо того, что вы увеличили скорость строительства зданий, вы также открыли возможность строительства резиденции.");
define("quest148","Резиденция ");
define("quest149","Для основания нового поселения вам понадобятся поселенцы, которых вы можете обучить во дворце или резиденции. Кроме того, если в деревне имеется дворец, то вы сможете провозгласить её столицей.");
define("quest150","Постройте резиденцию или дворец ");
define("quest151","Это здание необходимо для того, чтобы построить новую деревню или завоевать. Ее уровень ограничивает количество возможных деревень.");
define("quest152","Единицы культуры ");
define("quest153","Для захвата существующих или основания новых деревень вам понадобятся единицы культуры. На странице обзора резиденции, на вкладке «Культура», вы сможете ознакомиться с необходимым количеством единиц культуры для захвата или основания новой деревни.");
define("quest154","Перейдите в здание резиденции, откройте вкладку «Культура» ");
define("quest155","В списке деревни можно также увидеть текущее состояние возможных новых деревень и количество пропавших без вести единиц культуры.");
define("quest156","Склад 7");
define("quest157","Перед основанием новой деревни вам следует увеличить вместимость склада. Для обучения поселенцев и строительства новых зданий текущей вместимости в скором времени будет недостаточно.");
define("quest158","Улучшите уровень имеющегося склада до 7-ми.");
define("quest159","Отлично,  емкость склада должна быть достаточно в течение некоторого времени.");
define("quest160","Резиденция или дворец 10");
define("quest161","Для основания нового поселения вам понадобятся 3 поселенца, которых вы можете обучить во дворце или резиденции. Перейдите на вкладку «Обучить» резиденции или дворца, чтобы узнать о необходимых уровнях здания. Из каждой деревни можно основать две или три новые деревни. Сейчас для основания новой деревни вам осталось лишь обучить трёх поселенцев и набрать необходимое количество единиц культуры.");
define("quest162"," Улучшите уровень вашей резиденции или дворца до 10-ти.");
define("quest163","167 едениц культуры ");
define("quest164","Из каждой деревни можно основывать только  2-3 деревни. Все, что для новой деревни не хватает сейчас 3 поселенца и много очков культуры.");
define("quest165","Три поселенца");
define("quest166"," Как уже было сказано ранее, для основания новой деревни вам потребуются три поселенца. Защищайте ваших поселенцев вплоть до самой отправки на основание нового поселения. Для основания деревни и её первоначального развития вашим поселенцам понадобится по 750 единиц каждого вида сырья.");
define("quest167","Для того, чтобы обучить трёх поселенцев, вам нужно открыть страницу резиденции/дворца, перейти на вкладку «Обучить» и начать обучение трёх поселенцев.");
define("quest168","Класс. Поселенцы всегда будет иметь некоторые ресурсы для новой деревни, чтобы ВЫ могли начать строить ее сразу же.");
define("quest169"," Новая деревня ");
define("quest170","Выберите на карте для основания новой деревни свободную клетку. Учитывайте расположение вашей новой деревни. Вы можете основать деревню рядом с первой деревней, рядом с полезными оазисами или на клетке с большим количеством полей определенного вида сырья.");
define("quest171","Отправьте поселенцев на основание новой деревни. Для этого перейдите на карту, выберите свободную клетку, а затем нажмите на «Основать поселение».");
define("quest172"," Отлично! Получите 48 часов TRAVIAN Plus");
define("quest173","Ежедневные задания");
define("quest174","Подарки каждый день");
define("quest175","Подробности");
define("register1","Регистрация ");
define("register2","Здравствуйте");
define("register3","Большое спасибо за регистрацию.");
define("register4","Имя");
define("register5","Пароль");
define("register6","Код активации");
define("register7","Воспользуйтесь следующей ссылкой, чтобы активировать свою учетную запись::");
define("register8","Желаем вам приятной игры и много славных сражений!,");
define("register9","Команда xTravian.ru");
define("register10","Новый пароль");
define("register11","Вы запросили новый пароль для своего аккаунта.");
define("register12","Пожалуйста, нажмите на эту ссылку, чтобы активировать новый пароль.Старый пароль
становится недействительным:");
define("register13","Если вы хотите, чтобы изменить свой пароль, вы можете изменить в вашем профиле
на вкладке Учетная запись.");
define("register14","В случае, если Вы не запрашивали новый пароль, вы можете проигнорировать это письмо.");