﻿<?php include('../GameEngine/Lang/Admin/en.php'); ?>
<div>
    <img border="0" height="98" width="244" src="../img/admin/Logo_large.png"/>
    <div>
        <div class="point2"><?php echo $defineLang['top_date']." <b>".date('l, j F Y - h:i:sa'); ?></b></div>
        <div class="point2">
			<?php
			if($_SESSION['access'] == MULTIHUNTER) { 
				$msg = '<b><font color="Blue">'.$defineLang['top_multihunter'].'</font></b>';
                
			} elseif($_SESSION['access'] == ADMIN){
				$msg = '<b><font color="Red">'.$defineLang['top_administrator'].'</font></b>'; 
			}
			echo $defineLang['top_welcome_msg'] ." ". $msg;
			?>
		</div>
        <div class="point2"><?php echo $defineLang['top_IPlogged'] . " " . $_SERVER['REMOTE_ADDR']; ?></div>
    </div>
</div>