<?php
class UnitPayModel
{
    private $mysqli;
    private $mysqli2;
    static function getInstance()
    {
        return new self();
    }

    private function __construct()
    {
        $this->mysqli = @new mysqli (
            Config::DB_HOST, Config::DB_USER, Config::DB_PASS, Config::DB_NAME
        );
        /* проверка подключения */
        if (mysqli_connect_errno()) {
            throw new Exception('Не удалось подключиться к бд');
        }
    }

    function createPayment($unitpayId, $account, $sum, $itemsCount)
    {

        $account=explode("|",$account);
        $query = '
            INSERT INTO
                unitpay_payments (unitpayId, account, sum, itemsCount, dateCreate, status,server)
            VALUES
                (
                    "'.$this->mysqli->real_escape_string($unitpayId).'",
                    "'.$this->mysqli->real_escape_string($account[0]).'",
                    "'.$this->mysqli->real_escape_string($sum).'",
                    "'.$this->mysqli->real_escape_string($itemsCount).'",
                    NOW(),
                    0,
                    "'.$this->mysqli->real_escape_string($account[1]).'"
                )
        ';

        return $this->mysqli->query($query);
    }

    function getPaymentByUnitpayId($unitpayId)
    {
        $query = '
                SELECT * FROM
                    unitpay_payments
                WHERE
                    unitpayId = "'.$this->mysqli->real_escape_string($unitpayId).'"
                LIMIT 1
            ';
            
        $result = $this->mysqli->query($query);
        return $result->fetch_object();
    }

    function confirmPaymentByUnitpayId($unitpayId)
    {
        $query = '
                UPDATE
                    unitpay_payments
                SET
                    status = 1,
                    dateComplete = NOW()
                WHERE
                    unitpayId = "'.$this->mysqli->real_escape_string($unitpayId).'"
                LIMIT 1
            ';
        return $this->mysqli->query($query);
    }
    
    function getAccountByName($account)
    {
        $account=explode("|",$account);
        $sql = "
            SELECT
                *
            FROM
               unitpay_payments
            WHERE
               account= '".$this->mysqli->real_escape_string($account[0])."'
            LIMIT 1
         ";
         
        $result = $this->mysqli
            ->query($sql);


       // if(!$result0->num_rows){

          //  $this->mysqli->query("INSERT INTO `bank` (`id`,`email`,`gold`) VALUES (0,'".$this->mysqli->real_escape_string($account)."',0)");
       // }
       /* $sql = "
            SELECT
                *
            FROM
               ".Config::TABLE_ACCOUNT."
            WHERE
               ".Config::TABLE_ACCOUNT_NAME." = '".$this->mysqli->real_escape_string($account)."'
            LIMIT 1
         ";
*/
       // $result = $this->mysqli
           // ->query($sql);
        return $result->fetch_object();
    }
    
    function donateForAccount($account, $count)
    {
        global $loginDB;
        $account=explode("|",$account);

      /*  $this->mysqli->query("INSERT IGNORE INTO `bank` (`id`,`email`,`gold`) VALUES ('0','".$this->mysqli->real_escape_string($account)."','0')");

        $query = "
            UPDATE
                ".Config::TABLE_ACCOUNT."
            SET
                ".Config::TABLE_ACCOUNT_DONATE." = ".Config::TABLE_ACCOUNT_DONATE." + ".$this->mysqli->real_escape_string($count)."
            WHERE
                ".Config::TABLE_ACCOUNT_NAME." = '".$this->mysqli->real_escape_string($account)."'
        ";
     */
        $this->mysqli2 = @new mysqli (
            $loginDB[$account[1]]['Server'], $loginDB[$account[1]]['User'], $loginDB[$account[1]]['Password'], $loginDB[$account[1]]['Database']
        );

        $query="UPDATE `users` SET `gold`= gold + ".$this->mysqli->real_escape_string($count)." WHERE `email`='".$this->mysqli->real_escape_string($account[0])."'";


        return $this->mysqli2->query($query);
    }
}