<?php
###############################  E    N    D   ##################################
##              -= YOU MAY NOT REMOVE OR CHANGE THIS NOTICE =-                 ##
## --------------------------------------------------------------------------- ##
##  Developed by:  Brainiac & Wolfcruel                                        ##
##  License:       BrainianZ Project                                        ##
##  Copyright:     BrainianZ © 2011-2014. Skype brainiac.brainiac         ##
##                                                                             ##
#################################################################################
header('Content-Type: text/html; charset=UTF-8');

set_time_limit(0);
error_reporting(E_ALL ^ E_NOTICE);

date_default_timezone_set("Europe/Moscow");

$loginDB['index']=array('Server'=>"localhost",'User'=>"karamzda_index",'Password'=>"superparolforbank",'Database'=>"karamzda_index");

define("SERVER_NAME","%SERVERNAME%");
define("COSTRES",%COSTRES%);
define("DEFAULT_GOLD",%DEFGOLD%);  //скок голды даем по умолчанию
define("HOWRES",%HOWRES%);
define("COSTCP",%COSTCP%);
define("HOWCP",%HOWCP%);
define("AUCTIONTIME",%AUCTIME%);
define("GP_LOCATE", "gpack/delusion_4.4/");
define("OPENING", %OPENING%); //открытие серва
define("REF_POP",%REFPOP%);
define("REF_GOLD",%REFGOLD%);
define("OASISX",%OASISX%);
define("SPEED", %SPEED%);
define("MAX_FILES",%MAX_FILES%);
define("MAX_FILESH",%MAX_FILESH%);
define("IMGQUALITY",%IMGQUALITY%);
define("MOMENT_TRAIN",%MOMENT_TRAIN%);
define("QUEST",%QUEST%);
define("ARTEFACTS",%ARTEFACTS%);
define("WW_TIME",%WW_TIME%);
define("WW_PLAN",%WW_PLAN%);
define("SELL_CP",%SELL_CP%);
define("SELL_RES",%SELL_RES%);
define("CRANNY_CAP",%CRANNY%);
define("ADV_TIME",86400/%ADVS%);
define("TRAPPER_CAPACITY",%TRAPER%);
define("xQUEST",0);


define("MAX_UNIT",60);
define("MAX_TRIBE",6);
// ***** Change storage capacity
define("STORAGE_MULTIPLIER","%STORAGE_MULTIPLIER%");

define("STORAGE_BASE",800*STORAGE_MULTIPLIER);

// ***** World size
// Defines world size. NOTICE: DO NOT EDIT!!
define("WORLD_MAX", "%MAX%");

define("INCREASE_SPEED",%INCSPEED%);

// ***** Beginners Protection
define("PHOUR", "%PRHOUR%");
define("PROTECTIOND",%BEGINNER%);
$timestoup = 0;
$fromstart=time()-OPENING;
if($fromstart>=42300){
$timestoup=floor($fromstart/42300);
}
define("PROTECTION",PROTECTIOND+($timestoup*PHOUR));
// ***** Trader capacity
// Values: 1 (normal), 3 (3x speed) etc...
define("TRADER_CAPACITY",%TRADER%);

define("INCLUDE_ADMIN",%ARANK%);

//////////////////////////////////
//   ****  SQL SETTINGS  ****   //
//////////////////////////////////

// ***** SQL Hostname
// example. sql106.000space.com / localhost
// If you host server on own PC than this value is: localhost
// If you use online hosting, value must be written in host cpanel
define("SQL_SERVER", "%SSERVER%");

// ***** Database Username
define("SQL_USER", "%SUSER%");

// ***** Database Password
define("SQL_PASS", "%SPASS%");

// ***** Database Name
define("SQL_DB", "%SDB%");


define("CP", "%VILLAGE_EXPAND%");

// ***** PLUS
//Plus account lenght
define("PLUS_TIME",%PLUS_TIME%);
//+25% production lenght
define("PLUS_PRODUCTION",%PLUS_PRODUCTION%);
// ***** через сколько клеток начинается действие арены
define("TS_THRESHOLD",%TS_THRESHOLD%);


//////////////////////////////////////////
//   ****  DO NOT EDIT SETTINGS  ****   //
//////////////////////////////////////////
define("ALLOW_ALL_TRIBE",false);
define("USRNM_MIN_LENGTH",3);
define("PW_MIN_LENGTH",4);
define("BANNED",0);
define("MULTIHUNTER",8);
define("ADMIN",9);
define("COOKIE_EXPIRE", 60*60*24*7);
define("COOKIE_PATH", "/");
define("HOMEPAGE", "%HOMEPAGE%");
define("MAXLENGHT","15");
define("RADIUS",2); //делитель для максимального радиуса выпадения артов,например если карта 400 а делитель 2 то дальше 200х200 арт НЕ улетит

