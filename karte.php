<?php
include "GameEngine/Village.php";
if(isset($_GET['d']) && !is_numeric($_GET['d'])) die('Hacking Attemp');
if(isset($_GET['c'])){$c=$_GET['c'];}

ob_start("ob_gzhandler");

?>
<!DOCTYPE html>
<html>
<?php include("Templates/html.php");?>


<body class="v35 <?=$database->bodyClass($_SERVER['HTTP_USER_AGENT']); ?> map <?php if($dorf1==''){echo 'perspectiveBuildings';}else{ echo 'perspectiveResources';} ?>">
<script type="text/javascript">
    window.ajaxToken = 'de3768730d5610742b5245daa67b12cd';
</script>
<div id="background">
    <div id="headerBar"></div>
    <div id="bodyWrapper">



        <div id="header">
            <?php
            include("Templates/topheader.php");
            include("Templates/toolbar.php");
            ?>
        </div>
        <div id="center">


            <?php include("Templates/sideinfo.php"); ?>

            <div id="contentOuterContainer">

                <?php include("Templates/res.php"); ?>
                <div class="contentTitle">
                    <a id="closeContentButton" class="contentTitleButton" href="dorf<?=$session->link?>.php" title="Close window">&nbsp;</a>
                    <a id="answersButton" class="contentTitleButton" href="http://t4.answers.travian.com/index.php?aid=106#go2answer" target="_blank" title="Travian Answers">&nbsp;</a>						</div>
                <div class="contentContainer">
                <?php
if(!isset($_GET['d'])){
	include("Templates/Map/newmap.php");
}else{
    include("Templates/Map/vilview.php");
}

?>

                </div>
                <div class="clear">&nbsp;</div>

                <div class="contentFooter"></div>
            </div>
            <?php
            include("Templates/rightsideinfor.php");
            ?>
            <div class="clear"></div>
        </div>
        <?php

        include("Templates/header.php");
        ?>
    </div>
    <div id="ce"></div>
</div>
</body>
</html>
